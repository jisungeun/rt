﻿Feature: 3D Visualizer FL Time-Lapse

  Scenario: 3D Visualizer application을 이용하여 HT+FL Time-Lapse 데이터에서 이미지 파일과 영상 파일을 Export
Given 빈 TA가 열려있다.

When Select Project 탭의 Open 버튼을 누른다.

And “S03_3DVisualizerC” 프로젝트 파일을 선택한다.

Then “S03_3DVisualizerC” 프로젝트가 TA에 열렸는지 확인

When Preview 패널에서 FL Time-lapse TCF를 한 개 선택한다.

And Toolbox의 File Information 버튼을 누른다.

Then 선택한 HT+FL Time-lapse TCF의 Information 팝업이 열렸는지 확인 

When Information 팝업 이미지 부분 상단의 FL 버튼을 누른다.

Then Information 팝업의 이미지가 FL 사진으로 바뀌었는지 확인

When Information 팝업 이미지 부분 하단의 타임 바를 마우스로 클릭한 채로 오른쪽으로 끝까지 움직인다.

Then Information 팝업 이미지의 time point가 달라졌는지 확인

When Information 팝업에서 오른쪽 위의 X 표시를 누른다.

And Preview 패널에서 FL Time-lapse TCF를 한 개 선택한다.

And Toolbox의 3D visualization 버튼을 누른다.

Then 선택한 FL Time-lapse TCF로 3D Visualizer 탭이 열렸는지 확인

When Preset 패널 TF canvas 밑의 Heatmap 체크박스를 누른다.

Then Preset 패널 TF canvas의 색이 Heatmap 색으로 변경되었는지 확인

When Modality 패널의 FL 체크박스를 클릭한다.

Then View 패널 3D 화면에 FL Rendering이 생겼는지 확인

And View 패널 HT 화면에 형광 표지가 입혀졌는지 확인 한다.

When Preset 패널 상단의 Choose a TF preset 드롭다운 버튼을 누른다.

And 드롭다운에서 K562_FLTL을 선택한다.

Then Preset 패널 상단 드롭다운 버튼에 K562_FLTL 텍스트가 나타나는지 확인

And TF canvas에 K562_FLTL preset의 TF box가 생성됐는지 확인

And View 패널에 K562_FLTL preset의 Rendering이 생성됐는지 확인

When Viewing tool 패널의 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Show TimeStamp 체크박스를 누른다.

Then View 패널 3D rendering 화면에 Time stamp가 생겼는지 확인

When Movie maker 탭의 Add 버튼을 누르고 Timelapse 버튼을 누른다.

And 마우스 포인터 모양이 바뀌면 마우스를 클릭한 채로 Timelapse 옵션 막대 길이를 4초까지 늘린다.

And Movie maker 탭의 Record 버튼을 누르고 Multi-view 버튼을 누른다.

And 파일 이름과 경로를 지정한다.

Then 동영상이 지정한 경로에 지정한 이름으로 생성되었는지 확인

When Application tab 바의 3D Visualizer 탭 이름 옆의 x 버튼을 클릭한다.

Then 3D Visualizer 탭이 종료되는지 확인