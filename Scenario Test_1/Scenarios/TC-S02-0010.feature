﻿Feature: 2D Image Gallery

  Scenario: TA에서 2D Image Gallery application을 이용하여 Hypercube 내의 Cube들 사이의 2D image를 비교
Given 빈 TA가 열려있다.

When Select Project 탭의 Open 버튼을 누른다.

And “S02_2DImageGallery” 프로젝트 파일을 선택한다.

Then “S02_2DImageGallery” 프로젝트가 TA에 열렸는지 확인

When Playground canvas 패널에서 “Test” Hypercube에 연결된 2D Image Gallery 아이콘을 클릭한다.

Then Application Parameter 패널에 Batch RUN 버튼이 있는지 확인

When 2D Image Gallery Application Parameter 패널의 Batch RUN 버튼을 누른다.

Then 2D Image Gallery 탭이 나타났는지 확인

And 2D Image Gallery 탭에서 각 cube 내의 TCF가 HT 2D MIP 사진으로 cube 별로 보이는지 확인

When 2D Image Gallery 탭의 “Case” cube 영역에서 > 버튼을 누른다.

Then 2D Image Gallery 탭의 Page 드롭다운 버튼에 Page 2가 나타났는지 확인

And 2D Image Gallery 탭의 cube 패널에 보이는 이미지가 달라졌는지 확인

And 2D Image Gallery 탭의 이미지가 없는 빈칸에 각각 No Image 텍스트가 출력 되었는지 확인

When 2D Image Gallery 탭의 “Case” cube 영역에서 "<" 버튼을 누른다.

Then 2D Image Gallery 탭의 Page 드롭다운 버튼에 Page 1이 나타났는지 확인

And 2D Image Gallery 탭의 cube 패널에 나타나는 이미지가 달라졌는지 확인

When 2D Image Gallery 탭 우상단의 FL 버튼을 누른다.

Then 2D Image Gallery 탭에서 FL로 촬영된 이미지가 HT MIP 이미지에서 FL MIP 이미지로 바뀌었는지 확인

And 2D Image Gallery 탭의 FL이 없는 이미지의 자리에서 HT 2D MIP 이미지가 사라지고 No FL 텍스트가 나타났는지 확인

When 2D Image Gallery 탭 우상단의 BF 버튼을 누른다.

Then 2D Image Gallery 탭에서 BF로 촬영된 이미지가 No FL 텍스트에서 BF MIP 이미지로 바뀌었는지 확인

And 2D Image Gallery 탭의 BF가 없는 이미지의 자리에서 FL MIP 이미지가 사라지고 No BF 텍스트가 나타났는지 확인

When 2D Image Gallery 탭 우상단의 HT 버튼을 누른다.

Then 2D Image Gallery 탭의 각 cube 패널에 HT 이미지가 나타났는지 확인

When 2D Image Gallery 탭 “Case” cube의 영역 page 1에서 제일 왼쪽 TCF 이미지를 클릭한다.

Then 2D Image Gallery 탭에서 클릭한 TCF 이미지가 선택되었는지 확인

When 2D Image Gallery 탭에서 이미지가 선택되어 있을 때 2D Image Gallery 탭 우상단의 Info 버튼을 누른다.

Then 2D Image Gallery 탭 위에 View information 팝업이 나타났는지 확인

When View information 팝업에서 X 버튼을 누른다.

And 2D Image Gallery 탭에서 Rep 버튼을 클릭한다.

And 팝업의 Target Index 칸에 2-2를 입력하고 키보드의 엔터 키를 누른다.

Then 2D Image Gallery 탭에서 두 TCF의 자리가 서로 바뀌었는지 확인

And 2D Image Gallery 탭 “Case” cube 영역 1-1 자리의 TCF 네모 하이라이트가 1-1 자리에 그대로 남아있는지 확인

When 2D Image Gallery 탭의 Cap 버튼을 클릭한다.

And 파일 이름과 저장 경로를 지정한다.

Then 2D Image Gallery 탭 화면이 지정한 경로에 지정한 이름으로 .png 파일로 저장되었는지 확인

When Application tab 바의 2D Image Gallery 탭 이름 옆의 x 버튼을 누른다.

Then 2D Image Gallery 탭이 종료됐는지 확인