﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
import re
global Count
Count = 0
global mhtpass
mhtpass = 'C:\\Work\\Log{count}.mht'
global testname
testname = 'test'

def LogColumn():
  # Obtains a collection of logs
  LogsCol = Project.Logs
  # Obtains the first log item
  LogItem = LogsCol.LogItem[0]
  # Obtains the first dataset
  # of the specified log item
  LogData = LogItem.Data[0]
  # Obtains the scheme of the dataset
  Sch = LogData.Scheme
  # Obtains information about the column
  Column = Sch.ColumnByName["Message"]

def GeneralEvents_OnLogError(Sender, LogParams):
  global testname
  testname=Project.TestItems.Current.Name
  
def GeneralEvents_OnStopTestCase( Sender ,  StopTestCaseParams):
  if StopTestCaseParams.StatusAsText == "Error":
    global Count
    global testname
    Log.SaveResultsAs(mhtpass.format(count=Count), lsMHT,True,2)
    scenario = Features.Item[0].Description
    TestItems = Project.TestItems
    summary = '[TCRT]{name}'
    IssueToJira(summary.format(name=testname), StopTestCaseParams.Name)
  
def IssueToJira(summary,description):
  # Log in to Jira
  global Count
  Jira.Login("https://tomocube.atlassian.net/", "seji@tomocube.com", "HxauRk8w78iNlYxCscMC536E")
  priorityJSON = '{"SUNGEUN JI":"Low"}'
  # Create an object that defines task properties
  jiraData = Jira.CreateNewIssueData("TCRT", "버그").\
                setField("summary", summary).\
                setField("description", description).\
                setField("customfield_10667", ProjectSuite.Variables.version);
  # Post the issue to Jira 
  key = Jira.PostIssue(jiraData)
  # Attach the desired file to the created issue
  Jira.PostAttachment(key, mhtpass.format(count=Count))
  mhtremove = r'C:\Work\*.mht'
  upPriorityJSON = '{"SUNGEUN JI":"High"}'
  # Create an object that defines updating task properties
  upJiraData = Jira.CreateUpdateIssueData()
  # Update the issue
  Count = Count + 1
  Jira.UpdateIssue(key, upJiraData)
  for f2 in glob.glob(mhtremove):
    os.remove(f2)

  
@given("‘Playground’라는 이름의 playground가 생성되어 있다.")
def test():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")

  
@when("Set playground 탭으로 이동한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab.ClickButton()

@when("New Playground 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddPGPanel.widget_panel.bt_round_operation.Click(94, 30)

@when("Playground의 이름을 ‘Playground’로 지정하고 OK 버튼을 누른다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddPGPanel.widget_panel2
  lineEdit = widget.input_high
  lineEdit.Click(143, 5)
  lineEdit.SetText("Playground")
  widget.bt_round_operation.Drag(44, 7, 1, 5)

@then("에러 팝업이 나타나며 Playground가 생성되지 않음을 확인")
def step_impl():
  Aliases.TomoAnalysis.MessageBox.qt_msgbox_buttonbox.buttonOk.ClickButton()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("“A”라는 이름의 playground가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.pglistPanel.panel_base
  frame.ClickR(285, 25)
  frame.bt_square_gray.Click(98, 14)
  tomoAnalysis_ProjectManager_Plugins_RenameDialog = tomoAnalysis.RenameDialog
  lineEdit = tomoAnalysis_ProjectManager_Plugins_RenameDialog.input_high
  lineEdit.Click(67, 16)
  lineEdit.SetText("A")
  tomoAnalysis_ProjectManager_Plugins_RenameDialog.bt_square_primary.ClickButton()

@given("“B” 라는 이름의 playground가 생성되어 있다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.frame_tab.bt_round_tab.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_AddPGPanel = frame.widget_panel.AddPGPanel
  tomoAnalysis_ProjectManager_Plugins_AddPGPanel.widget_panel.bt_round_operation.Click(176, 22)
  lineEdit = tomoAnalysis_ProjectManager_Plugins_AddPGPanel.widget_panel2.input_high
  lineEdit.Click(181, 23)
  lineEdit.SetText("B")
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddPGPanel.widget_panel2
  widget.bt_round_operation.Drag(44, 7, 1, 5)
    
@when("Playground Setup 패널에서 “B” playground를 선택하고 Rename 버튼을 누른다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.pglistPanel.panel_base
  frame.panel_contents.ClickItem("B")
  frame.bt_square_gray.Click(131, 17)

@when("팝업에서 변경할 이름으로 “A”를 입력하고 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_RenameDialog = Aliases.TomoAnalysis.RenameDialog
  lineEdit = tomoAnalysis_ProjectManager_Plugins_RenameDialog.input_high
  lineEdit.Click(40, 21)
  lineEdit.SetText("A")
  tomoAnalysis_ProjectManager_Plugins_RenameDialog.bt_square_primary.ClickButton()

@then("에러 팝업이 나타나며 Playground의 이름이 변경되지 않음을 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.MessageBox.qt_msgbox_buttonbox.buttonOk.ClickButton()
  tomoAnalysis.MessageBox2.qt_msgbox_buttonbox.buttonOk.ClickButton()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("“alpha”라는 이름의 Hypercube가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()

@given("“beta” 라는 이름의 Hypercube가 생성되어 있다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.frame_tab.bt_round_tab3.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(136, 16)
  lineEdit.SetText("beta")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()

@when("Playground canvas 패널에서 “beta” Hypercube를 우클릭하고 Rename Hypercube 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_hcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.ClickR(41, 60)
  Aliases.TomoAnalysis.QtObject("QMenu", "", 1).Click(10,10)


@when("팝업에서 변경할 이름으로 “alpha”를 입력하고 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_RenameCube = Aliases.TomoAnalysis.RenameCube
  lineEdit = tomoAnalysis_ProjectManager_Plugins_RenameCube.newName
  lineEdit.Click(57, 13)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_RenameCube.okBtn.ClickButton()

@then("에러 팝업이 나타나며 Hypercube의 이름이 변경되지 않음을 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.MessageBox3.qt_msgbox_buttonbox.buttonOk.ClickButton()
  tomoAnalysis.MessageBox4.qt_msgbox_buttonbox.buttonOk.ClickButton()
  Aliases.TomoAnalysis.mainwindow.Click(1657, 24)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("'12'라는 이름의 Cube가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()

@given("'21' 이라는 이름의 Cube가 생성되어 있다.")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("21")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()

@when("Playground canvas 패널에서 “{arg}” Cube를 우클릭하고 Rename Cube 버튼을 누른다.")
def step_impl(param1):
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.ClickR(25, 64)
  Aliases.TomoAnalysis.QtObject("QMenu", "", 1).Click(10,10)



@when("팝업에서 변경할 이름으로 “{arg}”를 입력하고 OK 버튼을 누른다.")
def step_impl(param1):
  tomoAnalysis_ProjectManager_Plugins_RenameCube = Aliases.TomoAnalysis.RenameCube
  lineEdit = tomoAnalysis_ProjectManager_Plugins_RenameCube.newName
  lineEdit.Click(33, 16)
  lineEdit.SetText("12")
  tomoAnalysis_ProjectManager_Plugins_RenameCube.okBtn.ClickButton()

@then("에러 팝업이 나타나며 Cube의 이름이 변경되지 않음을 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.MessageBox5.qt_msgbox_buttonbox.buttonOk.ClickButton()
  tomoAnalysis.MessageBox6.qt_msgbox_buttonbox.buttonOk.ClickButton()
  Aliases.TomoAnalysis.mainwindow.Click(1657, 24)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Select Application 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(129, 32)
  frame.bt_round_tool.Click(52, 8)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()

@given("Playground에 Hypercube가 두 개 생성되어 있다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit = frame.widget_panel.HyperCubePanel.input_high
  lineEdit.Click(126, 20)
  lineEdit.SetText("alpha3")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.HyperCubePanel.bt_round_operation.ClickButton()

@when("Select Application 탭에서 App을 적용할 hypercube를 선택하는 드롭다운 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high2.DropDown()
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high.DropDown()

@then("Hypercube 한 개당 Select Application 탭의 드롭다운에 하나의 버튼만 생성되었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport.Check(Aliases.TomoAnalysis.ComboBoxPrivateContainer.ListView.qt_scrollarea_viewport)
  Aliases.TomoAnalysis.mainwindow.Click(1657, 24)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Playground에 “A” 라는 이름의 Hypercube가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("A")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()

@when("Select Project 탭의 Open 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.frame_tab.bt_round_tab2.ClickButton()
  frame.widget_panel.SelectProjectPanel.bt_round_operation.Click(111, 6)

@when("파일 탐색기에서 “A” 라는 이름의 Hypercube가 없는 다른 프로젝트를 선택한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E02\\E02.tcpro")

@when("새로 열린 프로젝트의 Select Application 탭에서 App을 적용할 hypercube를 선택하는 드롭다운 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.frame_tab.bt_round_tab4.ClickButton()
  frame.widget_panel.SelectAppPanel.dropdown_high.DropDown()

@then("Select Application 드롭다운 버튼에 “A” hypercube가 없는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport1.Check(Aliases.TomoAnalysis.ComboBoxPrivateContainer.ListView.qt_scrollarea_viewport)
  Aliases.TomoAnalysis.mainwindow.Click(1657, 24)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("프로젝트에 “{arg}” 이라는 이름의 cube가 생성되어 있다.")
def step_impl(param1):
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12345678901234567890")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()

@when("List 패널 우상단의 ‘Cube name :’ 텍스트 옆에 있는 드롭다운 버튼에서 “{arg}” cube를 선택한다.")
def step_impl(param1):
  tomoAnalysis = Aliases.TomoAnalysis
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.ClickTab("List")
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.cubeComboBox.DropDown()


@then("List 패널 드롭다운 버튼의 “{arg}” cube 이름 뒷부분이 깔끔하게 생략되었는지 확인")
def step_impl(param1):
  Regions.qt_scrollarea_viewport2.Check(Aliases.TomoAnalysis.ComboBoxPrivateContainer.ListView.qt_scrollarea_viewport)
  Aliases.TomoAnalysis.mainwindow.Click(1657, 24)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("BA Application Parameter에서 AI Segmentation이 프로세서로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(129, 32)
  frame.bt_round_tool.Click(52, 8)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(117, 52)
  frame.bt_round_tool.Click(33, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab4.ClickButton()
  splitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()

@when("Select Application 탭에서 Basic Analyzer가 적용된 Hypercube에 2D Image Gallery를 적용한다.")
def step_impl():
  comboBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.dropdown_high2
  comboBox.ClickItem("2D Image Gallery")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()

@then("Application Parameter 패널에 2D Image Gallery의 UI가 나타났는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow3.TomoAnalysis_ProjectManager_Plugins_DraggableIcon, "QtText", cmpEqual, "")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow3.TomoAnalysis_ProjectManager_Plugins_DraggableIcon, "toolTip", cmpEqual, "Application!2D Image Gallery!alpha")
  Aliases.TomoAnalysis.mainwindow.Click(1657, 24)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("TA의 Select Project 탭 화면이 나타나있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(129, 32)
  frame.bt_round_tool.Click(52, 8)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(117, 52)
  frame.bt_round_tool.Click(33, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab4.ClickButton()
  splitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()


@when("Select Project의 Create 버튼을 누른다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.frame_tab.bt_round_tab2.ClickButton()
  frame.widget_panel.SelectProjectPanel.bt_round_operation2.Click(52, 13)

@when("New Project 팝업의 Project name 칸을 클릭하고 {arg}를 키보드로 누른다.")
def step_impl(param1):
  lineEdit = Aliases.TomoAnalysis.NewProjectDialog.input_high
  lineEdit.Click(212, 18)
  lineEdit.Keys("?")
  lineEdit.Click(53, 22)
  lineEdit.Keys("???!!!!@!!@#$%^^&*()")

@then("New Project 팝업의 Project name 칸에 아무 것도 입력되지 않았는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.NewProjectDialog.input_high, "wText", cmpEqual, "")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.NewProjectDialog.Close()
  tomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Create Hypercube 탭이 열려있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("A")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()


@when("Create Hypercube 탭의 hypercube 이름을 지정하는 칸에 {arg}를 키보드로 입력한다.")
def step_impl(param1):
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  buttonCreateHypercube = frame.frame_tab.bt_round_tab3
  buttonCreateHypercube.Drag(122, 26, -3, 23)
  buttonCreateHypercube.ClickButton()
  buttonCreateHypercube.ClickButton()
  lineEdit = frame.widget_panel.HyperCubePanel.input_high
  lineEdit.Click(88, 20)
  lineEdit.Keys("?!!@#$%^^")

@then("Create Hypercube 탭의 hypercube 이름을 지정하는 칸에 아무것도 입력되지 않았는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.HyperCubePanel.input_high, "wText", cmpEqual, "")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Create Cube 탭이 열려있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()


@when("Create Cube 탭의 cube 이름을 지정하는 칸에 {arg}를 키보드로 입력한다.")
def step_impl(param1):
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(73, 22)
  lineEdit.Keys("?!!@#$%")

@then("Create Cube 탭의 cube 이름을 지정하는 칸에 아무것도 입력되지 않았는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high, "wText", cmpEqual, "")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Playground canvas 패널에서 Hypercube에 적용되어 있는 Basic Analyzer 아이콘이 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(129, 32)
  frame.bt_round_tool.Click(52, 8)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(117, 52)
  frame.bt_round_tool.Click(33, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab4.ClickButton()
  splitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()

@when("Playground canvas에서 Hypercube 아이콘을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow4.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.Click(25, 45)

@then("Application Parameter 패널이 비활성화 되었는지 확인")
def step_impl():
  Regions.processorParamFrame.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)
  
@when("Playground canvas에서 Cube 아이콘을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow4.TomoAnalysis_ProjectManager_Plugins_DraggableIcon2.Click(44, 50)

@when("Playground canvas의 빈 공간을 클릭한다.")
def step_impl():
  Regions.processorParamFrame1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)
  
@given("“test” 라는 이름의 프로젝트가 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")

@when("Select Project 탭의 Create 버튼을 누르고 이름을 “test”로, Location을 열려 있는 프로젝트와 같은 경로로 설정한 후 Create 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.frame_tab.bt_round_tab2.ClickButton()
  frame.widget_panel.SelectProjectPanel.bt_round_operation2.Click(160, 36)
  tomoAnalysis_ProjectManager_Plugins_NewProjectDialog = tomoAnalysis.NewProjectDialog
  lineEdit = tomoAnalysis_ProjectManager_Plugins_NewProjectDialog.input_high
  lineEdit.Click(151, 6)
  lineEdit.SetText("E01")
  tomoAnalysis_ProjectManager_Plugins_NewProjectDialog.pathButton.ClickButton()
  dlg = tomoAnalysis.dlg
  HWNDView = dlg.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(77, 14)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.Prefix_File
  UIItem.Item.Click(59, 11)
  UIItem.Keys("[Enter]")
  UIItemsView.Create_Cube.Item.MouseWheel(-1)
  dlg.btn_.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_NewProjectDialog.createButton.ClickButton()

@then("Warning 팝업이 뜨며 프로젝트 생성이 되지 않는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.MessageBox8.qt_msgbox_buttonbox.buttonOk.ClickButton()
  tomoAnalysis.NewProjectDialog.Close()
  tomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Playground에 “A”, “B” 두 개의 cube가 만들어져 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("A")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  lineEdit.SetText("B")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()


@given("“A” cube에 어떤 TCF도 링크되어 있지 않다.")
def step_impl():
  Regions.ET_1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon2)

@given("“B” cube에 TCF가 link되어 있다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(106, 52)
  frame.bt_round_tool.Click(33, 12)
  tomoAnalysis_ProjectManager_Plugins_LinkDialog = tomoAnalysis.LinkDialog
  tomoAnalysis_ProjectManager_Plugins_LinkDialog.dropdown_high.ClickItem("B")
  tomoAnalysis_ProjectManager_Plugins_LinkDialog.bt_square_primary.ClickButton()

@when("Preview 패널에서 “B” cube에 link된 TCF를 클릭하고 Unlink from cube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  frame = splitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame2 = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget2.TC_TCF2DWidget.Click(136, 26)
  splitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow5.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.Click(43, 61)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(62, 103)
  frame.bt_round_tool2.Click(82, 13)

@when("팝업의 cube 드롭다운 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.UnlinkDialog.dropdown_high.DropDown()
  
@then("팝업의 cube 드롭다운 버튼에 “A” cube는 나타나지 않고 “B” cube만 나타났는지 확인")
def step_impl():
  Regions.ListView.Check(Aliases.TomoAnalysis.ComboBoxPrivateContainer.ListView)
  Aliases.TomoAnalysis.UnlinkDialog.dropdown_high.ClickItem("B")
  NameMapping.Sys.TomoAnalysis.UnlinkDialog.bt_square_line.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)



@given("Basic Analyzer 탭의 Mask Viewing panel에 3D AI 마스크가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(129, 32)
  frame.bt_round_tool.Click(52, 8)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(117, 52)
  frame.bt_round_tool.Click(33, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab4.ClickButton()
  splitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(31, 13)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(132, 100)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(85, 31)
  widget.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@given("Basic Analyzer 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸에 .5를 입력한다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity
  doubleSpinBox.qt_spinbox_lineedit.Drag(26, 6, -69, 8)
  doubleSpinBox.Keys("0")
  doubleSpinBox.wValue = 0
  doubleSpinBox.Keys(".5")
  doubleSpinBox.wValue = 0.5

@then("Mask Viewing panel의 마스크들이 입력 값에 맞게 투명해짐을 확인")
def step_impl():
  Regions.TomoAnalysis.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis4)

@when("Basic Analyzer 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창 옆의 위 화살표 버튼을 10번 누른다.")
def step_impl():
   for i in range(1, 11) :
    Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.Click(85,5)

@then("화살표에 따라 Basic Analyzer 탭 Mask Viewing panel의 Opacity 수치 값이 0.60으로 올라감을 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.qt_spinbox_lineedit, "wText", cmpEqual, "0.60")

@then("Basic Analyzer 탭 Mask Viewing panel의 마스크들이 점점 진해짐을 확인")
def step_impl():
  Regions.TomoAnalysis1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel,False,False,1000)


@given("Basic Analyzer 탭의 Mask Viewing panel에 3D 마스크의 Opacity가 0.5이다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity
  doubleSpinBox.qt_spinbox_lineedit.Drag(26, 6, -69, 8)
  doubleSpinBox.Keys("0")
  doubleSpinBox.wValue = 0
  doubleSpinBox.Keys(".5")
  doubleSpinBox.wValue = 0.5

@when("Basic Analyzer 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창 옆의 아래 화살표 버튼을 10번 누른다.")
def step_impl():
   for i in range(1, 11) :
    Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.Click(85,15)

@then("화살표에 따라 Basic Analyzer 탭 Mask Viewing panel의 Opacity 수치 값이 0.50으로 내려감을 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity, "wValue", cmpEqual, 0.5)

@then("Basic Analyzer 탭 Mask Viewing panel의 마스크들이 점점 투명해짐을 확인")
def step_impl():
  Regions.TomoAnalysis2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel,False,False,1000)

@when("Basic Analyzer 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창에 마우스 포인터를 대고 마우스 휠을 10칸 올린다.")
def step_impl():
  tomoAnalysis_BasicAnalysis_Plugins_VizControlPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity
  for i in range(1, 11) :
    doubleSpinBox.qt_spinbox_lineedit.MouseWheel(1)

@then("Basic Analyzer 탭 Mask Viewing panel의 Opacity 수치 값이 0.60으로 올라감을 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.qt_spinbox_lineedit, "wText", cmpEqual, "0.60")

@when("Basic Analyzer 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창에 마우스 포인터를 대고 마우스 휠을 10칸 내린다.")
def step_impl():
  tomoAnalysis_BasicAnalysis_Plugins_VizControlPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity
  for i in range(1, 11) :
    doubleSpinBox.qt_spinbox_lineedit.MouseWheel(-1)

@given("Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 AI Segmentation으로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(129, 32)
  frame.bt_round_tool.Click(52, 8)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(117, 52)
  frame.bt_round_tool.Click(33, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab4.ClickButton()
  splitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()


@given("Basic Analyzer 탭 Mask Viewing panel의 HT 이미지가 Inverse grayscale ColorMap으로 설정되어 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(734, 19)
  NameMapping.Sys.TomoAnalysis.Menu2.QtMenu.Check("ColorMap|Inverse grayscale", True)

@given("Basic Analyzer 탭 Mask Viewing panel의 HT 이미지의 Level window에서 최솟값이 최댓값까지 올라가있다.")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer 탭 Mask Viewing panel의 HT 이미지 캔버스의 우상단의 톱니바퀴 아이콘을 클릭하고 Reset View 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer 탭 Mask Viewing panel HT 이미지의 ColorMap이 Grayscale로, Level window가 초기 상태로 돌아왔는지 확인")
def step_impl():
  raise NotImplementedError

@given("BA Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E03\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E03'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E03'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E03\\E03.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.operationPanel.panel_base
  widget = frame.widget_panel
  widget2 = widget.AddPGPanel.widget_panel2
  lineEdit = widget2.input_high
  lineEdit.Click(190, 24)
  lineEdit.SetText("PG")
  widget2.bt_round_operation.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = widget.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(86, 19)
  lineEdit.SetText("HC")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  widget.CubePanel.bt_round_operation.ClickButton()
  frame.frame_tab.bt_round_tab5.ClickButton()
  frame = splitter.previewPanel.panel_base
  frame2 = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(164, 61)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(102, 75)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget2.TC_TCF2DWidget.panel_contents_image.Widget.Click(151, 97)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget4.TC_TCF2DWidget.panel_contents_image.Widget.Click(111, 97)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget5.TC_TCF2DWidget.panel_contents_image.Widget.Click(67, 97)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget6.TC_TCF2DWidget.panel_contents_image.Widget.Click(54, 80)
  frame.bt_round_tool.Click(76, 13)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  widget.SelectAppPanel.bt_round_operation.ClickButton()

  
@when("Batch Run 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(13, 15)

@when("프로세싱 바 팝업의 Cancel 버튼을 누른다.")
def step_impl():
  aqUtils.Delay(3000)
  Aliases.TomoAnalysis.ProgressDialog.buttonCancel.ClickButton()

  
@then("Basic Analyzer 탭과 프로세싱 바 팝업이 종료됐는지 확인")
def step_impl():
  aqUtils.Delay(50000)
  Regions.ET_2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

  
@when("BA Application Parameter에서 Single RUN 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(51, 21)

@when("Single run 팝업의 Cancel 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.Close()

@then("Single run 팝업이 닫히는지 확인")
def step_impl():
  if  Aliases.TomoAnalysis.widget_popup.Exists :
    Log.Error("Single run Exist")
    Aliases.TomoAnalysis.widget_popup.bt_square_gray.ClickButton()
    Aliases.tomoAnalysis.mainwindow.Close()
    aqUtils.Delay(3000)
    
  else :
    pass
    Aliases.tomoAnalysis.mainwindow.Close()
    aqUtils.Delay(3000)
  

@given("Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(129, 32)
  frame.bt_round_tool.Click(52, 8)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(117, 52)
  frame.bt_round_tool.Click(33, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab4.ClickButton()
  splitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(31, 13)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(132, 100)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(85, 31)
  widget.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()


@when("Basic Analyzer 탭 Mask Viewing panel에서 3D 마스크 화면의 톱니바퀴 버튼을 누르고 View 버튼을 누른 뒤 View X 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer 탭 Mask Viewing panel에서 마스크가 사라지지 않고 3D 마스크가 X축 방향으로 돌아갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer 탭 Mask Viewing panel에서 3D 마스크 화면의 톱니바퀴 버튼을 누르고 View 버튼을 누른 뒤 View Y 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer 탭 Mask Viewing panel에서 마스크가 사라지지 않고 3D 마스크가 Y축 방향으로 돌아갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer 탭 Mask Viewing panel에서 3D 마스크 화면의 톱니바퀴 버튼을 누르고 View 버튼을 누른 뒤 View Z 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer 탭 Mask Viewing panel에서 마스크가 사라지지 않고 3D 마스크가 Z축 방향으로 돌아갔는지 확인")
def step_impl():
  raise NotImplementedError

@given("BA Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(129, 32)
  frame.bt_round_tool.Click(52, 8)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(117, 52)
  frame.bt_round_tool.Click(33, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab4.ClickButton()
  splitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high2.ClickItem("Basic Analyzer[T]")
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.Click(677, 145)

@when("BA Application Parameter의 Basic Measurement에서 table header의 오른쪽 끝에서 마우스를 클릭한 채로 오른쪽으로 패널 끝까지 드래그 한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 203
  scrollBar.wPosition = 203
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterTable.TableWidget.TC_AdaptiveHeader.qt_scrollarea_viewport.Drag(48, 7, 133, -7)

@then("BA Application Parameter의 Basic Measurement에서 table의 너비가 늘어났는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0, "Width", cmpEqual, 182)

@then("BA Application Parameter의 Basic Measurement에서 table의 각 값들이 잘리거나 줄이 밀리지 않고 한 줄로 출력 됐는지 확인")
def step_impl():
  Regions.Preset_Setter_0.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0)
  Aliases.tomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(129, 32)
  frame.bt_round_tool.Click(52, 8)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(117, 52)
  frame.bt_round_tool.Click(33, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab4.ClickButton()
  splitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(55, 28)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(122, 84)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(45, 21)

@when("Basic Analyzer 탭의 Basic Measurement에서 table header의 오른쪽 끝에서 마우스를 클릭한 채로 오른쪽으로 패널 끝까지 드래그 한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 135
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.TC_AdaptiveHeader.qt_scrollarea_viewport.Drag(47, 8, 120, 7)


@then("Basic Analyzer 탭의 Basic Measurement에서 table의 너비가 늘어났는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0, "Width", cmpEqual, 169)

@then("Basic Analyzer 탭의 Basic Measurement에서 table의 각 값들이 잘리거나 줄이 밀리지 않고 한 줄로 출력 됐는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 130
  Regions.CollapseWidget.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)

@given("Basic Analyzer 탭의 Mask Viewing panel에 RI 마스크가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E01\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  img = r'E:\Regression_Test\*.png'
  for f in glob.glob(img):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\E01'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E01'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E01\\E01.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = frame.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(134, 25)
  frame.frame_tab.bt_round_tab3.ClickButton()
  lineEdit.Click(135, 22)
  lineEdit.SetText("alpha")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(49, 17)
  lineEdit.SetText("12")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(129, 32)
  frame.bt_round_tool.Click(52, 8)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(117, 52)
  frame.bt_round_tool.Click(33, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab4.ClickButton()
  splitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(31, 13)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(132, 100)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(85, 31)
  widget.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@when("Basic Analyzer 탭에서 Mask Viewing panel의 2D HT 화면의 톱니바퀴 버튼을 누르고 Color map 버튼을 누른 뒤 Inverse grayscale 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer 탭에서 Mask Viewing panel의 2D HT 화면에 마스크가 남아있는지 확인")
def step_impl():
  raise NotImplementedError

@when("BA Application parameter 패널의 Batch Run 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(71, 29)

@when("프로세싱 바 팝업의 진행이 완료된다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");
  
@then("프로세싱 바 팝업과 Basic Analyzer 탭이 사라지는 지 확인")
def step_impl():
  Regions.ET_3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@then("table과 graph가 출력된 Report 탭이 화면에 나타나는지 확인")
def step_impl():
  Regions.ET_4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)

@then("Report[T] 탭의 Select Organelle 패널에 Whole Cell 체크박스 하나밖에 없는지 확인")
def step_impl():
  Regions.organellesGroupBox.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox)
  Aliases.tomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Basic Analyzer[T] Single Run 탭에서 AI Segmentation이 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E04\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E04'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E04'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E04\\E04.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(56, 15)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(123, 105)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(35, 9)
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.timeInterval.qt_spinbox_lineedit.Click(18, 15)
  spinBox = groupBox.endTime
  spinBox.qt_spinbox_lineedit.Drag(19, 15, -35, -7)
  spinBox.Keys("2")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.applyBtn.ClickButton()


@when("Basic Analyzer[T] 탭 AI Inference 탭의 Execute 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis

@when("프로세싱 바 팝업이 생기면 팝업의 Cancel 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.ProgressDialog.Close()

@then("Basic Analyzer[T] 탭의 프로세싱 바 팝업이 종료됐는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  if tomoAnalysis.ProgressDialog.Exists :
    Log.Warning("Exist")
  else :
    pass
    
@then("작업 중이던 time point까지의 마스크만 생성되었는지 확인")
def step_impl():
  aqUtils.Delay(30000)
  Regions.TomoAnalysis15.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.wValue = 2
  Regions.TomoAnalysis16.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)
  Aliases.tomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel에서 3D 마스크 캔버스의 우상단의 톱니바퀴 아이콘을 클릭하고 View 버튼을 클릭한 후 View X 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel에 X축 방향에서 3D 마스크가 보이는지 확인")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스 좌하단의 정육면체가 Front 방향으로 변경되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel에서 3D 마스크 캔버스의 우상단의 톱니바퀴 아이콘을 클릭하고 View 버튼을 클릭한 후 View Y 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel에 Y축 방향에서 3D 마스크가 보이는지 확인")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스 좌하단의 정육면체가 Left 방향으로 변경되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel에서 3D 마스크 캔버스의 우상단의 톱니바퀴 아이콘을 클릭하고 View 버튼을 클릭한 후 View Z 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel에 Z축 방향에서 3D 마스크가 보이는지 확인")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스 좌하단의 정육면체가 Bottom 방향으로 변경되었는지 확인")
def step_impl():
  raise NotImplementedError

@given("Basic Analyzer[T] Single Run 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E04\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E04'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E04'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E04\\E04.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(56, 15)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(123, 105)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(35, 9)
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.timeInterval.qt_spinbox_lineedit.Click(18, 15)
  spinBox = groupBox.endTime
  spinBox.qt_spinbox_lineedit.Drag(19, 15, -35, -7)
  spinBox.Keys("2")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.applyBtn.ClickButton()

@given("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지가 Inverse grayscale ColorMap으로 설정되어 있다.")
def step_impl():
  raise NotImplementedError

@given("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지의 Level window에서 최솟값이 최댓값까지 올라가있다.")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스의 우상단의 톱니바퀴 아이콘을 클릭하고 Reset View 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel HT 이미지의 ColorMap이 Grayscale로, Level window가 초기 상태로 돌아왔는지 확인")
def step_impl():
  raise NotImplementedError

@given("Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 마스크가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E04\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E04'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E04'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E04\\E04.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(56, 15)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(123, 105)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(35, 9)
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.timeInterval.qt_spinbox_lineedit.Click(18, 15)
  spinBox = groupBox.endTime
  spinBox.qt_spinbox_lineedit.Drag(19, 15, -35, -7)
  spinBox.Keys("2")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.applyBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

@when("Basic Analyzer[T] 탭 Basic Measurement의 table의 parameter를 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.")
def step_impl():
  Delay(5000, "Waiting for window to close...")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 70
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.buttonExecute.ClickButton()


@when("프로세싱 바 팝업이 나타나면 팝업의 Cancel 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.ProgressDialog.Close()

@then("작업 중이던 time point 까지 정량 분석 결과 Measureme")
def step_impl():
  while NameMapping.Sys.TomoAnalysis.BaTimeResultPanel.Exists:
    if NameMapping.Sys.TomoAnalysis.BaTimeResultPanel.Exists :
      Tables.tableWidget.Check()
      NameMapping.Sys.TomoAnalysis.BaTimeResultPanel.Close()
      Aliases.TomoAnalysis.mainwindow.Close()
      break
    else :
      pass



@when("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스 우상단의 톱니바퀴 아이콘을 클릭하고 Color Map 버튼을 클릭한 뒤 Color map의 Inverse grayscale 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스와 마스크 색이 Inverse grayscale로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스 우상단의 톱니바퀴 아이콘을 클릭하고 Color Map 버튼을 클릭한 뒤 Color map의 Hot iron 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스와 마스크 색이 Hot iron으로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스 우상단의 톱니바퀴 아이콘을 클릭하고 Color Map 버튼을 클릭한 뒤 Color map의 JET 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스와 마스크 색이 JET로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스 우상단의 톱니바퀴 아이콘을 클릭하고 Color Map 버튼을 클릭한 뒤 Color map의 Rainbow 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스와 마스크 색이 Rainbow로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스 우상단의 톱니바퀴 아이콘을 클릭하고 Color Map 버튼을 클릭한 뒤 Color map의 Grayscale 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스와 마스크 색이 Grayscale로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@given("Playground canvas에서 Basic Analyzer[T] 아이콘이 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E04\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E04'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E04'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E04\\E04.tcpro")

@when("BAT Application Parameter에서 Single RUN 버튼을 누르고 Single run 팝업의 Cancel 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(102, 18)
  Aliases.TomoAnalysis.widget_popup.Close()


@given("Basic Analyzer[T] 탭의 RI Thresholding 모드에서 Mask Viewing panel에 마스크가 생성되어 있다.")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭에서 Mask Viewing panel의 2D HT 화면의 톱니바퀴 버튼을 누르고 Color map 버튼을 누른 뒤 Inverse grayscale 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스와 마스크 색이 Inverse grayscale로 변경됐는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭에서 Mask Viewing panel의 2D HT 화면의 톱니바퀴 버튼을 누르고 Color map 버튼을 누른 뒤 Hot iron 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스와 마스크 색이 Hot iron으로 변경됐는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭에서 Mask Viewing panel의 2D HT 화면의 톱니바퀴 버튼을 누르고 Color map 버튼을 누른 뒤 JET 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스와 마스크 색이 JET로 변경됐는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭에서 Mask Viewing panel의 2D HT 화면의 톱니바퀴 버튼을 누르고 Color map 버튼을 누른 뒤 Rainbow 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스와 마스크 색이 Rainbow로 변경됐는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭에서 Mask Viewing panel의 2D HT 화면의 톱니바퀴 버튼을 누르고 Color map 버튼을 누른 뒤 Grayscale 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스와 마스크 색이 Grayscale로 변경됐는지 확인")
def step_impl():
  raise NotImplementedError

@given("Basic Analyzer[T] 탭에서 Use Labeling 체크박스가 체크되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E04\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E04'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E04'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E04\\E04.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(65, 15)
  tomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(128, 73)
  groupBox = widget.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox
  groupBox.procCombo.ClickItem("RI Thresholding")
  groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(12, 11)

@given("Basic Analyzer[T] 탭의 Mask Viewing panel에 마스크가 생성되어 있지 않다.")
def step_impl():
  Regions.TomoAnalysis5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@when("Basic Analyzer[T] 탭 Labeling 탭의 Select Option 드롭다운 버튼에서 Multi Label을 선택하고 Labeling 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  widget = scrollArea.qt_scrollarea_viewport.Widget
  widget.MouseWheel(-2)
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 120
  widget.CollapseWidget4.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer[T] 탭에서 경고창이 뜨며 아무런 변화도 없는지 확인")
def step_impl():
  NameMapping.Sys.TomoAnalysis.MessageBox9.qt_msgbox_buttonbox.buttonOk.ClickButton()
  Regions.TomoAnalysis6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@when("Basic Analyzer[T] 탭 Labeling 탭의 Select Option 드롭다운 버튼에서 Largest Label을 선택하고 Labeling 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Largest Label")
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  widget = scrollArea.qt_scrollarea_viewport.Widget
  widget.MouseWheel(-2)
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 120
  widget.CollapseWidget4.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  
@then("Basic Analyzer[T] 탭에서 경고창이 나타나며 아무런 변화도 없는지 확인")
def step_impl():
  NameMapping.Sys.TomoAnalysis.MessageBox9.qt_msgbox_buttonbox.buttonOk.ClickButton()
  Regions.TomoAnalysis7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@given("Basic Analyzer[T] 탭 Batch Run setting 탭의 Processing mode 패널에서 RI Thresholding이 프로세서로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E04\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E04'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E04'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E04\\E04.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base
  frame.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  frame.bt_square_primary.Click(66, 14)

@when("Batch Run setting 탭 리스트에 존재하는 모든 TCF에 Time points를 “{arg}, {arg}”로 설정하고 Processing mode 패널의 Execute Whole 버튼을 누른다.")
def step_impl(param1, param2):
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab
  widget.batchListPanel.scrollArea.qt_scrollarea_viewport.Widget.BaTimeBatchListWidget.tableWidget.ClickCell("1", "Selected time points")
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = widget.batchParameterPanel
  groupBox = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup
  spinBox = groupBox.endTime
  spinBox.qt_spinbox_lineedit.Drag(51, 14, -55, 0)
  spinBox.Keys("2")
  spinBox.wValue = 2
  groupBox.applyChkBtn.ClickButton()
  scrollArea = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 90
  scrollArea.qt_scrollarea_viewport.Widget.buttonExecuteWhole.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis

  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 140
  
  scrollArea.qt_scrollarea_viewport.Widget.buttonExecuteWhole.ClickButton()

@when("Batch Run setting 탭 위에 프로세싱 바 팝업의 진행이 완료될 때 까지 기다린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

@then("프로세싱 바 팝업과 Basic Analyzer[T] 탭이 사라지는 지 확인")
def step_impl():
  Regions.ET_5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@then("Report[T] 탭이 화면에 나타나는지 확인")
def step_impl():
  Regions.ET_6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)

@then("Report[T] 탭에 app을 적용한 hypercube의 데이터에 따른 table과 graph가 각 패널에 출력 되었는지 확인")
def step_impl():
  Regions.Widget.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.graphWidget.chartView.Widget)
  Regions.DataTab.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.dataTable.tabWidget.qt_tabwidget_stackedwidget.DataTab)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("3D Visualizer 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  img = r'E:\Regression_Test\*.png'
  for f1 in glob.glob(img):
    os.remove(f1)
  img1 = r'E:\Regression_Test\test\*.png'
  for f2 in glob.glob(img1):
    os.remove(f2)  
  dir4 =r'E:\Regression_Test\Prefix_File\E05\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E05'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E05'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E05\\E05.tcpro")

  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(78, 93)
  frame.bt_round_tool3.Click(100, 19)
  Delay(3000)
  
@given("Visualizer 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E04\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E04'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E04'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E04\\E04.tcpro")

  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(78, 93)
  frame.bt_round_tool3.Click(100, 19)

@given("3D Visualizer 탭 View 패널의 HT 이미지가 Inverse grayscale ColorMap으로 설정되어 있다.")
def step_impl():
  raise NotImplementedError

@given("3D Visualizer 탭 View 패널의 HT 이미지의 Level window에서 최솟값이 최댓값까지 올라가 있다.")
def step_impl():
  raise NotImplementedError

@when("3D Visualizer 탭 View 패널의 HT 이미지 캔버스의 우상단의 톱니바퀴 아이콘을 클릭하고 Reset View 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널 HT 이미지의 ColorMap이 Grayscale로, Level window가 초기 상태로 돌아왔는지 확인")
def step_impl():
  raise NotImplementedError

@given("3D Visualizer Modality 패널의 FL 체크박스가 체크 되어있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E05\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E05'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E05'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E05\\E05.tcpro")
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = widget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(141, 87)
  frame.bt_round_tool3.Click(56, 18)
  widget.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.ClickTab("FL")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox.ClickButton(cbChecked)

@when("Preset 패널 FL 탭 하단의 table에서 Blue 채널의 Opacity 셀을 선택한 뒤 50을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.channelTableWidget
  tableWidget.ClickCell("1", "Min")
  tableWidget.ClickCell("1", "Opacity")
  tableWidget.Keys("50")
  tableWidget.qt_scrollarea_viewport.ExpandingLineEdit.Keys("[Enter]")

@then("View 패널에서 파란색 형광 Rendering이 바뀌는지 확인")
def step_impl():
  Regions.TomoAnalysis8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널 FL 탭의 Blue Opacity 사이즈 바와 값이 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.blueSlider, "wPosition", cmpEqual, 50)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.blueSpinbox.qt_spinbox_lineedit, "wText", cmpEqual, "50")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Preset 패널 FL 탭 하단의 table에서 Green 채널의 Opacity 셀을 선택한 뒤 50을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.channelTableWidget
  tableWidget.ClickCell("2", "Opacity")
  tableWidget.Keys("50")
  tableWidget.qt_scrollarea_viewport.ExpandingLineEdit.Keys("[Enter]")

@then("View 패널에서 초록색 형광 Rendering이 바뀌는지 확인")
def step_impl():
  Regions.TomoAnalysis9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널 FL 탭의 Green Opacity 사이즈 바와 값이 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.greenSlider, "wPosition", cmpEqual, 50)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.greenSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "50")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Preset 패널 FL 탭 하단의 table에서 Red 채널의 Opacity 셀을 선택한 뒤 50을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.channelTableWidget
  tableWidget.ClickCell("3", "Opacity")
  tableWidget.Keys("50")
  tableWidget.qt_scrollarea_viewport.ExpandingLineEdit.Keys("[Enter]")

@then("View 패널에서 빨간색 형광 Rendering이 바뀌는지 확인")
def step_impl():
  Regions.TomoAnalysis10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널 FL 탭의 Red Opacity 사이즈 바와 값이 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.redSlider, "wPosition", cmpEqual, 50)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.redSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "50")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Preset 패널 TF canvas 우상단의 겹쳐진 사각형 아이콘을 클릭한다.")
def step_impl():
  TC_RetrieveDockWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner
  TC_RetrieveDockWidget.Click(249, 11)
  TC_RetrieveDockWidget.qt_dockwidget_floatbutton.Click(12, 8)

@then("분리된 TF canvas 팝업 창의 이름이 2D Transfer Function Canvas로 출력됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.dockwidget_inner, "QtText", cmpEqual, "2D Transfer Function Canvas")
  Aliases.TomoAnalysis.dockwidget_inner.DblClick(10,10)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Movie maker 패널의 Add 버튼을 누르고 Orbit 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.TC_TabBar.Click(188, 9)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.bt_round_gray700.ClickButton()
  Sys.Desktop.Mousey += 60;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  Aliases.TomoAnalysis.Menu4.Widget.bt_square_gray700.Click(-1)

@when("3D Visualizer 탭의 Movie maker 패널과 View 탭의 경계선에 마우스 포인터를 가져간 뒤 클릭한 채로 아래로 끝까지 내린다.")
def step_impl():
  splitter = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter
  splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer.Click(399, 716)
  splitter.qt_splithandle_.Drag(753, 3, -16, 214)

@then("Movie maker 패널의 세로 길이가 줄어드는 동안 탭의 텍스트가 서로 겹치지 않는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.qt_splithandle_.Drag(714, 3, 9, -58)
  Regions.ET_7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("3D Visualizer 탭 View 패널에 rendering이 생성되어 있다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(38, 212, 41, 43)
  Aliases.TomoAnalysis.QtObject("QColorDialog", "Select Color", 2).QtObject("QDialogButtonBox", "", 1).QtObject("QPushButton", "OK", 1).ClickButton()

@when("Movie maker 패널에서 Add 버튼을 눌러 Orbit 효과를 추가하고 효과 옵션 막대의 길이를 2초까지 늘린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.TC_TabBar.Click(180, 16)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.bt_round_gray700.ClickButton()
  Sys.Desktop.Mousey += 60;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  menu = tomoAnalysis.Menu
  Aliases.TomoAnalysis.Menu4.Widget.bt_square_gray700.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents.editWidget.table_tracks.qt_scrollarea_viewport.TomoAnalysis_Viewer_Plugins_RecordActionItem.handle_track_cut_right.Drag(6, 15, 78, -1)

@when("Movie maker 패널의 Record 버튼을 누르고 Multi-view 버튼을 누른 뒤 파일 경로를 지정하고 파일 이름을 “e1”로 지정한다.")
def step_impl():
  raise NotImplementedError

@when("3D Visualizer를 종료한다.")
def step_impl():
  raise NotImplementedError

@when("Preview 패널에서 다른 TCF를 선택한 뒤 3D Visualization 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 TF canvas 탭에서 TF box를 그려 View 패널에 rendering을 생성한다.")
def step_impl():
  raise NotImplementedError

@when("Movie maker 패널의 Record 버튼을 누르고 Multi-view 버튼을 누른 뒤 파일 경로를 지정하고 파일 이름을 “e2”로 지정한다.")
def step_impl():
  raise NotImplementedError

@when("Preview 패널에서 또 다른 TCF를 선택한 뒤 3D Visualization 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Movie maker 패널의 Record 버튼을 누르고 Multi-view 버튼을 누른 뒤 파일 경로를 지정하고 파일 이름을 “e3”으로 지정한다.")
def step_impl():
  raise NotImplementedError

@then("지정한 경로에 생성된 “e1”, “e2”, “e3” 동영상 파일이 모두 2초 길이로 생성되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Screen shot 패널의 3D View 탭에서 All plane 버튼을 누르고 파일 탐색기의 위 화살표를 누른 뒤 폴더 선택 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.btn_.ClickButton()


@when("Screen shot 패널의 3D View 탭에서 All plane 버튼을 다시 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.ScreenShotPanel.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view3DTab.bt_round_gray700.ClickButton()

@then("파일 탐색기 팝업에 방금 All plane screen shot을 저장했던 폴더가 나타났는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.dlg2.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar_ERegressionTest, "WndCaption", cmpEqual, "주소: E:\\Regression_Test")
  tomoAnalysis = Aliases.TomoAnalysis
  Aliases.TomoAnalysis.dlg2.Window("Button", "취소", 2).Click()
  tomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Movie maker 패널의 Add 버튼을 누르고 Slice 버튼을 누른다.")
def step_impl():
  dir3 = r'C:\Users\HP\Desktop\*.mp4'
  for f in glob.glob(dir3):
      os.remove(f)
  tomoAnalysis = Aliases.TomoAnalysis
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.TC_TabBar.Click(188, 9)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.bt_round_gray700.ClickButton()
  Sys.Desktop.Mousey += 90;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  Aliases.TomoAnalysis.Menu4.Widget.bt_square_gray700.Click(76, 11)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(600, 11)
  
@when("Movie maker 패널의 Record 버튼을 누르고 Multi-view 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(109, 19)
  Aliases.TomoAnalysis.QtObject("QMenu", "", 1).Click(20, 200)

@when("파일 탐색기 팝업에서 바탕 화면 버튼을 누른 뒤 저장 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  progress = Aliases.TomoAnalysis.dlgSaveVideoFile.WorkerW.ReBarWindow32.AddressBandRoot.progress
  progress.BreadcrumbParent.toolbar_C.Click(284, 27)
  comboBox = progress.comboBox
  comboBox.SetText("")
  edit = comboBox.ComboBox.Edit
  comboBox.SetText("바탕 화면")
  edit.Keys("[Enter]")
  Aliases.TomoAnalysis.dlgSaveVideoFile.Window("Button", "저장(&S)", 1).ClickButton()



  
@when("녹화가 완료되면 다시 한 번 Movie maker 패널의 Record 버튼을 누르고 Multi-view 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(109, 19)
  Aliases.TomoAnalysis.QtObject("QMenu", "", 1).Click(20, 200)

@then("파일 탐색기 팝업에 바탕 화면이 나타났는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.dlgSaveVideoFile.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar_, "WndCaption", cmpEqual, "주소: 바탕 화면")
  Aliases.TomoAnalysis.dlgSaveVideoFile.Window("Button", "취소", 2).ClickButton()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Viewing tool 패널의 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Show TimeStamp 체크박스를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round2.ClickButtonXY(85, 18)
  Sys.Desktop.Mousey += 160;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.Menu4.CheckBox2.ClickButton(cbChecked)

@then("Viewing tool 패널 View 드롭다운 버튼의 TimeStamp 버튼에서 Change Color 텍스트 앞에 흰 색 네모가 있는지 확인")
def step_impl():
  Regions.Menu.Check(Aliases.TomoAnalysis.Menu4)

@when("Viewing tool 패널의 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Change Color 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round2.ClickButtonXY(85, 18)
  Sys.Desktop.Mousey += 170;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  aqUtils.Delay(200)
  Sys.Desktop.Mousex += 220;
  Sys.Desktop.Mousey += 30;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  Sys.Desktop.MouseUp(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  
@when("팝업에서 검은 색을 선택하고 OK 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.ColorDialog.WellArray.Click(17, 9)
  Aliases.TomoAnalysis.ColorDialog.DialogButtonBox.buttonOk.Click(-1)


@then("Viewing tool 패널 View 드롭다운 버튼의 TimeStamp 버튼에서 Change color 텍스트 앞에 검은 색 네모가 있는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round2.ClickButtonXY(85, 18)
  Sys.Desktop.Mousey += 170;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  aqUtils.Delay(200)
  Sys.Desktop.Mousex += 220;
  Sys.Desktop.Mousey += 30;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  Regions.Menu1.Check(Aliases.TomoAnalysis.Menu4)

  
@given("TCF가 프로젝트에 불러져 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E05\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E05'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E05'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E05\\E05.tcpro")


@when("Preview 패널에서 Blue, Green FL 정보가 있는 TCF를 선택하고 3D visualization 버튼을 누른다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = widget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(141, 87)
  frame.bt_round_tool3.Click(56, 18)

@when("3D Visualizer 탭 Preset 패널의 FL 탭을 누른다.")
def step_impl():
  
  
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.ClickTab("FL")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox.ClickButton(cbChecked)
  
@then("3D Visualizer 탭 Preset 패널의 FL 탭에서 Blue, Green 체크박스에 체크 되어 있고 Red 체크박스는 비활성화 되어있는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.redCheckBox, "Enabled", cmpEqual, False)

@when("3D Visualizer 탭을 종료한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar.CloseButton.Click(7, 9)

@when("Preview 패널에서 Green, Red FL 정보가 있는 TCF를 선택하고 3D visualization 버튼을 누른다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(137, 146)
  frame.bt_round_tool3.Click(31, 12)

@then("3D Visualizer 탭 Preset 패널의 FL 탭에서 Green, Red 체크박스에 체크 되어 있고 Blue 체크박스는 비활성화 되어있는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.blueCheckBox, "Enabled", cmpEqual, False)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Preset 패널의 TF canvas에서 rendering의 TF box에 마우스 포인터를 가져간 뒤 우클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.ClickR(58, 239)

@then("3D Visualizer 탭 View 패널에서 우클릭한 TF box의 rendering이 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널 TF canvas에서 우클릭 한 TF box의 테두리가 점선으로 바뀌었는지 확인")
def step_impl():
  Regions.Widget1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget, False, False, 4853, 17)

@when("Preset 패널의 TF canvas에서 비활성화 된 TF box에 마우스 포인터를 가져간 뒤 우클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.ClickR(65, 230)

@then("3D Visualizer 탭 View 패널에서 우클릭한 TF box의 rendering이 나타났는지 확인")
def step_impl():
  Regions.TomoAnalysis12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널 TF canvas에서 우클릭 한 TF box의 테두리가 점선에서 실선으로 바뀌었는지 확인")
def step_impl():
  Regions.Widget2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget, False, False, 4853, 17)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("3D Visualizer 탭 View 패널의 3D 화면에 마우스 포인터를 가져간 채로 마우스 휠을 두 칸 위로 올린다.")
def step_impl():

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(900,500)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.MouseWheel(2)

@then("3D Visualizer 탭 View 패널에서 3D 화면의 rendering이 두 칸 조금씩 확대됐는지 확인")
def step_impl():
  Regions.ET_30.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("3D Visualizer 탭 View 패널의 3D 화면에 마우스 포인터를 가져간 채로 마우스 휠을 두 칸 아래로 내린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(900,500)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.MouseWheel(-2)
  
@then("3D Visualizer 탭 View 패널에서 3D 화면의 rendering이 두 칸 조금씩 축소됐는지 확인")
def step_impl():
  Regions.ET_31.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Application tab 바에서 Project Manager 탭을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar.Click(113, 10)

@when("Preview 패널에서 새로운 TCF를 선택하고 3D Visualization 버튼을 누른다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(128, 85)
  frame.bt_round_tool3.Click(72, 19)

@then("기존 3D Visualizer 내용이 사라지고 빈 3D Visualizer가 새로 열리는지 확인")
def step_impl():
  Regions.ET_8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("3D Visualizer 탭 View 패널과 Viewing tool 패널 사이의 경계에 마우스 포인터를 가져가서 클릭한 채로 오른쪽 끝까지 드래그한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.Drag(353, 393, 1435, 65)

@when("Viewing tool 패널의 Layout 드롭다운 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round.ClickButtonXY(678, 13)

@then("Viewing tool 패널의 Layout 드롭다운 버튼의 너비와 클릭 시 나오는 Layout 선택 창의 너비가 같은지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round, "Width", cmpEqual, 731)

  Regions.ET_32.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow)


@when("Viewing tool 패널의 View 드롭다운 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round2.ClickButtonXY(381, 6)

@then("Viewing tool 패널의 View 드롭다운 버튼의 너비와 클릭 시 나오는 View 선택 창의 너비가 같은지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round2, "Width", cmpEqual, 731)
  Regions.ET_33.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(889, 14)





@when("Preset 패널 FL 탭에서 형광 Opacity 수치 칸에 50을 입력한다.")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 FL 탭에서 형광 Opacity 칸의 위 화살표를 누른다.")
def step_impl():
  raise NotImplementedError

@then("Preset 패널 FL 탭에서 형광 Opacity 값이 51로 바뀌었는지 확인")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널에서 형광 rendering이 급격하게 밝아지지 않았는지 확인")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 HT 탭에서 rendering의 TF box를 선택하고 Opacity 수치 칸에 .31을 입력한다.")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 HT 탭 Opacity 칸의 위 화살표를 누른다.")
def step_impl():
  raise NotImplementedError

@then("Preset 패널 HT 탭의 Opacity 값이 0.32로 바뀌었는지 확인")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널에서 HT rendering이 급격하게 밝아지지 않았는지 확인")
def step_impl():
  raise NotImplementedError

@given("3D Visualization 탭이 HT only 데이터로 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  img = r'E:\Regression_Test\*.png'
  for f1 in glob.glob(img):
    os.remove(f1)
  img1 = r'E:\Regression_Test\test\*.png'
  for f2 in glob.glob(img1):
    os.remove(f2)  
  dir4 =r'E:\Regression_Test\Prefix_File\E05\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E05'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E05'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E05\\E05.tcpro")
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget2.TC_TCF2DWidget.panel_contents_image.Widget.Click(132, 89)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool3.Click(48, 14)

@when("3D Visualization 탭을 종료한다.")
def step_impl():
  closeButton = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar

  closeButton.Click(290,25)

  
@when("Preview 패널에서 HT+FL 데이터를 선택하고 3D visualization 버튼을 누른다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(-1)
  frame.bt_round_tool3.Click(64, 20)

@when("3D Visualizer 탭이 열리면 Modality 패널에서 FL 체크박스를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox.ClickButton(cbChecked)

@then("3D Visualizer 탭 View 패널의 2D HT 화면들의 HT 이미지와 FL rendering이 정상적으로 출력 되는지 확인")
def step_impl():
  Regions.ET_34.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("3D Visualizer 탭 View 패널의 2D XY HT 화면에 마우스 포인터를 대고 마우스 휠을 아래로 5칸 내린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer.Widget.Splitter.Splitter.TomoAnalysis_Viewer_Plugins_ViewerRenderWindow2d.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(222, 172)
  tomoAnalysis.MouseWheel(-1)
  tomoAnalysis.MouseWheel(-1)
  tomoAnalysis.MouseWheel(-1) 
  tomoAnalysis.MouseWheel(-1)
  tomoAnalysis.MouseWheel(-1)

@then("3D Visualizer 탭 View 패널의 2D XY HT 이미지 slice가 변했는지 확인")
def step_impl():
  Regions.ET_35.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@given("3D Visualizer 탭의 View 패널에 Rendering이 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E05\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E05'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E05'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E05\\E05.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = widget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(175, 70)
  frame.bt_round_tool3.Click(20, 15)
  widget.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(30, 210, 33, 49)
  Aliases.TomoAnalysis.ColorDialog.DialogButtonBox.buttonOk.ClickButton()



@given("Movie maker 탭에 Orbit 효과 옵션이 추가되어 있다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.TC_TabBar.Click(180, 16)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.bt_round_gray700.ClickButton()
  Sys.Desktop.Mousey += 40;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  Aliases.TomoAnalysis.Menu4.Widget.bt_square_gray700.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(794, 26)

@given("PC 바탕 화면에 “테스트 데이터” 폴더가 생성되어 있따.")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭의 Record 버튼을 누르고 Multi-view 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(55, 16)
  NameMapping.Sys.TomoAnalysis.Menu2.QtMenu.Click("Multi-view")

@when("파일 탐색기에서 저장 경로를 “테스트 데이터” 폴더로 지정하고 이름을 입력한 뒤 저장 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  progress = tomoAnalysis.dlgSaveVideoFile.WorkerW.ReBarWindow32.AddressBandRoot.progress
  progress.BreadcrumbParent.toolbar_C.Click(308, 11)
  comboBox = progress.comboBox
  comboBox.SetText("C:\\Users\\HP\\Desktop\\테스트 데이터")
  comboBox.ComboBox.Edit.Keys("[Enter]")
  Aliases.TomoAnalysis.dlgSaveVideoFile.Window("Button", "저장(&S)", 1).ClickButton()


@then("3D Visualizer 탭에 “Target movie path contains non-english characters” 경고창이 나타났는지 확인")
def step_impl():
  Aliases.TomoAnalysis.QtObject("QMessageBox", "Target movie path contains non-english characters", 2).QtObject("qt_msgbox_buttonbox").QtObject("QPushButton", "OK", 1).ClickButton()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("View 패널의 XY HT Slice 화면에서 톱니바퀴 버튼을 누르고 RI Picker 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis2
  tomoAnalysis.Click(275, 17)
  tomoAnalysis = Aliases.TomoAnalysis
  Aliases.TomoAnalysis.QtObject("QMenu", "", 1).Click(50,187)

@when("View 패널의 XY HT Slice 화면에서 마우스를 클릭한 채로 드래그한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis2 = tomoAnalysis.mainwindow.TomoAnalysis2
  tomoAnalysis2.Drag(110, 97, 43, 43)
  tomoAnalysis2.Drag(113, 94, 43, 44)
  tomoAnalysis.DialogButtonBox.buttonOk.ClickButton()


@when("팝업에서 흰색을 선택하고 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.DialogButtonBox.buttonOk.ClickButton()

@then("View 패널 3D 화면에 흰색 Rendering이 생성되었는지 확인")
def step_impl():
  Aliases.mspaint.wndMSPaintApp.MSPaintView.Afx.Drag(697, 344, -25, 15)

@given("3D Visualizer View 패널의 XY HT 이미지에 Scalar bar가 생성되어 있다.")
def step_impl():
  raise NotImplementedError

@when("3D Visualizer 탭 View 패널 XY HT 이미지의 톱니바퀴 버튼을 누른 뒤 ScalarBar 버튼을 누르고 Move ScalarBar 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("3D Visualizer 탭 View 패널 XY HT 이미지에서 빨갛게 표시된 Scalar bar를 클릭한 채로 오른쪽에서 왼쪽으로 드래그한다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널 XY HT 이미지에 생성된 Scalar bar가 움직였는지 확인")
def step_impl():
  raise NotImplementedError

@when("키보드의 Esc 키를 누른다.")
def step_impl():
  raise NotImplementedError

@when("3D Visualizer 탭 View 패널 XY HT 이미지의 톱니바퀴 버튼을 누른 뒤 ScalarBar 버튼을 누르고 Scale ScalarBar 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("3D Visualizer 탭 View 패널 XY HT 이미지에서 녹색으로 표시된 Scalar bar를 클릭한 채로 왼쪽 아래에서 오른쪽 위로 드래그한다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널 XY HT 이미지에 생성된 Scalar bar가 확대되었는지 확인")
def step_impl():
  raise NotImplementedError

@then("Application Parameter 패널이 비활성화 되었는지 확인한다")
def step_impl():
  Regions.processorParamFrame.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@then("화살표에 따라 Basic Analyzer 탭 Mask Viewing panel의 Opacity 수치 값이 0.50으로 내려감을 확인한다")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity, "wValue", cmpEqual, 0.5)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("BA Application Parameter에서 프로세서로 RI Thresholding이 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E03\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E03'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E03'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E03\\E03.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.operationPanel.panel_base
  widget = frame.widget_panel
  widget2 = widget.AddPGPanel.widget_panel2
  lineEdit = widget2.input_high
  lineEdit.Click(190, 24)
  lineEdit.SetText("PG")
  widget2.bt_round_operation.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = widget.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(86, 19)
  lineEdit.SetText("HC")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  widget.CubePanel.bt_round_operation.ClickButton()
  frame.frame_tab.bt_round_tab5.ClickButton()
  frame = splitter.previewPanel.panel_base
  frame2 = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(164, 61)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(102, 75)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget2.TC_TCF2DWidget.panel_contents_image.Widget.Click(151, 97)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget4.TC_TCF2DWidget.panel_contents_image.Widget.Click(111, 97)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget5.TC_TCF2DWidget.panel_contents_image.Widget.Click(67, 97)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget6.TC_TCF2DWidget.panel_contents_image.Widget.Click(54, 80)
  frame.bt_round_tool.Click(76, 13)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  widget.SelectAppPanel.bt_round_operation.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

@given("3D Visualizer Modality 패널의 FL 체크가 체크 되어있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E05\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E05'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E05'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  splitter = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter
  splitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(89, 25)
  tomoAnalysis.Menu.OpenFile("E:\\Regression_Test\\Prefix_File\\E05\\E05.tcpro")
  tomoAnalysis_ProjectManager_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(-1)
  frame.bt_round_tool3.Click(56, 15)
  tomoAnalysis_ProjectManager_AppUI_MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox.ClickButton(cbChecked)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.ClickTab("FL")

@then("TA종료")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Screen shot 패널의 3D View 탭에서 All axis 버튼을 다시 한 번 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.ScreenShotWidgetUI.panel_contents.tabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(140, 29)
  TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.view3DTab.bt_round_gray700.ClickButton()
  tomoAnalysis.Menu.WorkerW.ReBarWindow32.UpBand.toolbar.ClickItem(256, False)
  tomoAnalysis.btn_.ClickButton()

@when("Screen shot 패널의 3D View 탭에서 All axis 버튼을 누르고 파일 탐색기의 위 화살표를 누른 뒤 폴더 선택 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.ScreenShotWidgetUI.panel_contents.tabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(157, 24)
  TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.view3DTab.bt_round_gray700.ClickButton()
  tomoAnalysis.dlg2.WorkerW.ReBarWindow32.UpBand.toolbar.ClickItem(256, False)
  tomoAnalysis.btn_.ClickButton()

@then("TA종료한다")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(842, 28)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("3D Visualizer 탭 TF c anvas에 비활성화 상태의 TF box가 만들어져 있다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget
  widget.Drag(59, 187, 96, 66)
  Aliases.TomoAnalysis.ColorDialog.WellArray.Click(77, 60)
  Aliases.TomoAnalysis.ColorDialog.DialogButtonBox.buttonOk.ClickButton()
  widget.Click(89, 110)
