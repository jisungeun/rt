﻿Feature: 3D Visualizer

  Scenario: 2D TF Canvas 패널을 팝업으로 뽑고 TA 종료할 때 팝업이 같이 종료
Given 3D Visualizer 탭이 열려 있다.

When Preset 패널에서 TF canvas 우상단의 겹쳐진 사각형 아이콘을 클릭한다.

And TA 창의 X 버튼을 누른다.

Then TA 창과 2D TF Canvas 팝업이 모두 종료되었는지 확인