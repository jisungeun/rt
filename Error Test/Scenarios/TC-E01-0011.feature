﻿Feature: Playground를 기존에 존재하는 다른 Playground의 이름으로 Rename할 때 이름이 변경되지 않음

  Scenario: Playground를 기존에 존재하는 다른 Playground의 이름으로 Rename할 때 이름이 변경되지 않음
Given “A”라는 이름의 playground가 생성되어 있다.

And “B” 라는 이름의 playground가 생성되어 있다.

When Playground Setup 패널에서 “B” playground를 선택하고 Rename 버튼을 누른다.

And 팝업에서 변경할 이름으로 “A”를 입력하고 OK 버튼을 누른다.

Then 에러 팝업이 나타나며 Playground의 이름이 변경되지 않음을 확인