﻿Feature: BAT AI Segmentation

  Scenario: BAT Single Run 탭에서 Basic Measurement 탭의 Execute 버튼을 눌러 Cell 정량 분석을 실행하는 도중 팝업에서 Cancel 버튼을 눌러 실행 취소
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭 Basic Measurement의 table의 parameter를 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.

And 프로세싱 바 팝업이 나타나면 팝업의 Cancel 버튼을 누른다.

Then Basic Analyzer[T] 탭의 프로세싱 바 팝업이 종료됐는지 확인

And 작업 중이던 time point 까지 정량 분석 결과 Measurement 탭이 생성됐는지 확인

Then TA종료
