﻿Feature: TC-E01-0080

  Scenario: Cube에서 TCF를 unlink할 때, 팝업의 드롭다운 버튼에 TCF가 속해 있는 cube 이름만 나타남
Given Playground에 “A”, “B” 두 개의 cube가 만들어져 있다.

And “A” cube에 어떤 TCF도 링크되어 있지 않다.

And “B” cube에 TCF가 link되어 있다.

When Preview 패널에서 “B” cube에 link된 TCF를 클릭하고 Unlink from cube 버튼을 누른다.

And 팝업의 cube 드롭다운 버튼을 누른다.

Then 팝업의 cube 드롭다운 버튼에 “A” cube는 나타나지 않고 “B” cube만 나타났는지 확인