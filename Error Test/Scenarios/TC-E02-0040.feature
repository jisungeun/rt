﻿Feature: 	BA AI Segmentation

  Scenario: Border kill 옵션 선택 후 Batch run 수행시, mask 정상적으로 생성

Given “Hep G2-094” TCF 파일이 들어간 프로젝트 파일이 열려있다.

When Preview 패널에서 TCF 썸네일을 우클릭 하고 Create Simple Playground 버튼을 누른다.

And Select Application 탭에서 Basic Analyzer을 선택하고 OK를 누른다.

And Application Parameter에서 AI Segmentation을 선택한뒤, Use Border Kill을 체크하고 Batch Run을 클릭한다.

And 프로세싱 바 팝업이 생기면 진행이 완료되고 팝업이 사라질 때 까지 기다린다.

And Report 탭의 오른쪽, Graph of individual cell 패널에서 1_1막대그래프를 선택하고 Edit Mask를 누른다.

Then Mask Editor 탭 Mask selector 패널의 5개 버튼이 모두 활성화 되는지 확인

Then TA종료