﻿Feature: Project Manager

  Scenario: TA를 종료하지 않고 프로젝트를 바꿔도 이전 프로젝트에 있던 Hypercube 정보가 남아있지 않음

Given Playground에 “A” 라는 이름의 Hypercube가 생성되어 있다.

When Select Project 탭의 Open 버튼을 누른다.

And 파일 탐색기에서 “A” 라는 이름의 Hypercube가 없는 다른 프로젝트를 선택한다.

And 새로 열린 프로젝트의 Select Application 탭에서 App을 적용할 hypercube를 선택하는 드롭다운 버튼을 클릭한다.

Then Select Application 드롭다운 버튼에 “A” hypercube가 없는지 확인