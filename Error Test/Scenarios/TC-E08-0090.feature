﻿Feature:	3D Visualizer

  Scenario: FL 데이터를 3D Visulizer로 열었을 때 데이터에 없는 색 형광이 비활성화
Given TCF가 프로젝트에 불러져 있다.

When Preview 패널에서 Blue, Green FL 정보가 있는 TCF를 선택하고 3D visualization 버튼을 누른다.

And 3D Visualizer 탭 Preset 패널의 FL 탭을 누른다.

Then 3D Visualizer 탭 Preset 패널의 FL 탭에서 Blue, Green 체크박스에 체크 되어 있고 Red 체크박스는 비활성화 되어있는지 확인

When 3D Visualizer 탭을 종료한다.

And Preview 패널에서 Green, Red FL 정보가 있는 TCF를 선택하고 3D visualization 버튼을 누른다.

And 3D Visualizer 탭 Preset 패널의 FL 탭을 누른다.

Then 3D Visualizer 탭 Preset 패널의 FL 탭에서 Green, Red 체크박스에 체크 되어 있고 Blue 체크박스는 비활성화 되어있는지 확인