﻿Feature: 3D Visualizer

  Scenario: 존재하지 않는 FL 채널 테이블에 값이 입력되지 않음
Given HeLa-Rab5a-Rab7a-005 데이터로 3D Visualizer가 열려 있다.

When Modality 패널에서 FL 체크박스를 클릭하고 Preset 패널에서 FL 탭을 클릭한다.

And Preset 패널 FL 탭의 테이블에서 Blue channel의 Max 칸을 더블 클릭하고 키보드로 100을 입력한 뒤 키보드의 엔터 키를 누른다.

Then 값을 입력한 칸에 100이 입력되지 않고 빈칸으로 남아있는지 확인

Then TA종료