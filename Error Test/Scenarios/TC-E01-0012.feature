﻿Feature: Project Manager

  Scenario: Hypercube를 기존에 존재하는 다른 Hypercube의 이름으로 Rename할 때 이름이 변경되지 않음
Given “alpha”라는 이름의 Hypercube가 생성되어 있다.

And “beta” 라는 이름의 Hypercube가 생성되어 있다.

When Playground canvas 패널에서 “beta” Hypercube를 우클릭하고 Rename Hypercube 버튼을 누른다.

And 팝업에서 변경할 이름으로 “alpha”를 입력하고 OK 버튼을 누른다.

Then 에러 팝업이 나타나며 Hypercube의 이름이 변경되지 않음을 확인