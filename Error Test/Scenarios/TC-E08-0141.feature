﻿"Feature: 3D Visualizer

  Scenario: 3D Visualizer에서 HT Opacity 조절 시 매 값마다 균일하게 조절
Given 3D Visualizer 탭이 열려 있다.

And 3D Visualizer 탭 View 패널에 rendering이 생성되어 있다.

When Preset 패널 HT 탭에서 rendering의 TF box를 선택하고 Opacity 수치 칸에 .31을 입력한다.

And Preset 패널 HT 탭 Opacity 칸의 위 화살표를 누른다.

Then Preset 패널 HT 탭의 Opacity 값이 0.32로 바뀌었는지 확인

And 3D Visualizer 탭 View 패널에서 HT rendering이 급격하게 밝아지지 않았는지 확인