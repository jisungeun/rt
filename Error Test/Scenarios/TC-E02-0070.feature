﻿Feature: BA AI Segmentation

  Scenario: BA AI Segmentation Mask가 생성되어 있을 때, Mask Viewing panel 설정에서 View X/Y/Z를 눌러도 마스크가 사라지지 않음
Given Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Analyzer 탭 Mask Viewing panel에서 3D 마스크 화면의 톱니바퀴 버튼을 누르고 View 버튼을 누른 뒤 View X 버튼을 누른다.

Then Basic Analyzer 탭 Mask Viewing panel에서 마스크가 사라지지 않고 3D 마스크가 X축 방향으로 돌아갔는지 확인

When Basic Analyzer 탭 Mask Viewing panel에서 3D 마스크 화면의 톱니바퀴 버튼을 누르고 View 버튼을 누른 뒤 View Y 버튼을 누른다.

Then Basic Analyzer 탭 Mask Viewing panel에서 마스크가 사라지지 않고 3D 마스크가 Y축 방향으로 돌아갔는지 확인

When Basic Analyzer 탭 Mask Viewing panel에서 3D 마스크 화면의 톱니바퀴 버튼을 누르고 View 버튼을 누른 뒤 View Z 버튼을 누른다.

Then Basic Analyzer 탭 Mask Viewing panel에서 마스크가 사라지지 않고 3D 마스크가 Z축 방향으로 돌아갔는지 확인