﻿Feature: BAT AI Segmentation

  Scenario: BAT Single Run 탭에서 AI Inference 탭의 Execute 버튼을 클릭하여 Mask Viewing panel에 Mask를 생성하는 도중 작업 취소
Given Basic Analyzer[T] Single Run 탭에서 AI Segmentation이 선택되어 있다.

When Basic Analyzer[T] 탭 AI Inference 탭의 Execute 버튼을 클릭한다.

And 프로세싱 바 팝업이 생기면 팝업의 Cancel 버튼을 누른다.

Then Basic Analyzer[T] 탭의 프로세싱 바 팝업이 종료됐는지 확인

And 작업 중이던 time point까지의 마스크만 생성되었는지 확인