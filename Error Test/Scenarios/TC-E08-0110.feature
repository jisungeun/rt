﻿Feature: 3D Visualizer

  Scenario: 3D Visualizer 탭 View 패널에서 3D rendering을 마우스 휠로 줌 인/아웃
Given 3D Visualizer 탭이 열려 있다.

And 3D Visualizer 탭 View 패널에 rendering이 생성되어 있다.

When 3D Visualizer 탭 View 패널의 3D 화면에 마우스 포인터를 가져간 채로 마우스 휠을 두 칸 위로 올린다.

Then 3D Visualizer 탭 View 패널에서 3D 화면의 rendering이 두 칸 조금씩 확대됐는지 확인

When 3D Visualizer 탭 View 패널의 3D 화면에 마우스 포인터를 가져간 채로 마우스 휠을 두 칸 아래로 내린다.

Then 3D Visualizer 탭 View 패널에서 3D 화면의 rendering이 두 칸 조금씩 축소됐는지 확인

Then TA종료