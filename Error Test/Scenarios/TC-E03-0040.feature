﻿Feature: BA RI Thresholding

  Scenario: Border kill 수행한 데이터 다시 열기
Given Basic Analyzer Single Run 탭에 Hep G2-094 데이터의 RI 마스크가 열려 있다.

When Basic Analyzer 탭 Labeling 탭에서 Use Labeling을 체크하고 Option을 Multi Labels로 선택한 뒤 Labeling 탭의 Execute 버튼을 클릭한다.

And Basic Analyzer 탭 Border Kill 탭에서 Use Border Kill 체크박스를 체크하고 Border Kill 탭의 Execute를 누른다.

And Basic Analyzer 탭을 종료한 뒤 다시 Application parameter의 Single RUN 버튼을 누른 뒤 Hep G2-094 데이터를 선택한다.

Then 빈 Basic Analyzer 탭이 열렸는지 확인

Then TA종료
