﻿Feature: Project Manager

  Scenario: Cube 생성 시 이름에 특수문자 입력 불가
Given Create Cube 탭이 열려있다.

When Create Cube 탭의 cube 이름을 지정하는 칸에 "? , < > / ; : ' “ \ | [ { ] } ! @ # $ % ^ & * ( ) + ="를 키보드로 입력한다.

Then Create Cube 탭의 cube 이름을 지정하는 칸에 아무것도 입력되지 않았는지 확인