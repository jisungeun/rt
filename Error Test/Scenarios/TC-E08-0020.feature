﻿Feature: 3D Visualizer

  Scenario: Preset 패널 하단의 table에서 파란색 형광 Rendering의 Opacity를 조절
Given 3D Visualizer Modality 패널의 FL 체크박스가 체크 되어있다.

When Preset 패널 FL 탭 하단의 table에서 Blue 채널의 Opacity 셀을 선택한 뒤 50을 입력하고 키보드의 엔터 키를 누른다.

Then View 패널에서 파란색 형광 Rendering이 바뀌는지 확인

And Preset 패널 FL 탭의 Blue Opacity 사이즈 바와 값이 바뀌는지 확인