﻿Feature: BA AI Segmentation

  Scenario: Single run 팝업의 Cancel 버튼으로 팝업 닫기
Given BA Application Parameter에서 AI Segmentation이 프로세서로 선택되어 있다.

When BA Application Parameter에서 Single RUN 버튼을 누른다.

And Single run 팝업의 Cancel 버튼을 누른다.

Then Single run 팝업이 닫히는지 확인