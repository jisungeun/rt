﻿Feature: 	BA AI Segmentation

  Scenario: AI Segmentation 정량분석 도중 핵 마스크를 찾지 못해도 TA가 crash 되지 않음

Given Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Analyzer 탭의 Mask Correction 버튼을 누른다.

And ID manager 패널의 Copy 버튼을 누르고 팝업에 ‘e02’를 입력한 뒤 OK 버튼을 누른다.

And Mask selector 패널에서 Nucleus 버튼을 누른 뒤 Clean tool의 Flush slice 버튼을 누르고 XY slice 화면의 핵 마스크를 클릭한다.

And Mask selector 패널에서 Nucleus 버튼을 다시 한 번 누른 뒤 Apply to Result 버튼을 누른다.

And Basic Analyzer 탭의 toolbox에서 Nucleus 버튼을 클릭한다.

Then Basic Analyzer 탭 Mask Viewing panel에 아무런 마스크도 없는지 확인

When Basic Analyzer 탭의 Basic Measurement 탭에서 Execute 버튼을 누른다.

Then Measurement 팝업창이 BA탭이 있는 창 위에 나타나는지 확인

Then TA종료