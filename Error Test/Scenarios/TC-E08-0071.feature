﻿Feature: 3D Visualizer

  Scenario: 3D Visualizer 탭 Movie maker 패널에서 Record 버튼을 누르면 파일 탐색기에 이전 이미지 저장 경로가 나타남
Given 3D Visualizer 탭이 열려 있다.

When Movie maker 패널의 Add 버튼을 누르고 Slice 버튼을 누른다.

And Movie maker 패널의 Record 버튼을 누르고 Multi-view 버튼을 누른다.

And 파일 탐색기 팝업에서 바탕 화면 버튼을 누른 뒤 저장 버튼을 누른다.

And 녹화가 완료되면 다시 한 번 Movie maker 패널의 Record 버튼을 누르고 Multi-view 버튼을 누른다.

Then 파일 탐색기 팝업에 바탕 화면이 나타났는지 확인