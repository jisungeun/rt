﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Movie player에서 아래 화살표를 이용하여 화면에 보이는 이미지의 time point를 내리기
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

And Basic Analyzer[T] 탭의 Mask Viewing panel에 time point 3의 이미지가 나타나있다.

When Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자 칸 옆의 아래 화살표 버튼을 한 번 누른다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel에 나타나는 이미지가 time point 2의 이미지로 바뀌었는지 확인

And Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자가 2로 내려갔는지 확인