﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, UL Threshold 탭에서 아래 화살표 버튼을 눌러 lower threshold 수치를 내리기
Given BA Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When ‘Assign lower threshold’ 수치 칸 옆의 아래 방향 화살표를 누른다.

Then 1과 2 사이에서 ‘Assign lower threshold’ 의 수치가 내려가는지 확인
