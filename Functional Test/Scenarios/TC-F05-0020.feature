﻿Feature: Create Cube

  Scenario: Create Cube 탭에서 Cube를 생성
Given 한 개 이상의 cube가 생성되어 있다.

When Playground canvas 위의 Normal cube 아이콘을 누른다.

Then Playground canvas 위의 Normal cube 아이콘이 Active cube 아이콘으로 바뀜을 확인

And Preview 패널의 Title이 선택한 cube 이름으로 바뀜을 확인

And 표시되는 TCF도 cube 내의 데이터로 바뀜을 확인

When Cube 아이콘 바깥의Playground canvas 아무 영역이나 누른다.

Then Playground canvas 위의 Active cube 아이콘이 Normal cube 아이콘으로 바뀜을 확인

And  Preview 패널의 Title이 Playground 이름으로 바뀜을 확인한다.

And Preview 패널에 표시되는 TCF가 Playground 내의 전체 TCF로 바뀜을 확인한다

Then TA종료