﻿Feature: Mask Editor; From empty mask

  Scenario: Report[T] 탭의 그래프에서 개별 셀 선택 후 Mask Editor에서 마스크를 수정하여 그래프에 반영
Given Mask Editor 탭에 Time-lapse TCF가 열려 있다.

When Mask Editor 탭 하단 타임 바 오른쪽의 위 화살표 버튼을 한 번 누른다.

Then Mask Editor 탭에 나타나는 TCF 이미지가 time step 2의 이미지로 바뀌었는지 확인

And Mask Editor 탭의 타임 바 오른쪽의 숫자가 2로 올라갔는지 확인

Then TA종료