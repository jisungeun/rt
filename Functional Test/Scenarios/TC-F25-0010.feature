﻿Feature: Mask Editor; After BA Single Run

  Scenario: BA AI Segmentation에서 마스크가 생성되어 있지 않을 때 Mask correction 버튼을 누르면 경고창
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.

When Basic Analyzer Single Run 탭의 Mask Correction 버튼을 누른다.

Then There is no temporal mask 경고창이 나타났는지 확인

Then TA종료