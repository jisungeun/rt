﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Time points setting 패널에서 end 칸에 마우스 포인터를 대고 마우스 휠을 내려 time point 끝 값을 내림
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Basic Analyzer[T] 탭의 Time points setting 패널에서 end 칸에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.

Then Basic Analyzer[T] 탭 end 칸의 숫자가 1 내려갔는지 확인

And Time points setting 패널의 Selected time points에 나타나는 끝 숫자가 1 내려갔는지 확인