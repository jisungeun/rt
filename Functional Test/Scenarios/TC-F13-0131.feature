﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Labeling 탭에서 값을 직접 입력하여 ‘Assign Size of Neglectable Particle’ 수치를 설정하기
Given BAT Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.

When BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 사이즈 바 옆의 칸에 50.0000을 입력한다.

Then BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 사이즈 바가 값에 따라 정 중앙에 위치하는지 확인