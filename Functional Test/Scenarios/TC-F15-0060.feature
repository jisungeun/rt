﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭의 Time points setting 패널에서 end 칸에 수치를 입력하여 time point 끝 값을 setting
Given Basic Analyzer[T] 탭의 Batch Run setting 탭이 열려 있다.

When Batch Run setting 탭의 Time points setting 패널에서 end 칸에 ‘16’을 입력한다.

Then Batch Run setting 탭 Time points setting 패널에서 end 칸의 숫자가 16으로 변경되는지 확인

And Batch Run setting 탭 Time points setting 패널의 Selected time points에 나타나는 숫자가 모두 16보다 작은지 확인