﻿
Feature: 3D Visalizer; HT+FL Image

  Scenario: 형광 표지가 있는 TCF를 3D Visualizer로 열고 FL Rendering 입힌 뒤 Preset 패널의 FL 탭에서 Green, Red FL 설정
Given TCF가 프로젝트에 있다.

When Preview 패널에서 FL 정보가 있는 TCF를 선택한다.

And Toolbox의 3D Visualization 버튼을 누른다.

Then 3D Visualizer 탭이 열리는지 확인

And Modality 패널의 FL 체크박스가 비어있는지 확인

When Modality 패널의 FL 체크박스를 클릭한다.

Then Modality 패널의 FL 체크박스가 체크 됐는지 확인

And 3D Visualizer 탭 View 패널 3D, HT 화면에 FL Rendering이 생겼는지 확인

When Preset 패널의 FL 탭을 누른다.

Then Preset 패널 FL 탭의 Blue 체크 박스가 선택 불가능한 상태인지 확인

And  Preset 패널 FL 탭의 Blue Opacity 사이즈 바가 조작 불가능한 상태인지 확인

When Preset 패널 FL 탭의 Green 체크박스를 클릭한다.

Then 3D Visualizer 탭 View 패널에서 초록색 형광의 Rendering이 모두 사라졌는지 확인

And Preset 패널의 FL 탭의 Green 체크박스가 체크 해제됐는지 확인

When Preset 패널 FL 탭의 Green 체크박스를 다시 한 번 클릭한다.

Then 3D Visualizer 탭 View 패널에서 초록색 형광의 Rendering이 모두 생성됐는지 확인

And Preset 패널의 FL 탭의 Green 체크박스가 체크 됐는지 확인

When Preset 패널 FL 탭의 Red 체크박스를 클릭한다.

Then 3D Visualizer 탭 View 패널에서 빨간색 형광의 Rendering이 모두 사라졌는지 확인

And Preset 패널의 FL 탭의 Red 체크박스가 체크 해제됐는지 확인

When Preset 패널 FL 탭의 Red 체크박스를 다시 한 번 클릭한다.

Then 3D Visualizer 탭 View 패널에서 빨간색 형광의 Rendering이 모두 생성됐는지 확인

And Preset 패널의 FL 탭의 Red 체크박스가 체크 됐는지 확인

When Preset 패널 FL 탭의 Hide All 버튼을 누른다.

Then 3D Visualizer 탭 View 패널에서 형광의 Rendering이 모두 사라졌는지 확인

And Preset 패널의 FL 탭의 체크박스가 모두 체크 해제됐는지 확인

When Preset 패널 FL 탭의 Show All 버튼을 누른다.

Then 3D Visualizer 탭 View 패널에서 형광의 Rendering이 모두 나타났는지 확인

And Preset 패널의 FL 탭의 체크박스가 모두 체크 됐는지 확인

When Preset 패널 FL 탭 하단의 table에서 Green 채널의 Min 셀에 150을 입력하고 키보드의 엔터 키를 누른다.

Then 3D Visualizer 탭 View 패널에서 초록색 형광 Rendering이 바뀌는지 확인한다

When Preset 패널 FL 탭 하단의 table에서 Green 채널의 Max 셀에 180을 입력하고 키보드의 엔터 키를 누른다.

Then 3D Visualizer 탭 View 패널에서 초록색 형광 Rendering이 바뀌는지 확인

When Preset 패널 FL 탭 하단의 table에서 Red 채널의 Min 셀에 150을 입력하고 키보드의 엔터 키를 누른다.

Then 3D Visualizer 탭 View 패널에서 빨간색 형광 Rendering이 일부 사라지는지 확인

When Preset 패널 FL 탭 하단의 table에서 Red 채널의 Max 셀에 180을 입력하고 키보드의 엔터 키를 누른다.

Then 3D Visualizer 탭 View 패널에서 빨간색 형광 Rendering이 바뀌는지 확인

When Modality 패널의 FL 체크박스를 누른다.

Then Modality 패널의 FL 체크박스가 체크 해제됐는지 확인

And 3D Visualizer 탭 View 패널 3D, HT 화면에 FL Rendering이 사라졌는지 확인

Then 3D Visualizer TA종료