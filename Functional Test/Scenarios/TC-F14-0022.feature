﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Movie player에서 위 화살표를 이용하여 화면에 보이는 이미지의 time point를 올리기
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자 칸 옆의 위 화살표 버튼을 한 번 누른다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel 화면에 나타나는 이미지가 time point 2의 이미지로 바뀌었는지 확인

And Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자가 2로 올라갔는지 확인