﻿Feature: 3D Visualizer; HT Image Setup and Screenshot

  Scenario: Screen shot 패널에서 여러가지 Screen shot 저장
Given 3D Visualizer 탭이 열려 있다.

When Screen shot 패널 2D view 탭의 Upsampling 수치 입력 칸에 '1'을 입력하고 Capture 버튼을 누른다.

And 파일 탐색기에서 이름은 그대로 두고 경로만 지정한 뒤 저장 버튼을 누른다.

And Screen shot 패널 2D view 탭의 Upsampling 수치 입력 칸에서 위 화살표를 두 번 누르고 Capture 버튼을 누른다.

And 파일 탐색기에서 경로는 그대로 두고 이름을 변경한 뒤 저장 버튼을 누른다.

Then 지정한 경로에 서로 사이즈만 다른 .png XY 스크린 샷 파일이 두 개 생성되었는지 확인

When Screen shot 패널 2D view 탭의 All 체크박스에 체크한다.

And Screen shot 패널 2D view 탭의 Capture 버튼을 누른다.

And 파일 탐색기에서 파일 경로를 지정하고 저장 버튼을 누른다.

And 팝업에서 Replace 버튼을 누른다.

Then 지정한 경로에 .png 스크린 샷 파일이 3개 있는지 확인

When Screen shot 패널의 3D view 탭을 클릭한다.

Then Screen shot 패널에 3D view 탭 전용 UI가 나타남을 확인

When Preset 패널의 TF canvas에 TF box를 생성하여 View 패널의 3D 화면에 rendering을 만든다.

And Screen shot 패널 3D view 탭의 Capture 버튼을 누른다.

And 파일 탐색기에 경로만 지정하고 저장 버튼을 누른다.

Then 지정한 경로에 .png 3D 스크린 샷 파일이 생겼는지 확인

And 촬영 시점의 View 패널 3D 화면과 저장된 스크린 샷 파일이 일치하는지 확인

When Screen shot 패널 3D view 탭의 All axis 버튼을 누른다.

And 파일 탐색기에서 경로만 지정하고 저장 버튼을 누른다.

Then 지정한 경로에 .png 3D 스크린 샷 파일 3장이 생성되었는지 확인

When Screen shot 패널의 multi view 탭을 클릭한다.

Then Screen shot 패널에 multi view 탭 전용 UI가 나타남을 확인

When Screen shot 패널 multi view 탭의 Capture 버튼을 누른다.

And 파일 탐색기에서 경로를 설정하고 저장 버튼을 누른다.

Then .png multi-view 스크린 샷 파일이 설정한 경로에 생성됐는지 확인

And .png 파일이 촬영 시점의 View 패널 화면과 일치하는지 확인

When Screen shot 패널의 2D view 탭을 클릭한다.

Then Screen shot 패널에 2D view 탭 전용 UI가 나타남을 확인

Then 3D Visualizer TA종료