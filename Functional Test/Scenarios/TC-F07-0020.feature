﻿Feature: Select Application

  Scenario: 	Hypercube에 Application 연결 시 Playground canvas의 Hypercube 아이콘이 변경
Given hypercube에 어떤 app도 연결되어 있지 않다.

When 적용할 App과 적용 시킬 hypercube를 선택한다.

And Select Application패널에 OK 버튼을 누른다.

Then Playground canvas에서 연결된 hypercube의 아이콘이 Hypercube containing Cubes 아이콘에서 Hypercube to which the application is connected 아이콘으로 변경됨을 확인