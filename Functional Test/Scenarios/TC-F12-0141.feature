﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Labeling 탭에서 값을 직접 입력하여 ‘Assign Size of Neglectable Particle’ 수치를 설정하기
Given Basic Analyzer 탭에서 Use Labeling 체크박스가 체크되어 있다.

When Basic Analyzer 탭 ‘Assign Size of Neglectable Particle’ 사이즈 바 옆의 칸에 50.0000을 입력한다.

Then Basic Analyzer 탭 ‘Assign Size of Neglectable Particle’ 사이즈 바가 값에 따라 정 중앙에 위치하는지 확인