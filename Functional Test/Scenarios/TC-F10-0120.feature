﻿Feature: BA AI Segmentation Single RUN

  Scenario: Mask Viewing panel 상단의 Mask 툴박스에서 Whole cell 마스크를 출력
Given Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Mask Viewing panel 상단 Mask 툴박스의 Whole Cell 체크박스에 체크한다.

Then 3D 마스크 캔버스에서 Cell 전체에 마스크가 입혀졌는지 확인

And HT 사진의 Cell 영역 전체에 마스크가 입혀졌는지 확인