﻿Feature: Link Data to Cube

  Scenario: cube에 TCF 연결 해제 시 Operation sequence 패널이 Select Application 탭으로 자동 진행
Given Hypercube에 Application이 연결되어있지 않다.

And cube에 TCF가 연결되어 있다.

When 1개 이상의 TCF가 cube에 남아 있도록 TCF를 cube에서 연결 해제한다.

Then Operation sequance 패널이 Select Application 탭으로 자동으로 이동하는지 확인