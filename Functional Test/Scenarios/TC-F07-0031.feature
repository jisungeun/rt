﻿Feature: Select Application

  Scenario: hypercube에 연결되어 있는 Basic Analyzer 아이콘 선택 시 Application Parameter 패널의 UI 변경
Given BA 아이콘이 Active 상태가 아니다.

When hypercube에 연결된 BA 아이콘을 선택한다.

Then 선택된 BA 아이콘이 Active Application 아이콘으로 변경되는지 확인

And Application Parameter의 UI가 BA 버전으로 바뀌는지 확인