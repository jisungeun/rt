﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, UL Threshold 탭의 Execute 버튼을 눌러 마스크를 생성
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.

When Basic Analyzer Single Run 탭 Processing mode 패널에서 UL Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer Single Run 탭의 Mask Viewing panel에 3D RI 마스크가 생성되었는지 확인

And Basic Analyzer Single Run 탭의 Mask Viewing panel에 HT 이미지에 마스크가 생성되었는지 확인

And Basic Analyzer Single Run 탭 Mask Viewing panel 상단에 Cell Instance, Whole Cell 체크박스와 Cell-wise filter 스위치, Opacity 조절 버튼이 있는 툴박스가 생성되었는지 확인