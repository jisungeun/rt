﻿Feature: Mask Editor; After BA/BAT Batch Run
  Scenario: Report 탭의 Data table에서 데이터를 선택 후 Mask Editor를 연결
Given TomoAnalysis에 1개 cube로 Report 탭이 열려 있다.

When Report 탭 Data Table 패널의 테이블에서 가장 위에 있는 데이터를 클릭한다.

And Report 탭의 Edit Mask 버튼을 클릭한다. 

Then Mask Editor 탭이 열렸는지 확인