﻿Feature: Mask Editor; After BA Single Run

  Scenario: Mask Editor의 Semi-auto tool로 마스크를 수정
Given Mask Editor 탭에 1개의 Cell이 있는 TCF의 RI Thresholding cell instance 마스크가 ID ‘test’로 Copy되어 있다.

When Mask selector에서 Cell instance 아이콘을 누른다.

And Semi-auto tool 패널의 Erosion 버튼을 누른다.

Then Mask Editor의 XY Slice 화면에서 마스크가 쪼그라들었는지 확인

When Semi-auto tool 패널의 Dilation 버튼을 누른다.

Then Mask Editor의 XY Slice 화면에서 마스크가 팽창했는지 확인

When Semi-auto tool 패널의 Watershed 버튼을 누른다.

Then Mask Editor의 XY, YZ, XZ Slice 화면과 3D 화면에서 모든 마스크가 Watershed 알고리즘에 따라 수정되었는지 확인

When Semi-auto tool 패널의 Divider 버튼을 누른다.

And Mask Editor 탭의 3D 마스크 화면에서 마우스를 클릭한 채로 마스크를 가로지르도록 직선으로 드래그한다.

And Semi-auto tool 패널에서 활성화된 Divider 버튼을 누른다.

Then Mask Editor 탭의 마스크가 빨간색, 녹색 마스크 두 개로 나뉘었는지 확인

Then TA종료