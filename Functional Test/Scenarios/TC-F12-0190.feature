﻿Feature: BA AI Segmentation Single RUN

  Scenario: Measurement 팝업의 데이터를 .csv 파일로 Export
Given Basic Analyzer 탭에서 Measurement 팝업이 열려 있다.

When Basic Analyzer 탭의 Measurement 팝업에서 Export 버튼을 누른다.

And 저장 경로와 파일 이름을 지정하고 저장 버튼을 누른다.

Then 저장 경로에 지정한 이름의 .csv 파일이 생성되었는지 확인한다.

And .csv 파일의 데이터가 Measurement 탭의 데이터와 일치하는지 확인한다.