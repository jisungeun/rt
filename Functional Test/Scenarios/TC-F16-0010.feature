﻿Feature: BAT Report[T]

  Scenario: Report[T] 탭에서 그래프 옵션 설정
  
Given RI Thresholding으로 생성한 Report[T] 탭이 열려 있다.

When Select Data 패널의 All 체크박스 왼쪽의 옆 방향 화살표(▷)를 누른다.

And Report[T] 탭 Select Data 패널의 cube 체크박스 왼쪽의 옆 방향 화살표(▷)를 누른다.

And Report[T] 탭 Select Data 패널의 All 체크박스를 누른다.

And Report[T] 탭 Select Data 패널의 Cube 체크박스를 한 개만 누른다.

And Report[T] 탭 Select Data 패널의 체크되지 않은 개별 TCF 체크박스를 한 개만 누른다.

And Report[T] 탭 Select Parameter 패널에서 Sphericity 버튼을 누르고 Draw Graph 버튼을 누른다.

Then Report[T] 탭에서 설정한 데이터와 설정한 parameter로 그래프가 출력됐는지 확인

When Report[T] 탭의 Graph theme 스위치 버튼을 클릭한다.

And Report[T] 탭에서 그래프의 Min 값을 입력하는 창에 0을 입력하고 키보드의 엔터 키를 누른다.

And Report[T] 탭에서 그래프의 Max 값을 입력하는 창에 1을 입력하고 키보드의 엔터 키를 누른다.

And Report[T]의 그래프 패널 상단에서 cube 이름 옆의 드롭다운 버튼을 누르고 변경할 색을 선택한다.

When Report[T] 탭의 그래프 패널의 Export graph 버튼을 누른다.

And 파일 탐색기에서 파일 경로와 파일 이름을 지정하고 저장 버튼을 누른다.

Then 해당 저장 경로에 저장된 png 파일이 Report[T] 탭의 그래프와 동일하게 설정되었는지 확인

When Report[T] 탭의 Graph theme 스위치 버튼을 클릭한다.

Then Report[T] 탭의 그래프 배경 색이 흰 색으로 바뀌었는지 확인

And TA종료
