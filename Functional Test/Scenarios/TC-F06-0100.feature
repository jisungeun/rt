﻿Feature: Link Data to Cube

  Scenario: Operation sequence 패널의 Select Application 탭 화면에서 cube에 연결된 TCF를 연결 해제 시 Link Data to Cube 탭으로 자동으로 되돌아감
Given Operation sequence 패널의 Select Application 탭 화면이 열려 있다.

When Cube에 연결된 TCF를 모두 연결 해제한다.

Then Operation sequence 패널의 Select Application 탭 화면이 Link Data to Cube 탭으로 자동으로 돌아감을 확인