﻿Feature:  BAT Report[T]

  Scenario:	Report[T] 탭 그래프의 organelle 선택
	
Given AI Segmentation으로 생성한 Report[T] 탭이 열려 있다.

When Select organelle 패널에서 Nucleolus를 선택하고 Draw Graph 버튼을 누른다.

Then Report[T] 탭 그래프 패널의 그래프 타이틀이 Nucleolus로 바뀌었는지 확인

And TA종료
