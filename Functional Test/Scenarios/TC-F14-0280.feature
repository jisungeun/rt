﻿Feature: Segmentation Single RUN

  Scenario: BAT Single Run 탭Measurement 팝업의 각 row 색깔이 cell mask의 색과 동일
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭 Basic Measurement의 table의 parameter를 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.

And 프로세싱 바 팝업이 전부 진행될 때 까지 기다린다.

Then Basic Analyzer[T] 탭 위의 Measuerment 팝업의 각 row가 하이라이트 된 색이 cell mask 색과 동일한지 확인