﻿Feature: 2DIG Batch Run

  Scenario: 어떤 TCF도 선택하지 않고 2D Image Gallery 탭의 Rep 버튼을 누르면 조그맣게 경고 창이 뜸
Given 2D Image Gallery 탭이 열려 있다. 

When 2D Image Gallery 탭의 Rep 버튼을 누른다.

Then 2D Image Gallery 탭의 오른쪽 하단에 ‘There is no selected item.’ 이라는 안내문이 나타났는지 확인

Then TA종료