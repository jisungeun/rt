﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Time points setting 패널에서 custom 체크박스에 체크
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Basic Analyzer[T] 탭의 Time points setting 패널에서 custom 체크박스에 체크한다.

Then Basic Analyzer[T] 탭 custom 체크박스와 글자가 하이라이트 되는지 확인

And Basic Analyzer[T] 탭 체크 박스에 체크 표시가 생기는지 확인

And Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 직접 숫자를 입력할 수 있게 되는지 확인

And Basic Analyzer[T] 탭 Time points setting 패널의 start, end, interval 값을 변경할 수 없게 되었는지 확인