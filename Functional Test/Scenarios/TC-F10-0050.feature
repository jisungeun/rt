﻿Feature: BA AI Segmentation Single RUN

  Scenario: BA 탭에서 프로세서가 AI Segmentation일 때, Processing mode 패널의 AI Inference 탭 접기
Given Basic Analyzer 탭에서 AI Segmentation이 프로세서로 선택되어 있다.

And AI Inference 버튼 맨 오른쪽의 화살표가 윗 방향(△)이다.

When AI Inference 버튼을 누른다.

Then AI Inference 버튼 밑의 UI가 접히는지 확인

And AI Inference 버튼 맨 오른쪽의 화살표가 아래 방향(▽)으로 바뀌는지 확인