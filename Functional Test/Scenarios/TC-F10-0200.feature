﻿Feature: BA AI Segmentation Single RUN

  Scenario: Hide Result 버튼을 눌러 Measurement 팝업을 없앰
Given Basic Analyzer 탭에서 Measurement 팝업이 열려 있다.

When Hide Result 버튼을 누른다.

Then Measurement 팝업이 사라졌는지 확인

And Hide Result 버튼이 Show Result 버튼으로 바뀌었는지 확인