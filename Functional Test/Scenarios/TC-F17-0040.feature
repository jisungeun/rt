﻿Feature: TC-F17-0040

  Scenario: BAT 탭에서 프로세서가 RI Thresholding일 때, Labeling 탭에서 Execute 버튼을 눌러 Largest Label 방식으로 마스크를 수정
Given Basic Analyzer[T] 탭에서 Use Labeling 체크박스가 체크되어 있다.

And Basic Analyzer[T] 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭 Labeling 탭의 Select Option 드롭다운 버튼에서 Largest Label을 선택하고 Labeling 탭의 Execute 버튼을 누른다.

Then Basic Analyzer[T] 탭의 Mask Viewing 패널에서 가장 큰 크기의 마스크만 남는 방식으로 마스크가 수정되었는지 확인

And TA종료