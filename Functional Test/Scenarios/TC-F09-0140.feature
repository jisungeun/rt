﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Basic Measurement 탭 접기
Given BA Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

And Basic Measurement 버튼 맨 오른쪽의 화살표가 윗 방향(△)이다.

When RI Thresholding의 Basic Measurement 버튼을 누른다.

Then RI Thresholding의 Basic Measurement 밑의 UI가 접히는지 확인한다.

And RI Thresholding의 Basic Measurement 버튼 맨 오른쪽의 화살표가 아래 방향(▽)으로 바뀌는지 확인한다.