﻿Feature: Link Data to Cube

  Scenario: cube에 TCF를 연결 시 Preview 패널에서 TCF 썸네일에 연결된 cube 정보가 표시
Given Cube와 TCF가 생성되어 있다.

And Preview 패널이 열려 있다.

When TCF를 cube에 연결한다.

Then TCF 썸네일에 연결된 cube의 이름이 표시됨을 확인