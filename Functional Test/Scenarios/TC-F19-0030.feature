﻿Feature: 3D Visualizer; HT Image Data Rendering

  Scenario: Preset 패널에서 저장되어 있는 프리셋을 열고 수정/삭제
Given 3D Visualizer 탭이 열려 있다.

When 3D Visualizer 탭 Preset 패널의 Load 버튼을 누른다.

And 파일 탐색기에서 .xml 파일을 선택한다.

Then 3D Visualizer 탭의 Preset 패널 상단 드롭다운 버튼에 불러온 프리셋이 나타나는지 확인

And TF canvas에 프리셋의 TF box가 생성됐는지 확인

And View 패널에 Rendering이 생성됐는지 확인

When TF canvas의 하얀색 영역에서 겹치지 않게 TF box를 새로운 색으로 추가한다.

And 3D Visualizer 탭 Preset 패널의 Update 버튼을 누른다.

And 3D Visualizer 탭의 x버튼을 눌러 종료한다.

And Preview 패널에서 TCF를 선택하고 다시 3D Visualizer를 연다.

When 3D Visualizer 탭 Preset 패널 상단의 드롭다운 버튼을 누른다.

Then 3D Visualizer 탭 Preset 패널 상단 드롭다운 버튼에 지정된 경로의 프리셋 목록이 나타나는지 확인

When 3D Visualizer 탭 Preset 패널 상단 드롭다운 버튼의 목록에서 업데이트한 프리셋을 선택한다.

Then TF Canvas에 수정한 TF box가 나오는지 확인

And 3D Visualizer 탭의 View 패널에 수정한 Rendering이 나오는지 확인

When 3D Visualizer 탭 Preset 패널의 드롭다운 버튼에서 프리셋을 선택하고 Delete 버튼을 누른다. 

Then 3D Visualizer 탭 Preset 패널의 드롭다운 버튼이 Choose a TF preset 으로 바뀌는지 확인

And 삭제한 프리셋이 3D Visualizer 탭 Preset 패널의 드롭다운 버튼에서 사라졌는지 확인

And 3D Visualizer 탭의 Rendering과 TF box는 변화 없이 남아 있는지 확인

Then 3D Visualizer TA종료