﻿Feature: Select Application

  Scenario: Hypercube에 2D Image Gallery 앱 연결 시 Application Parameter 패널의 UI 변경
Given Hypercube에 2DIG app이 연결되어 있지 않다.

When 2DIG App과 적용 시킬 hypercube를 선택하고 ok버튼을 누른다. 

Then Application Parameter 패널의 UI가 2DIG 버전으로 바뀌는지 확인