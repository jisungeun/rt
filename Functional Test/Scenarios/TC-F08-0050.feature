﻿Feature: PEP/UI features

  Scenario: Operation sequence 패널에서 오른쪽 화살표(>) 버튼을 눌러 다음 단계로 진행
Given Operation sequence 패널에서 현재 단계의 해야 하는 작업을 모두 완료한 상태로 해당 탭이 열려 있다.

When Operation sequence 패널의 > 버튼을 누른다.

Then 다음 단계로 탭이 넘어가는지 확인