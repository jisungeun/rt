﻿Feature: Mask Editor; After BA/BAT Batch Run

  Scenario: Report[T] 탭의 그래프에서 개별 셀 선택 후 Mask Editor에서 마스크를 수정하여 그래프에 반영
Given TomoAnalysis에 Report[T] 탭이 열려 있다.

And Report[T] 탭에 Projected area-Lipid droplet 그래프가 그려져 있다.

When Report[T] 탭 Graph 패널의 점 한 개를 클릭하고 Report[T] 탭의 Edit mask 버튼을 누른다.

Then Report[T] 탭에서 선택한 파일이 Mask Editor 탭으로 열렸는지 확인

When Mask selector 패널에서 Cell instance 버튼을 클릭한다.

Then Mask Editor 탭에 Cell Instance 마스크가 생겼는지 확인

When Mask selector 패널에서 Whole cell 버튼을 클릭한다.

Then Mask Editor 탭에 Whole cell 마스크가 생겼는지 확인

When Mask selector 패널에서 Nucleus 버튼을 클릭한다.

Then Mask Editor 탭에 Nucleus 마스크가 생겼는지 확인

When Mask selector 패널에서 Nucleolus 버튼을 클릭한다.

Then Mask Editor 탭에 Nucleolus 마스크가 생겼는지 확인

When Mask selector 패널에서 Lipid droplet 버튼을 클릭한다.

Then Mask Editor 탭에 Lipid droplet 마스크가 생겼는지 확인

#When ID manager 패널의 Copy 버튼을 누르고 ‘test’를 입력한 후 OK 버튼을 클릭한다.

When Mask selector 패널에서 Lipid droplet 버튼을 누르고 Drawing tool 패널의 Paint 버튼을 클릭한 뒤 사이즈를 100으로 키우고 Mask Editor 탭 XY Slice 화면에서 마스크를 추가한다

When Mask selector 패널에서 Lipid droplet 아이콘을 한 번 더 클릭한다.

When General 패널에서 Apply to Result 버튼을 클릭한다.

Then Report[T] 탭의 그래프 패널 그래프가 바뀌었는지 확인

Then TA종료