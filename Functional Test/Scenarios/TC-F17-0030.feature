﻿Feature: TC-F17-0030

  Scenario: BAT 탭에서 프로세서가 RI Thresholding일 때, Auto lower Threshold 체크박스 체크
Given Basic Analyzer[T] 탭에서 RI Thresholding이 프로세서로 선택되어 있다.

And Basic Analyzer[T] 탭 Auto threshold 탭의 Auto lower threshold 체크박스가 체크되어 있지 않다.

When Basic Analyzer[T] 탭 Auto threshold 탭의 Auto lower Threshold 체크박스를 클릭한다.

Then Basic Analyzer[T] 탭 Auto Thresholde 탭에 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼, Execute 버튼이 나타나는지 확인

When Basic Analyzer[T] 탭 Auto Threshold 탭의 Select Algorithm 드롭다운 버튼에서 Entropy 알고리즘을 선택하고 Auto Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer[T] 탭의 UL Threshold 탭에서 값과 사이즈 바가 Entropy 알고리즘에 의해 자동으로 바뀌었는지 확인

When Basic Analyzer[T] 탭 Auto Threshold 탭의 Select Algorithm 드롭다운 버튼에서 Moment Preserving 알고리즘을 선택하고 Auto Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer[T] 탭의 UL Threshold 탭에서 값과 사이즈 바가 Moment Preserving 알고리즘에 의해 자동으로 바뀌었는지 확인

And TA종료