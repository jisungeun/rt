﻿Feature: Set Playground

  Scenario: 프로젝트에 Playground가 두 개 이상 존재할 때 원하는 Playground를 선택
Given 프로젝트에 Playground가 두 개 이상 생성되어 있다.

When Playground Setup 패널에서 원하는 Playground를 선택한다.

Then Playground Setup 패널에서 선택된 Playground가 하이라이트 됨을 확인

And Playground canvas 패널 상단에 선택된 Playground의 이름이 나타나는지 확인