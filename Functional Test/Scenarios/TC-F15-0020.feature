﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch run setting에서 time point를 설정하지 않고 batch run 하면 경고창이 출력
Given BAT Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.

When BAT Application Parameter의 Batch RUN 버튼을 누른다.

Then Basic Analyzer[T] 탭의 Batch Run setting 탭에서 Processing mode 패널의 맨 위 드롭다운 버튼이 AI Segmentation으로 설정됐는지 확인

When Basic Analyzer[T] 탭 Batch Run setting 탭에서 Processing mode 패널의 Execute Whole 버튼을 누른다.

Then “Set time steps for all image first!” 라는 에러 메세지 팝업이 나타나는지 확인

And TA종료