﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Time points setting 패널에서 start 칸의 위 화살표를 눌러 time point 시작 값을 올림
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Basic Analyzer[T] 탭의 Time points setting 패널에서 start 칸 옆의 위 화살표 버튼을 한 번 누른다.

Then Basic Analyzer[T] 탭 start 칸의 숫자가 1만큼 올라갔는지 확인

And Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 시작 숫자가 1만큼 올라갔는지 확인