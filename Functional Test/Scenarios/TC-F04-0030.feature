﻿Feature: Create Hypercube

  Scenario: 같은 이름의 Hypercube를 새로 생성하려 했을 때 생성되지 않음
	
Given ‘Hypercube’라는 이름의 Hypercube가 생성되어 있다.

When 같은 Playground에서 Creat Hypercube 탭을 연다.

And 새로 생성할 hypercube의 이름을 ‘Hypercube’로 지정한다.

And Creat Hypercube 탭의 OK 버튼을 누른다.

Then 에러 팝업이 나타나며 hypercube가 생성되지 않음을 확인

Then TA종료