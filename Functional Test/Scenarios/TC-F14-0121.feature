﻿Feature: BAT AI Segmentation Single RUNs

  Scenario: BAT Single Run 탭에서 프로세서가 AI Segmentation일 때, Processing mode 패널의 AI Inference 탭 펼치기
Given Basic Analyzer[T] Single Run 탭에서 AI Segmentation이 프로세서로 선택되어 있다.

And Basic Analyzer[T] 탭의 AI Inference 버튼 맨 오른쪽의 화살표가 아래 방향(▽)이다.

When Basic Analyzer[T] 탭의 AI Inference 버튼을 누른다.

Then Basic Analyzer[T] 탭의 AI Inference 버튼 밑의 UI가 펼쳐지는지 확인

And Basic Analyzer[T] 탭의 AI Inference 버튼 맨 오른쪽의 화살표가 윗 방향(△)으로 바뀌는지 확인