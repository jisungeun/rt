﻿"""Feature: BA AI Segmentation Single RUN

  Scenario: Mask Viewing panel의 3D 마스크 캔버스의 배경 색을 gradient로 변경
Given Basic Analyzer Single Run 탭이 열려 있다.

When 3D 마스크 캔버스의 우상단의 톱니바퀴 아이콘을 클릭한다.

And Background Option 버튼을 클릭한다.

And Background Option의 Gradient 버튼을 클릭한다.

Then 3D 마스크 캔버스의 배경 색이 gradient로 변경됨을 확인