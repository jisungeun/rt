﻿Feature: 	Set Playground

  Scenario: Set Playground 탭에서 엔터 키를 이용하여 Playground를 생성
Given Set playground의 Playground 이름을 지정하는 탭이 열려있다. 

When Default name 외의 Playground의 이름을 직접 지정한다.

And Playground를 클릭 하고 키보드의 엔터 키를 누른다.

Then Project Explorer 패널에 지정한 이름의 playground가 생성됐는지 확인