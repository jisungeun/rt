﻿Feature: PEP/UI features

  Scenario: Project Explorer Panel에서 프로젝트의 각 오브젝트 관리
Given Hypercube에 BA 아이콘이 연결되어 있다.

And Project Explorer Panel의 cube 이하의 항목이 모두 숨겨져 있다.

When Project Explorer Panel 최하단부에 위치한 아래 방향 겹쳐진 화살표 버튼(︾)을 클릭한다.

And Project Explorer Panel에서 Cube 왼쪽 끝에 위치한 화살표를 클릭한다.

Then Project Explorer Panel에서 화살표를 클릭한 Cube 외의 모든 항목이 펼쳐졌는지 확인

When Project Explorer Panel 최하단부에 위치한 위 방향 겹쳐진 화살표 버튼(︽)을 클릭한다.

Then Project Explorer Panel에서 cube 단위 이하의 개별 TCF 파일들이 숨겨졌는지 확인

When Project Explorer Panel에서 프로젝트 이름을 우클릭한다.

And Rename Project 버튼을 누른다.

And 팝업에서 변경할 이름을 입력하고 OK 버튼을 누른다.

Then Project Explorer Panel에서 프로젝트 이름의 변경을 확인

Then TA종료