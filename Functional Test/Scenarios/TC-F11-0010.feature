﻿Feature: BA Batch RUN

  Scenario: Report 탭 그래프의 parameter들을 변경
  
Given Report 탭이 열려 있다.

When 프로세싱 바 팝업이 전부 진행될 때 까지 기다린다.

When Report 탭 Select Prameter 패널에서 Volume 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 y축이 Volume으로 되어 있는지 확인

And Graph of individual cell 패널의 y축이 Volume으로 되어 있는지 확인

When Report 탭 Select Prameter 패널에서 Surface area 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 y축이 Surface area로 되어 있는지 확인

And Graph of individual cell 패널의 y축이 Surface area로 되어 있는지 확인

When Report 탭 Select Prameter 패널에서 Projected area 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 y축이 Projected area로 되어 있는지 확인

And Graph of individual cell 패널의 y축이 Projected area로 되어 있는지 확인

When Report 탭 Select Prameter 패널에서 Dry mass 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 y축이 Dry mass로 되어 있는지 확인

And Graph of individual cell 패널의 y축이 Dry mass로 되어 있는지 확인

When Report 탭 Select Prameter 패널에서 Concentration 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 y축이 Concentration으로 되어 있는지 확인

And Graph of individual cell 패널의 y축이 Concentration으로 되어 있는지 확인

When Report 탭 Select Prameter 패널에서 Sphericity 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 y축이 Sphericity로 되어 있는지 확인

And Graph of individual cell 패널의 y축이 Sphericity로 되어 있는지 확인

When Report 탭 Select Prameter 패널에서 Mean RI 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 y축이 Meean RI로 되어 있는지 확인

And Graph of individual cell 패널의 y축이 Mean RI로 되어 있는지 확인

When Report 탭 Select organelle 패널에서 Nucleus 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 타이틀이 Nucleus로 되어 있는지 확인

And Graph of individual cell 패널의 타이틀이 Nucleus로 되어 있는지 확인

When Report 탭 Select organelle 패널에서 Nucleolus 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 타이틀이 Nucleolus로 되어 있는지 확인

And Graph of individual cell 패널의 타이틀이 Nucleolus로 되어 있는지 확인

When Report 탭 Select organelle 패널에서 Lipid droplet 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 타이틀이 Lipid droplet으로 되어 있는지 확인

And Graph of individual cell 패널의 타이틀이 Lipid droplet으로 되어 있는지 확인

When Report 탭 Select organelle 패널에서 Whole cell 체크박스에 체크한다.

Then Inter-Cube mean comparison panel의 그래프 타이틀이 Whole cell으로 되어 있는지 확인

And Graph of individual cell 패널의 타이틀이 Whole cell으로 되어 있는지 확인

Then TA종료