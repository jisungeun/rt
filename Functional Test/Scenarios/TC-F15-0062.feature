﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭의 Time points setting 패널에서 end 칸의 아래 화살표를 눌러 time point 끝 값을 내림
	
Given Basic Analyzer[T] 탭의 Batch Run setting 탭이 열려 있다.

When Batch Run setting 탭의 Time points setting 패널에서 end 칸 옆의 아래 화살표 버튼을 한 번 누른다.

Then Batch Run setting 탭 Time points setting 패널에서 end 칸의 숫자가 1만큼 내려갔는지 확인

And Batch Run setting 탭 Time points setting 패널의 Selected time points에 나타나는 끝 숫자가 1만큼 내려갔는지 확인