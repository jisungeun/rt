﻿Feature: 3D Visualizer; HT Image Data Rendering

  Scenario: 3D Visualizer 탭에서 TF canvas에 마우스를 드래그하여 TF box와 Rendering을 생성, 수정 및 Preset으로 저장
Given 3D Visualizer 탭이 열려 있다.

When Preset 패널의 TF canvas 위의 하얀색 영역에서 마우스를 클릭한 채로 드래그한다.

And Select Color 팝업에서 원하는 색을 지정하고 OK 버튼을 누른다.

Then TF canvas의 드래그 한 영역에 지정한 색의 TF box가 나타났는지 확인

And 3D Visualizer 탭의 View 패널에 Rendering이 생기는지 확인

And 3D Visualizer 탭 Preset 패널의 parameter들이 생성된 TF box에 따라 바뀌는지 확인

When Preset 패널의 TF canvas 위의 하얀색 영역에서 마우스를 클릭한 채로 겹치지 않게 드래그한다.

And Select Color 팝업에서 원하는 색을 지정하고 키보드의 엔터 키를 누른다.

Then TF canvas의 드래그 한 영역에 지정한 색의 TF box가 생겼는지 확인

And 3D Visualizer 탭의 View 패널에 Rendering이 추가됐는지 확인

And 3D Visualizer 탭 Preset 패널의 parameter들이 생성된 TF box에 따라 바뀌었는지 확인

When TF canvas 내의 빈 공간에 마우스 포인터를 가져가서 클릭한다. 

Then 가장 최근에 추가한 TF box의 테두리에서 녹색 점이 사라졌는지 확인

And 3D Visualizer 탭 Preset 패널 하단의 table의  TF box 색 row에서 하이라이트가 제거되었는지 확인

When TF canvas에서 생성되어있는 TF box 한 개를 클릭한다.

Then 클릭한 TF box 테두리의 녹색 점이 생성됐는지 확인

And 3D Visualizer 탭 Preset 패널 하단 table의 TF box 색 row가 하이라이트 되었는지 확인

When 3D Visualizer 탭 Preset 패널 하단 table에서 선택되지 않은 TF box 색으로 칠해진 row를 클릭한다.

Then 클릭한 row 색의 TF box 테두리의 녹색 점이 생성됐는지 확인

When TF box 테두리에 생성된 녹색 점을 클릭한 채로 마우스를 드래그한다.

Then TF box의 크기가 달라졌는지 확인

And 3D Visualizer 탭 Preset 패널의 Parameter가 수정된 TF box에 따라 바뀌었는지 확인

And 3D Visualizer 탭의 View 패널에서 표시되는 Rendering이 달라졌는지 확인

When 선택된 TF box의 내부 영역을 마우스로 클릭한 채로 TF canvas의 위쪽 검은색 영역으로 드래그한다.

Then 3D Visualizer 탭 Preset 패널의 Parameter가 수정된 TF box에 맞게 바뀌었는지 확인

And 3D Visualizer 탭의 View 패널에서 표시되는 해당 TF box의 Rendering이 사라졌는지 확인

When 3D Visualizer 탭 Preset 패널의 Save As 버튼을 누른다.

And 파일 탐색기에 파일 이름과 파일 경로를 입력한다.

Then 입력한 폴더에 지정한 이름의 .xml 파일이 생성됐는지 확인

And 3D Visualizer 탭의 Preset 패널 상단 드롭다운 버튼에 지정한 이름의 프리셋이 나타나는지 확인

Then TA종료