﻿"""Feature: BA AI Segmentation Single RUN

  Scenario: Mask Viewing panel의 3D 마스크 캔버스의 배경 색을 검은 색으로 변경
Given Basic Analyzer Single Run 탭이 열려 있다.

When Background Option의 Black 버튼을 클릭한다.

Then 3D 마스크 캔버스의 배경 색이 검은 색으로 변경됨을 확인"""