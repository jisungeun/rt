﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭에서 Mask Viewing panel의 3D 마스크를 마우스로 클릭한 채로 움직이면 마스크가 회전
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

When 마우스를 커서 모양이 바뀌는 Basic Analyzer[T] 탭의 Mask Viewing panel 3D 마스크 캔버스 위로 가져가 클릭한 채로 마우스를 움직인다.

Then Basic Analyzer[T] 탭 Mask Viewing panel에 생성된 3D 마스크와 좌표축이 회전하는지 확인