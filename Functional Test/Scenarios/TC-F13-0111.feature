﻿Feature: 	BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Use Labeling 체크박스 체크 해제
Given BAT Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.

When BAT Application Parameter 패널의 Use Labeling 체크박스를 클릭한다.

Then BAT Application Parameter 패널 Labeling 탭에 ‘Select Option’ 문구와 옵션 드롭다운 버튼, ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 사라지는지 확인

And BAT Application Parameter 패널의 Use Labeling 체크박스가  체크 해제 되는지 확인