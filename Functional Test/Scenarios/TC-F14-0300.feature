﻿"""Feature: BAT AI Segmentation Single RUN

  Scenario:BAT Single Run 탭 Measurement 팝업의 데이터를 .csv 파일로 Export
Given Basic Analyzer[T] Single Run 탭에서 Measurement 팝업이 열려 있다.

When Basic Analyzer[T] 탭 위 Measurement 팝업의 Export 버튼을 누른다.

And 저장 경로와 이름을 지정하고 저장 버튼을 누른다.

Then 지정한 경로에 .csv 파일이 생성됐는지 확인한다.

And 생성된 .csv 파일의 데이터가 Measurement 탭의 데이터와 내용이 일치하는지 확인한다.