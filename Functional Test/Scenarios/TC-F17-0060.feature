﻿Feature: BAT RI Thresholding Single Run

  Scenario: BAT 탭의 Border Kill option
Given Basic Analyzer[T] Single Run 탭에 RAW LPS-24h-002P011 데이터의 RI 마스크가 열려 있다.

When Basic Analyzer[T] 탭 Labeling 탭에서 Use Labeling을 체크하고 Option을 Multi Labels로 선택한 뒤 Labeling 탭의 Execute 버튼을 누른다.

And Basic Analyzer[T] 탭 Border Kill 탭에서 Use Border Kill을 체크하고 Border Kill 탭의 Execute를 누른다.

Then Basic Analyzer[T] 탭 Mask Viewing panel에서 가장자리 세포의 마스크가 사라졌는지 확인

And TA종료