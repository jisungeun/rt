﻿Feature: BA AI Segmentation Single RUN

  Scenario: Basic Measurement 탭의 Execute 버튼을 눌러 Cell 정량 분석 결과 출력
Given Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Measurement의 table의 parameter를 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.

Then Measurement 팝업이 생기는지 확인한다.

And Basic Measurement 탭의 Execute 버튼의 하이라이트가 해제됐는지 확인

And Apply Parameter 버튼이 하이라이트 되었는지 확인