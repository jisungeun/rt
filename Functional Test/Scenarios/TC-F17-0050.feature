﻿Feature: TC-F17-0050

  Scenario: BAT 탭에서 time point를 이동하면 마스크가 리셋
Given Basic Analyzer[T] 탭의 RI Thresholding 모드에서 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭의 Movie Player 패널에서 time point를 한 포인트 위로 이동한다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel에서 마스크가 사라지는지 확인

When Basic Analyzer[T] 탭의 Movie Player 패널에서 time point를 다시 한 포인트 아래로 이동한다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel에서 마스크가 나타나지 않는지 확인

And TA종료