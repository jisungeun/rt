﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭에서 생성된 3D 마스크의 시야를 초기 시점으로 설정
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스 우상단의 톱니바퀴 아이콘을 클릭하고 View 버튼을 클릭한 뒤 Reset View 버튼을 클릭한다.

Then Basic Analyzer[T] 탭 Mask Viewing panel에서 처음 마스크가 생성되었을 때의 초기 시점으로 마스크가 보이는지 확인