﻿Feature: Import Data

  Scenario: TCF 썸네일 상단에 App 수행 필터 관련 정보 표기
Given Project Manager 탭에 Project 파일이 열려 있다. 

When Import Data 탭에서 import 버튼을 누르고 팝업창에서 HEK293FT-002P001 파일을 더블클릭한다. 

Then Preview 패널에 HEK293FT-002P001 파일의 썸네일이 보이고 썸네일 상단에 “AI” 글자와 초록색 원이 표기 돼 있는지 확인

When Import Data 탭에서 import 버튼을 누르고 팝업창에서 HT1080FFF-002Stitching 파일을 더블클릭한다. 

Then Preview 패널에 HT1080FFF-002Stitching 파일의 썸네일이 보이고 해당 썸네일 상단에 빨간색 원이 표기 돼 있는지 확인

When Import Data 탭에서 import 버튼을 누르고 팝업창에서 5x5-005Stitching 파일을 더블클릭한다. 

Then Preview 패널에 5x5-005Stitching 파일의 썸네일이 보이고 썸네일 상단에 아무런 글자 없이 초록색 원이 표기 돼 있는지 확인
  
Then TA종료