﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭에서 Basic Measurement 탭의 Execute 버튼을 눌러 Cell 정량 분석 결과 출력
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭 Basic Measurement의 table의 parameter를 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.

And 프로세싱 바 팝업이 전부 진행될 때 까지 기다린다.

Then Basic Analyzer[T] 탭 위에서 프로세싱 바 팝업이 사라지고 Measurement 팝업이 생기는지 확인한다.

And Basic Analyzer[T] 탭 Basic Measurement 탭의 Execute 버튼의 하이라이트가 해제됐는지 확인

And Basic Analyzer[T] 탭의 Apply Parameter 버튼이 하이라이트 되었는지 확인