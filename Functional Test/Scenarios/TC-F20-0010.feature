﻿Feature: 3D Visualizer; HT Image Setup and Screenshot

  Scenario: 3D Visualizer 각 패널을 팝업으로 분리 / 집어 넣기 / 병합
Given 3D Visualizer 탭이 열려 있다.

When Modality 패널 우상단의 겹쳐진 사각형 아이콘을 클릭한다.

Then Modality 패널이 팝업창으로 분리되었는지 확인

And Modality 패널의 HT 체크박스가 체크 되어있는지 확인

When Modality 팝업의 상단바를 더블 클릭한다.

Then Modality 팝업이 사라지고 3D Visualizer 탭의 원래 자리로 돌아갔는지 확인

When Modality 패널의 타이틀 텍스트를 마우스로 클릭한 채로 Navigator 패널 위로 가져간다.

And Navigator 패널에 흰색 테두리가 생기면 마우스를 놓는다.

Then 3D Visualizer 탭 왼쪽의 패널 하단에 Modality, Navigator 탭 툴바가 생성되었는지 확인

Then 3D Visualizer TA종료