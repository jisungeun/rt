﻿Feature: PEP/UI features

  Scenario: Operation sequence 패널에서 탭을 진행
Given Hypercube가 하나 이상 생성되어 있다.

And Operation sequence 패널에 Create Hypercube 탭이 열려 있다.

When Operation sequence 패널의 > 버튼을 누른다.

Then Operation sequence 패널에 Create Cube 탭이 열렸는지 확인

When Operation sequence 패널의 < 버튼을 누른다.

Then Operation sequence 패널에 Create Hypercube 탭이 열렸는지 확인

When Operation sequence 패널 상단의 Import Data 버튼을 누른다.

Then Operation sequence 패널에 Import Data 탭이 열렸는지 확인

Then TA종료