﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Basic Measurement의 table이 default 값으로 세팅
Given Playground canvas에서 Basic Analyzer[T] 아이콘이 선택되어 있다.

When BAT Application Parameter 패널에서 RI Thresholding을 프로세서로 선택한다.

Then BAT Application Parameter 패널 Basic Measurement의 table의 Preset row가 Protein으로 선택되어 있는지 확인