﻿Feature: BA Batch RUN

  Scenario: Inter-Cube mean comparison panel의 table을 저장
Given Report 탭의 Inter-Cube mean comparison panel에 table이 출력되어 있다.

When Inter-Cube mean comparison panel의 Export to Excel 버튼을 누른다.

And Export 할 경로와 파일 이름을 지정하고 저장 버튼을 누른다.

Then 해당 저장 경로에 지정한 이름의 table csv 파일이 생성되었는지 확인

And 저장한 table이 Inter-Cube mean comparison panel의 table과 같은 내용인지 확인