﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Labeling 탭에서 마우스 휠을 이용하여 ‘Assign Size of Neglectable Particle’ 수치를 내리기
Given BAT Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.

When BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.

Then BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 값이 24.9999로 바뀌는지 확인