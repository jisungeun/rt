﻿Feature: 3D Visualizer; Time-Lapse Image

  Scenario: 3D Visualizer에서 Timestamp 설정
Given 3D Visualizer 탭에 TimeLapse 이미지의 rendering이 생성되어 있다.

When Player 탭의 time point 칸에 3을 입력한다.

Then 3D Visualizer 탭 View 패널에서 2D HT 이미지와 3D rendering이 Time point 3으로 움직였는지 확인

And Player 탭의 타임 바가 Time point 3으로 움직였는지 확인

When Movie maker 탭을 클릭하고 Add 버튼을 누른 뒤 Timelapse 시간 설정 창에서 add 클릭

Then Movie maker 탭에 Timelapse 옵션이 추가되었는지 확인

And Movie maker 탭에 Time UI가 생겼는지 확인

When Movie maker 탭 Time UI의 Range 왼쪽 칸에 2를 입력한다.

And Movie maker 탭 Time UI의 Range 오른쪽 칸에 3를 입력한다.

And Movie maker 탭에서 Timelapse 효과 옵션 막대의 길이를 05:00까지 늘린다.

And Movie maker 탭 Time UI의 Reverse 체크박스를 클릭한다.

And Movie maker 탭의 Record 버튼을 누르고 3D 버튼을 누른다.

And 파일 탐색기 팝업에 파일 이름 'A'를 입력하고 저장 버튼을 누른다.

Then Movie maker 탭 Time UI의 Revere 체크박스가 체크 됐는지 확인

And A동영상 파일이 지정한 경로에 생성되었는지 확인

When Movie maker 탭 Time UI의 Reverse 체크박스를 한번 더 클릭한다.

Then Movie maker 탭 Time UI의 Revere 체크박스가 체크 해제됐는지 확인

When Movie maker 탭의 Record 버튼을 누르고 Multi view 버튼을 누른다.

And 파일 탐색기 팝업에 파일 이름 'B'를 입력하고 저장 버튼을 누른다.

Then B동영상 파일이 지정한 경로에 생성되었는지 확인

Then 3D Visualizer TA종료