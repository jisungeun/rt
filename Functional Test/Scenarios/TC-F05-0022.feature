﻿Feature: Create Cube

  Scenario: Create Cube 탭에서 키보드 엔터 키를 이용하여 Cube를 생성
Given Create Cube 탭이 열려있다.

When Cube의 이름을 지정한다.

And Cube의 경로를 지정하는 드롭다운 버튼을 클릭하여 경로를 지정하고 키보드의 엔터 키를 누른다.

Then Project Explorer 패널에 생성된 Cube가 표시됨을 확인