﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Time points setting 패널에서 interval 칸의 아래 화살표를 눌러 time point 간격을 줄임
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

And Basic Analyzer[T] 탭의 Time points setting 패널 interval 칸에 '3’이 입력되어 있다.

When Basic Analyzer[T] 탭의 Time points setting 패널에서 interval 칸 옆의 아래 화살표 버튼을 한 번 누른다.

Then Basic Analyzer[T] 탭 interval 칸의 숫자가 1만큼 내려갔는지 확인

And Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 수열의 공차가 1만큼 내려갔는지 확인