﻿Feature: Select Project

  Scenario: 엔터 키를 이용하여 Select Project 탭에서 Create 버튼을 눌러 새로운 프로젝트를 생성
  
  Given TA의 Select Project 탭 화면이 나타나있다.

  When  Create 버튼을 누른다.

  And 새로운 프로젝트의 이름과 저장 경로를 선택한다.

  And 키보드의 엔터 키를 누른다.

  Then Project Explorer에 프로젝트가 생성됐는지 확인