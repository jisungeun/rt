﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Auto lower Threshold 탭에서 Otsu 알고리즘을 적용하고  Execute 버튼을 눌러 UL Threshold를 재 설정
Given Basic Analyzer 탭에서 Auto lower Threshold 체크박스가 체크되어 있다.

When Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에서 Otsu 알고리즘을 선택하고 Auto Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭의 UL Threshold 탭에서 값과 사이즈 바가 Otsu 알고리즘에 의해 자동으로 바뀌었는지 확인