﻿Feature: Select Project

  Scenario: Select Project 탭에서 Open 버튼을 눌러 프로젝트를 열면 Operation Sequence 화면이 자동으로 다음 단계로 진행
  
  Given TA의 Select Project 탭 화면이 나타나있다.

  When open 버튼을 누른다.

  And 기존 프로젝트 파일을 선택한다.

  Then Operation sequence 화면이 Select Project 탭에서 다음 진행 단계의 탭으로 자동으로 넘어갔는지 확인