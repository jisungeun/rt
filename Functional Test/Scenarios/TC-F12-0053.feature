﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, UL Threshold 탭에서 윗 화살표 버튼을 눌러 lower threshold 수치를 올리기
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.

When Basic Analyzer Single Run 탭의 ‘Assign lower threshold’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.

Then Basic Analyzer Single Run 탭의 ‘Assign lower threshold’의 수치가 1.3501로 올라가는지 확인