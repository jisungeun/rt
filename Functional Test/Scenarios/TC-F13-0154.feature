﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Basic Measurement의 table에서 preset을 ‘Customize’로 설정했을 때 값을 직접 입력
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When BAT Application Parameter 패널 Basic Measurement의 table에서 default column의 Preset 드롭다운 버튼을 Customize로 선택한다.

And BAT Application Parameter 패널 Basic Measurement 탭 table에서 Baseline RI row의 cell을 클릭하고 키보드로 ‘12345’를 입력하고 키보드의 엔터 키를 누른다.

And BAT Application Parameter 패널 Basic Measurement 탭 table에서 RII row의 cell을 클릭하고 키보드로 ‘12345’를 입력하고 키보드의 엔터 키를 누른다.

Then BAT Application Parameter 패널 Basic Measurement 탭 table에서 Baseline RI 값과 RII값이 ‘12345’로 변경되는지 확인