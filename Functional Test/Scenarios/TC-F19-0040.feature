﻿Feature: 3D Visualizer; HT Image Data Rendering

  Scenario: Preset 패널의 RI Range 수치를 조절하여 TF box의 x축 길이 조절, Grad Range 수치를 조절하여 TF box의 y축 길이를 조절
Given 3D Visualizer 탭에서 TF box가 선택되어 있다.

When 3D Visualizer 탭 Preset 패널의 RI Range 왼쪽 칸에 1.3400을 입력한다.

And 3D Visualizer 탭 Preset 패널의 RI Range 오른쪽 칸에 1.3700을 입력한다.

Then 선택된 TF box의 x축 길이가 달라졌는지 확인

And 3D Visualizer 탭 View 패널에서 Rendering이 달라졌는지 확인

When 3D Visualizer 탭 Preset 패널의 왼쪽 RI Range 수치 입력 칸의 위 화살표 버튼을 한 번 누른다. 

Then 3D Visualizer 탭 Preset 패널 하단 table의 해당 TF box 색 row에서 RI Min 값이 0.0001만큼 올라갔는지 확인

And 3D Visualizer 탭 Preset 패널의 왼쪽 RI Range 수치 입력 칸의 수치가 0.0001만큼 올라갔는지 확인

When 3D Visualizer 탭 Preset 패널의 오른쪽 RI Range 수치 입력 칸의 아래 화살표 버튼을 한 번 누른다. 

Then 3D Visualizer 탭 Preset 패널 하단 table의 해당 TF box 색 row에서 RI Max 값이 0.0001만큼 내려갔는지 확인

And 3D Visualizer 탭 Preset 패널의 오른쪽 RI Range 수치 입력 칸의 수치가 0.0001만큼 내려갔는지 확인

When 3D Visualizer 탭 Preset 패널의 왼쪽 Grad Range 수치 입력 칸에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.

Then 3D Visualizer 탭 Preset 패널의 왼쪽 Grad Range 수치 입력 칸의 수치가 0.05만큼 올라갔는지 확인

When 3D Visualizer 탭 Preset 패널의 오른쪽 Grad Range 수치 입력 칸에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.

Then 3D Visualizer 탭 Preset 패널의 오른쪽 Grad Range 수치 입력 칸의 수치가 0.05만큼 내려갔는지 확인

Then 3D Visualizer TA종료