﻿Feature: Select Project

  Scenario:	Select Project 탭에서 Create 버튼을 눌러 새 프로젝트가 생성되면 Operation Sequence 화면이 자동으로 다음 단계로 진행
  Given TA의 Select Project 탭 화면이 나타나있다.

  When  Create 버튼을 누른다.

  And 새로운 프로젝트의 이름과 저장 경로를 선택한 후 프로젝트 파일을 생성한다.

  Then Operation sequence 화면이 Select Project 탭에서 Import Data 탭으로 자동으로 넘어갔는지 확인