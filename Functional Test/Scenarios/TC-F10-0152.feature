﻿"Feature: BA AI Segmentation Single RUN

  Scenario: Mask Viewing panel의 HT 이미지 캔버스의 이미지 색을 JET로 변경
Given Basic Analyzer Single Run 탭이 열려 있다.

When HT 이미지 캔버스의 우상단의 톱니바퀴 아이콘을 클릭한다.

And Color Map 버튼을 클릭한다.

And Color map의 JET 버튼을 클릭한다.

Then 3D 마스크 캔버스의 이미지 색이 JET로 변경됨을 확인