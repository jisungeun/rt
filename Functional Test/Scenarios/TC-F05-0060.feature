﻿Feature: Create Cube

  Scenario: Playground canvas에서 Cube 아이콘을 클릭하여 cube를 선택
Given 한 개 이상의 cube가 생성되어 있다.

When Playground canvas 위의 Normal cube 아이콘을 누른다.

Then Playground canvas 위의 Normal cube 아이콘이 Active cube 아이콘으로 바뀜을 확인