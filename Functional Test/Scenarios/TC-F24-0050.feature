﻿Feature: Mask Editor; From empty mask

  Scenario: 저장한 마스크를 불러온 뒤 Wipe 버튼과 History 패널을 이용하여 마스크 수정
Given Mask Editor 탭에서 ‘test’ ID가 선택되어 있다.

When General 패널의 msk file upload 버튼을 누른다.

And 20200611.164640.571.Hep G2-105_MultiLayer_test 파일을 선택한다.

Then Mask selector 패널의 Whole cell 버튼이 활성화 되었는지 확인

When Mask selector 패널 Whole cell의 버튼을 누른다.

Then Mask Editor 탭 3D 화면과 2D Slice 화면에 Whole cell 마스크가 나타났는지 확인

When General 패널에서 msk file upload 버튼을 누른다.

And 20200611.164640.571.Hep G2-105_MultiLabel_test 파일을 선택한다.

Then Mask selector 패널의 Cell instance 버튼이 활성화 되었는지 확인

When Mask selector 패널 Cell instance 버튼을 누른다.

Then Mask Editor 탭 3D 화면과 2D Slice 화면에 Cell instance 마스크가 나타났는지 확인

When Drawing tool 패널의 Wipe 버튼을 클릭한다.

And Mask Editor 탭 중앙의 XY Slice 화면 중 마스크가 존재하는 영역에 마우스를 클릭한다.

Then Mask Editor 탭에서 클릭한 자리에서 점으로 Cell instance 마스크가 지워졌는지 확인

When Mask selector 패널 Whole cell 아이콘을 클릭하고 Drawing tool 패널의 Erase 버튼을 클릭한다.

And Mask Editor 탭 중앙의 XY Slice 화면 중 마스크가 존재하는 영역에 마우스 포인터를 대고 클릭한다.

Then Mask Editor 탭에서 마우스 포인터로 클릭한 slice에서 Whole cell 마스크가 전부 지워졌는지 확인

When History 패널의 Undo 버튼을 누른다.

Then Mask Editor 탭 XY Slice 화면에서 Wipe로 지운 점이 다시 생겼는지 확인

When History 패널의 Redo 버튼을 누른다.

Then Mask Editor 탭 XY Slice 화면에서 Undo 버튼으로 생긴 부분이 다시 사라졌는지 확인

When History 패널의 Refresh 버튼을 누른다.

And History 패널의 Undo 버튼과 Redo 버튼을 누른다.

Then History 패널의 Undo 버튼과 Redo 버튼을 눌러도 Mask Editor 탭 XY Slice 화면에서 아무런 변화가 없는지 확인

Then TA종료