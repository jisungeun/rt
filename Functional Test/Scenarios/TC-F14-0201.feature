﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Mask Viewing panel 상단의 Mask 툴박스에서 Cell-wise filter 스위치를 끔
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

And Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스에 Whole Cell이 체크되어 있다.

And Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치가 켜져 있다.

When Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치를 끈다.

Then Basic Analyzer[T] 탭 Mask Viewing panel에서 모든 cell의 마스크가 같은 색으로 표시되는지 확인

And Basic Analyzer[T] 탭 Mask 툴박스의 Cell-wise filter 스위치 버튼이 하이라이트 해제됐는지 확인

And Basic Analyzer[T] 탭 Mask 툴박스의 Cell-wise filter 스위치 버튼이 왼쪽으로 움직였는지 확인