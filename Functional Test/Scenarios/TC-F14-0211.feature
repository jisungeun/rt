﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Mask Viewing panel 상단의 Mask 툴박스에서 Opacity를 화살표 버튼으로 설정하여 마스크의 불투명도를 올리기
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

And Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 값이 ‘.5’ 이다.

When Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창 옆의 윗 화살표 버튼을 10번 누른다.

Then Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의Opacity 수치 값이 0.60으로 올라갔는지 확인

And Basic Analyzer[T] Mask Viewing panel의 마스크들이 진해짐을 확인