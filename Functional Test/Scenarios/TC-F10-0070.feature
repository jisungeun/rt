﻿Feature: BA AI Segmentation Single RUN

  Scenario: AI Inference 탭의 Execute 버튼을 클릭하여 Mask Viewing panel에 Mask를 생성
Given Basic Analyzer 탭에서 AI Segmentation이 프로세서로 선택되어 있다.

When AI Inference 탭의 Execute 버튼을 클릭한다.

Then Mask Viewing panel에 3D 마스크가 생성되었는지 확인

And HT 이미지에 마스크가 생성되었는지 확인

And Mask Viewing panel 상단에 Cell Instance, Whole Cell, Nucleus, Nucleolus, Lipid droplet 체크박스와 Cell-wise filter 스위치, Opacity 조절 버튼이 있는 툴박스가 생성되었는지 확인

And AI Inference 탭의 Execute 버튼이 하이라이트 해제되었는지 확인

And Basic Measurement 탭의 Execute 버튼이 하이라이트 되었는지 확인