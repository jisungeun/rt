﻿Feature: BA Batch RUN

  Scenario: Inter-Cube mean comparison panel에 Table/graph 표시 설정
Given Report 탭의 Inter-Cube mean comparison panel에 graph가 열려 있다.

When 프로세싱 바 팝업이 전부 진행될 때 까지 기다린다.

When Inter-Cube mean comparison panel에서 ‘Table' 텍스트를 클릭한다.

Then Inter-Cube mean comparison panel에 graph 대신 table이 나타나는지 확인

When Inter-Cube mean comparison panel에서 Graph 글자를 클릭한다.

Then Inter-Cube mean comparison panel에 table 대신 graph가 나타나는지 확인

When Graph of individual cell 패널의 Max 값을 입력하는 창에 ‘1.4’를 입력하고 키보드의 엔터 키를 누른다.

Then Graph of individual cell 패널 그래프의 y축 최댓값이 1.4로 바뀌었는지 확인

When Inter-Cube mean comparison panel의 Export to Excel 버튼을 누른다.

And 파일 경로와 파일 이름을 지정하고 저장 버튼을 누른다.

Then 해당 저장 경로에 지정한 이름의 table csv 파일이 생성되었는지 확인

Then 리포트와 TA종료
