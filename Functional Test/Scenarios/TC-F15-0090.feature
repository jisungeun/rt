﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭의 Time points setting 패널에서 custom 체크박스에 체크하여 time point를 직접 입력
Given Basic Analyzer[T] 탭에서 Batch Run setting 탭의 Time points setting 패널의 custom 체크박스에 체크 되어있다.

When Batch Run setting 탭 Time points setting 패널의 Selected time points 칸에 time points를 직접 입력한다.

Then Batch Run setting 탭 Time points setting 패널의 Selected time points 칸의 숫자가 입력한 대로 바뀌는지 확인