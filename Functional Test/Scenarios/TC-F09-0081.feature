﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Auto lower Threshold 체크박스 체크 해제
Given BA Application Parameter에서 Auto lower Threshold 체크박스가 체크되어 있다.

When Auto lower Threshold 체크박스를 클릭한다.

Then ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼이 사라지는지 확인

And Auto lower Threshold 체크박스가  체크 해제 되는지 확인