﻿Feature: Import Data

  Scenario: Preview 패널에서 Deselect all 버튼을 눌러 불러온 TCF를 모두 선택 해제
Given 한 개 이상의 TCF가 선택되어 있다.

When Preview 패널에서 Deselect all 버튼을 누른다.

Then 선택되었던 모든 TCF가 선택 해제되었는지 확인