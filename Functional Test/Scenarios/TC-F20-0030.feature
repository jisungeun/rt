﻿Feature: 3D Visualizer; HT Image Setup and Screenshot

  Scenario: Viewing tool 패널의 View 드롭다운 버튼에서 View 패널 설정
Given 3D Visualizer 탭이 열려 있다.

And Viewing tool 패널의 View 드롭다운 버튼에서 Show Orientation marker 버튼이 체크 해제되어 있다.

When Viewing tool 패널의 Layout 드롭다운 버튼에서 2D image top, 3D bottom 버튼을 누른다.

Then View 패널의 레이아웃이 2D image top, 3D bottom으로 변경되었는지 확인

When Viewing tool 패널의 View 드롭다운 버튼에서 Show Orientation marker 버튼을 누른다.

Then Viewing tool 패널 View 드롭다운 버튼의 Show Orientation marker 버튼이 체크 됐는지 확인

And 3D Visualizer 탭 View 패널의 3D 화면에서 정육면체가 생성됐는지 확인

When Viewing tool 패널에서 View 드롭다운 버튼의 Show Orientation marker 버튼을 누른다.

Then Viewing tool 패널 View 드롭다운 버튼의 Show Orientation marker 버튼이 체크 해제됐는지 확인

And 3D Visualizer 탭 View 패널의 3D 화면에서 정육면체가 사라졌는지 확인

When Viewing tool 패널의 View 드롭다운 버튼에서 Show Boundary box 버튼을 누른다.

Then 3D Visualizer 탭 View 패널의 3D 화면에 Boundary box가 생성됐는지 확인

And Viewing tool 패널 View 드롭다운 버튼의 Show Boundary box 버튼이 체크 됐는지 확인

When Viewing tool 패널의 View 드롭다운 버튼에서 Show Boundary box 버튼을 누른다.

Then 3D Visualizer 탭 View 패널의 3D 화면에 Boundary box가 사라졌는지 확인

And Viewing tool 패널 View 드롭다운 버튼의 Show Boundary box 버튼이 체크 해제됐는지 확인

When Viewing tool 패널의 View 드롭다운 버튼에서 Show Axis grid 버튼을 누른다.

Then 3D Visualizer 탭 View 패널의 3D 화면에 Axis grid가 생성됐는지 확인

When Viewing tool 패널의 View 드롭다운 버튼에서 Show Axis grid 버튼을 누른다.

Then 3D Visualizer 탭 View 패널의 3D 화면에 Axis grid가 사라졌는지 확인


Then 3D Visualizer TA종료