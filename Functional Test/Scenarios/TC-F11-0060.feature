﻿Feature:  BA Batch RUN

  Scenario: Graph theme 버튼을 이용하여 Report 탭에 표시된 graph의 배경 색을 어두운 색으로 변경
Given Report 탭의 Graph theme 스위치 버튼이 꺼져 있다.

When Report 탭의 Graph theme 스위치 버튼을 클릭한다.

Then Report 탭의 Graph theme 스위치 버튼이 하이라이트 됐는지 확인

And Report 탭의 Graph theme 스위치 버튼이 오른쪽으로 움직였는지 확인

And Inter-Cube mean comparison panel과 Graph of individual cell 패널의 그래프 배경 색이 검은 색으로 바뀌었는지 확인