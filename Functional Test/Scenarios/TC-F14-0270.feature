﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Show Result 버튼을 눌러 Measurement 팝업을 띄움
Given 주어진 TCF에 대해 위, 아래 Execute 버튼을 모두 눌러 작업을 수행한 BAT Single Run 탭이 열려 있다.

And Basic Analyzer[T] 탭 위에 Measurement 팝업이 열려 있지 않다.

When Basic Analyzer[T] 탭의 Show Result 버튼을 누른다.

Then Basic Analyzer[T] 탭 위에 Measurement 팝업이 생기는지 확인

And Basic Analyzer[T] 탭의 Show Result 버튼이 Hide Result 버튼으로 바뀌었는지 확인