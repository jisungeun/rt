﻿Feature: TC-F11-0042

  Scenario: Report 탭의 Select organelle 패널을 Nucleolus로 변경
Given Report 탭이 열려 있다.

When Report 탭 Select organelle 패널에서 Nucleolus 체크박스에 체크한다.

Then Report 탭 Select Parameter 패널이 Nucleolus에 체크됐는지 확인

And Report 탭의 Nucleolus 글자가 하이라이트 됐는지 확인

And Inter-Cube mean comparison panel의 그래프 타이틀이 Nucleolus로 되어 있는지 확인

And Graph of individual cell 패널의 타이틀이 Nucleolus로 되어 있는지 확인