﻿Feature:BAT Application Parameter

  Scenario:AI Segmentation을 프로세서로 선택했을 때, Basic Measurement 탭 접기
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When BAT Application Parameter 패널의 ‘Assign lower threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.

Then BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바가 ‘Assign upper threshold’ 값을 넘지 않는 값 까지만 올라가는지 확인

When BAT Application Parameter 패널의 ‘Assign upper threshold’ 사이즈 바 옆의 칸에 '2'를 입력한다.

Then 입력한 값으로 수치가 바뀌었는지 확인

When BAT Application Parameter 패널의 Auto lower Threshold 체크박스를 클릭한다.

And BAT Application Parameter 패널의 Select Algorithm 드롭다운 버튼에서 Entropy를 선택한다.

Then BAT Application Parameter 패널의 Select Algorithm 버튼에 Entropy가 선택되었는지 확인

When BAT Application Parameter 패널의 Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 내린다.

Then BAT Application Parameter 패널의 Select Algorithm 버튼에 Moments Preserving이 선택되었는지 확인

When BAT Application Parameter 패널의 Auto lower Threshold 체크박스를 클릭한다.

Then BAT Application Parameter 패널 Auto Threshold 탭의 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼이 사라지는지 확인

When BAT Application Parameter 패널의 Use Labeling 체크박스를 클릭한다.

When BAT Application Parameter 패널의 Select Option 드롭다운 버튼에서 Largest Label 버튼을 누른다.

Then BAT Application Parameter 패널의 Select Option 버튼이 Largest Label 옵션으로 변경되었는지 확인

When BAT Application Parameter 패널의 Select Option 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 올린다.

Then BAT Application Parameter 패널의 Select Option 버튼이 Multi Labels 옵션으로 변경되었는지 확인

When BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 수치 칸 옆의 아래 방향 화살표를 한 번 누른다.

Then BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 값이 24.9999로 바뀌는지 확인

When BAT Application Parameter 패널의 Use Labeling 체크박스를 클릭한다.

Then BAT Application Parameter 패널 Labeling 탭에 ‘Select Option’ 문구와 옵션 드롭다운 버튼, ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 사라지는지 확인

Then TA종료