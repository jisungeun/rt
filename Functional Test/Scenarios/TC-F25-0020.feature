﻿Feature: Mask Editor; After BA Single Run

  Scenario: BA AI Segmentation에서 마스크가 생성되어 있을 때 Mask correction 버튼으로 마스크 수정
Given Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Analyzer Single Run 탭의 Mask Correction 버튼을 누른다.

Then Apply to Result 버튼과 Cell Instance, Organelle 마스크가 모두 활성화 되어있는 Mask Editor 탭이 열렸는지 확인

When Mask selector 패널 Cell instance 버튼을 클릭한다.

Then Mask Editor 탭에 Cell Instance 마스크가 나타나는지 확인

When Mask selector 패널 Whole cell 버튼을 클릭한다.

Then Mask Editor 탭에 Whole cell 마스크가 나타나는지 확인

When Mask selector 패널 Nucleus 버튼을 클릭한다.

Then Mask Editor 탭에 Nucleus 마스크가 나타나는지 확인

When Mask selector 패널 Nucleolus 버튼을 클릭한다.

Then Mask Editor 탭에 Nucleolus 마스크가 나타나는지 확인

When Mask selector 패널 Lipid droplet 버튼을 클릭한다.

Then Mask Editor 탭에 Lipid droplet 마스크가 나타나는지 확인

When ID manager 패널의 Copy 버튼을 누르고 ‘test’를 입력한 후 OK 버튼을 누른다.

When Mask selector 패널 Cell instance 버튼을 클릭한다.

And Drawing tool 패널의 Paint 버튼을 누르고 사이즈를 100으로 키운 뒤 Mask Editor 탭 XY Slice 화면에서 겹치지 않게 점을 찍어 마스크를 추가한다.

When Mask selector 패널 Cell instance 버튼을 클릭한다.

And General 패널의 Apply to Result 버튼을 누른다.

Then Basic Analyzer 탭 Mask Viewing panel에서 Cell instance 마스크가 수정한 대로 바뀌어있는지 확인

Then TA종료