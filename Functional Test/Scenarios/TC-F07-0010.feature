﻿Feature: Select Application

  Scenario: Hypercube에 Application 연결 시 Playground canvas에 Application 아이콘 생성
Given Hypercube에 연결된 BA 아이콘이 Active 상태가 아니다.

When Playground canvas에서 Hypercube에 연결된 BA 아이콘을 선택한다.

Then 선택된 Normal Application 아이콘이 Active Application 아이콘으로 변경되는지 확인

And Application Parameter의 UI가 BA 버전으로 바뀌는지 확인

When Select Application 탭에서 BAT App과 적용 시킬 hypercube를 선택하고 Select Application 패널의 OK 버튼을 누른다.

Then Application Parameter 패널의 UI가 BAT 버전으로 바뀌는지 확인

When Select Application 탭에서 2DIG App과 적용 시킬 hypercube를 선택한다.

And Select Application 패널의 OK 버튼을 누른다. 

Then Application Parameter 패널의 UI가 2DIG 버전으로 바뀌는지 확인

Then TA종료