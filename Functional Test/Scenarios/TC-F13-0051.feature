﻿Feature: TC-F13-0051

  Scenario: A description of your business scenario
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

And BAT Application Parameter 패널의 UL Threshold 버튼 맨 오른쪽의 화살표가 아래 방향(▽)이다.

When BAT Application Parameter 패널의 UL Threshold 버튼을 누른다.

Then BAT Application Parameter 패널의 UL Threshold 밑의 UI가 펼쳐지는지 확인

And BAT Application Parameter 패널의 UL Threshold 버튼 맨 오른쪽의 화살표가 윗 방향(△)으로 바뀌는지 확인