﻿Feature: Import Data

  Scenario: Shift 키를 이용하여 Preview 패널에서 TCF 썸네일을 클릭하여 TCF를 여러 개 선택
Given TCF의 썸네일이 보이는 Preview 패널이 열려 있다.

When 원하는 TCF 파일들 중 가장 윗 줄의 가장 왼쪽 파일을 선택한다.

And 원하는 TCF 파일들 중 가장 아랫 줄의 가장 오른쪽 파일을 키보드의 Shift 키와 함께 누른다.

Then 선택한 두 개의 파일 사이에 위치한 모든 TCF 파일들이 선택되었는지 확인