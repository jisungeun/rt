﻿Feature: BA AI Segmentation Single RUN

  Scenario: BA 탭 Processing mode 패널에서 프로세서를 변경
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 AI Segmentation으로 선택되어 있다.

When Processing mode 패널의 드롭다운 버튼을 클릭한다.

And Single Run 탭의 RI Thresholding을 선택한다.

Then Basic Analyzer Single Run 탭의 Processing mode 패널에 RI Thresholding의 UI가 나타나는지 확인

When Processing mode 패널의 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 올린다.

Then Basic Analyzer Single Run 탭의 Processing mode 패널에 AI Segmentation의 UI가 나타나는지 확인

Then TA종료