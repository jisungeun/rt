﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭에서 체크박스로 TCF 선택 후 time point 설정
Given Basic Analyzer[T]의 Batch Run setting 탭의 All 체크박스가 체크 되어있지 않다.

When Basic Analyzer[T] Batch Run setting 탭의 All 체크박스를 클릭한다.

Then Basic Analyzer[T] Batch Run setting 탭의 All 체크박스가 체크 됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭의 cube 체크박스가 모두 체크 됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭에서 모든 TCF 파일의 체크박스가 체크 됐는지 확인

When Basic Analyzer[T] Batch Run setting 탭의 단일 cube 체크박스 혹은 cube 이름을 클릭한다.

Then Basic Analyzer[T] Batch Run setting 탭의 단일 cube 체크박스가 체크 해제됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭에서 cube에 링크된 TCF 파일의 체크박스가 체크 해제됐는지 확인

When Basic Analyzer[T] Batch Run setting 탭에서 체크박스가 비어 있는 단일 TCF row를 클릭한다.

Then Basic Analyzer[T] Batch Run setting 탭의 단일 TCF 체크박스가 체크 됐는지 확인

When Batch Run setting 탭의 Time points setting 패널에서 custom 체크박스에 체크한다.

And Batch Run setting 탭 Time points setting 패널의 Selected time points 칸에 1, 2, 3을 직접 입력한다.

And Batch Run setting 탭에서 체크되지 않은 TCF를 한 개 클릭한다.

And Batch Run setting 탭 Time points setting 패널의 Appply time points 버튼을 누른다.

Then Batch Run setting 탭에서 마지막으로 클릭한 TCF의 Selected time points 칸에 세팅한 Time points가 적용되었는지 확인

When Batch Run setting 탭 Time points setting 패널의 Selected time points 칸에 1, 2, 3, 4를 직접 입력한다.

And Batch Run setting 탭 Time points setting 패널의 Apply time points to checked data 버튼을 누른다.

Then Batch Run setting 탭에서 체크된 두 TCF의 Selected time points 칸에 세팅한 Time points가 적용되었는지 확인

When Batch Run setting 탭 Time points setting 패널의 Apply time points to all data 버튼을 누른다.

Then Batch Run setting 탭에 있는 모든 TCF의 Selected time points 칸에서 세팅한 Time points가 적용되었는지 확인

And TA종료