﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Labeling 탭에서 값을 직접 입력하여 ‘Assign Size of Neglectable Particle’ 수치를 설정하기
Given BA Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.

When ‘Assign Size of Neglectable Particle’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.

Then ‘Assign Size of Neglectable Particle’ 값이 25.0001로 바뀌는지 확인