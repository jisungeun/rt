﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, UL Threshold 탭에서 마우스 휠을 이용하여 수치를 내리기
Given BA Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When ‘Assign lower threshold’의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 내린다.

And ‘Assign upper threshold’ 의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 내린다.

Then 1과 2 사이에서 ‘Assign lower threshold’, ‘Assign upper threshold’ 의 수치가 내려가는지 확인
