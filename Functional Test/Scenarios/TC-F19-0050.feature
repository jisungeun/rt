﻿Feature: 3D Visualizer; HT Image Data Rendering

  Scenario: Preset 패널에서 Rendering의  Overlay, Opacity 설정
Given 3D Visualizer 탭의 View 패널에 Rendering이 생성되어 있다.

When 3D Visualizer 탭 Preset 패널의 Overlay 체크박스를 클릭한다.

Then 3D Visualizer 탭 Preset 패널 Overlay 체크박스에 체크표시가 생기는지 확인

And 3D Visualizer 탭 View 패널의 HT 이미지에 Rendering이 생성되는지 확인

When 3D Visualizer 탭 Preset 패널의 Opacity 사이즈 바 옆 수치 입력 칸에 0.7를 입력한다.

Then 3D Visualizer 탭 View 패널의 Rendering의 불투명도가 달라졌는지 확인

And 3D Visualizer 탭 Preset 패널의 Opacity 사이즈 바의 위치가 입력한 수치에 따라 바뀌었는지 확인

When 3D Visualizer 탭 Preset 패널의 Overlay 체크박스를 클릭한다.

Then Preset 패널 Overlay 체크박스에 체크표시가 사라지는지 확인

And 3D Visualizer 탭 View 패널의 HT 이미지에서 Rendering이 사라지는지 확인

Then 3D Visualizer TA종료