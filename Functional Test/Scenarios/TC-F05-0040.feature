﻿Feature: Create Cube

  Scenario: Playground Canvas 탭에 생성된 Cube 아이콘 확인
Given Create Cube 탭이 열려있다.

When Cube의 이름과 경로를 지정하고 Create Cube 탭의 OK 버튼을 누른다.

Then Playground canvas 탭에 Normal Cube 아이콘과 Cube 이름이 나타남을 확인