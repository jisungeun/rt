﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Use Labeling 체크박스 체크
Given BA Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

And BA Application Parameter 패널의 Use Labeling 체크박스가 비어있다.

When Use Labeling 체크박스를 클릭한다.

Then ‘Select Option’ 문구와 옵션 드롭다운 버튼이 나타나는지 확인

And ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 나타나는지 확인

And Use Labeling 체크박스가 하이라이트 되며 체크 되는지 확인