﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭의 Time points setting 패널에서 Apply time points to all data 버튼을 눌러 선택한 TCF에 Time point를 입력
Given Basic Analyzer[T] 탭의 Batch Run setting 탭이 열려 있다.

When Batch Run setting 탭의 Time points setting 패널에서 Time points를 1, 2, 3으로 세팅한다.

And Batch Run setting 탭 Time points setting 패널의 Apply time points to all data 버튼을 누른다.

Then Batch Run setting 탭에 있는 TCF의 Selected time points 칸에서 세팅한 Time points가 적용되었는지 확인