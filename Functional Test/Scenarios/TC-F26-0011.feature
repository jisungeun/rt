﻿Feature: Mask Editor; After BA/BAT Batch Run

  Scenario: Report[T] 탭의 Current File 드롭다운 버튼에서 겹쳐진 셀 선택
Given TomoAnalysis에 Report[T] 탭이 열려 있다.

When Report[T] 탭 Graph 패널에서 점이 겹쳐진 부분을 클릭한다.

And Report[T] 탭 Graph 패널의 Current file 드롭다운 버튼을 클릭한다.

Then Report[T] 탭 Graph 패널의 Current file 드롭다운 버튼에 여러 개의 파일 리스트가 나타났는지 확인

Then TA종료