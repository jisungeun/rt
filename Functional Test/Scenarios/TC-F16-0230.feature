﻿Feature: BAT Report[T]

  Scenario: Report[T] 탭 이름 옆의 x 버튼을 눌러 Report[T] 탭을 종료
Given Report[T] 탭이 열려 있다.

When Application tab 바의 Report[T] 탭 이름 옆의 x 버튼을 클릭한다.

Then Report[T] 탭이 종료되는지 확인