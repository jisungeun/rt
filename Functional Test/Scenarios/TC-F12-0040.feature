﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, 마스크가 생성되어 있지 않으면 Labeling 탭에서 Execute 버튼을 눌렀을 때 경고창이 나타남
  
Given Basic Analyzer 탭에서 Use Labeling 체크박스가 체크되어 있다.

And Basic Analyzer 탭 Mask Viewing panel에 마스크가 생성되어 있지 않다.

When Basic Analyzer 탭 Processing mode 패널의 Option에서 Multi Label을 선택하고 Labeling 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭에 경고창이 떴는지 확인

Then error TA종료