﻿Feature: BA Application Parameter

  Scenario: Basic Analyzer app을 적용했을 때, Application Parameter 패널의 SELECT PROCESSOR에서 마우스 휠을 이용하여 RI Thresholding으로 변경
Given Playground canvas에서 Basic Analyzer 아이콘이 선택되어 있다.

And Application Parameter 패널에 AI Segmentation이 선택되어 있다.

When Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼에 마우스 포인터를 대고 마우스 휠을 아래로 내린다.

Then Application Parameter 패널 SELECT PROCESSOR에서 RI Thresholding이 선택되는지 확인

And Application Parameter의 UI가 달라지는지 확인 