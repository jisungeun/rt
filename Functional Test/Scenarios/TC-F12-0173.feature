﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Basic Measurement의 table에서 preset을 ‘Customize’로 설정했을 때 값을 직접 입력
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.

When Basic Analyzer 탭 Basic Measurement 탭의 table에서 default column의 Preset 드롭다운 버튼을 Customize로 선택한다.

And Basic Analyzer 탭 Basic Measurement 탭의 table에서 Baseline RI row의 cell을 클릭하고 키보드로 ‘12345’를 입력하고 키보드의 엔터 키를 누른다.

And Basic Analyzer 탭 Basic Measurement 탭의 table에서 RII row의 cell을 클릭하고 키보드로 ‘12345’를 입력하고 키보드의 엔터 키를 누른다.

Then Basic Analyzer 탭 Basic Measurement 탭의 table에서 Baseline RI 값과 RII값이 모두 ‘12345’로 변경되는지 확인