﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 RI 마스크 보기
  
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.

When Basic Analyzer Single Run 탭 Processing mode 패널에서 UL Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer Single Run 탭의 Mask Viewing panel 3D 화면과 2D 화면에 RI 마스크가 생성되었는지 확인

When 3D 마스크 캔버스 좌하단 정육면체의 위쪽 변을 클릭한다.

Then 해당 변이 정면으로 오도록 마스크와 정육면체의 방향이 움직였는지 확인

When 3D 마스크 캔버스 좌하단 정육면체의 Left면을 클릭한다.

Then Left 면이 정면으로 오도록 마스크와 정육면체의 방향이 움직였는지 확인

When 3D 마스크 캔버스 좌하단 정육면체의 Left 면의 오른쪽 위 점을 클릭한다.

Then 해당 점이 정면으로 오도록 마스크와 정육면체의 방향이 움직였는지 확인

When 마우스를 커서 모양이 바뀌는 3D 마스크 캔버스 위로 가져가 마우스 휠을 위로 두 칸 올린다.

Then 3D 마스크가 두 단위 확대됨을 확인

When 마우스를 커서 모양이 바뀌는 3D 마스크 캔버스 위로 가져가 마우스 휠을 아래로 두 칸 내린다.

Then 3D 마스크가 두 단위 축소됨을 확인

When Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 위로 5칸 올린다.

Then Basic Analyzer 탭의 Mask Viewing panel HT 단층 사진의 Z축이 올라가는 지 확인

When Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 아래로 5칸 내린다.

Then Basic Analyzer 탭의 Mask Viewing panel HT 단층 사진의 Z축이 내려가는 지 확인

Then TA종료