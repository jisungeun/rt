﻿Feature: BA Batch RUN

  Scenario: Report 탭의 Select organelle 패널의 기본 값이 Membrane으로 체크되어 있는지 확인
Given Basic Analyzer Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.

When Batch Run 버튼을 누른다.

And 프로세싱 바 팝업의 진행이 완료된다.

Then Report 탭 Select organelle 패널의 기본 값이 Whole cell에 체크됐는지 확인

And Report 탭의 Whole cell 글자가 하이라이트 됐는지 확인

And Inter-Cube mean comparison panel의 그래프 타이틀이 Whole cell으로 되어 있는지 확인

And Graph of individual cell 패널의 타이틀이 Whole cell으로 되어 있는지 확인