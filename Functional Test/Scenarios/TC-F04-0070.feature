﻿Feature: Create Hypercube

  Scenario: Hypercube가 선택되어 있을 때 Playground canvas의 다른 공간을 눌러 hypercube의 선택을 해제
	
Given Hypercube가 선택되어 있다.

When Hypercube 아이콘 바깥의Playground canvas 아무 영역이나 누른다.

Then Playground canvas 위의 Active hypercube 아이콘이 Normal hypercube 아이콘으로 바뀜을 확인