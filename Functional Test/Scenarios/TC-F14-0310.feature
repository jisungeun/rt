﻿"""Feature: BAT AI Segmentation Single RUN

  Scenario:BAT Single Run 탭의 Apply parameter 버튼을 눌러 BA 탭에서 사용한 Parameter를 PM 탭의 Application parameter 패널로 출력
Given Basic Analyzer[T] Single Run 탭의 Apply Parameter 버튼이 하이라이트 되어 있다.

When Basic Analyzer[T] Single Run 탭의 Apply Parameter 버튼을 누른다.

Then Basic Analyzer[T] Single Run 탭의 Processing mode 패널과 Project Manager 탭의 BAT Application Parameter 패널의 두 Basic Measurement 값이 일치하는지 확인

And 화면에서 Basic Analyzer[T] Single Run 탭이 Project Manager 탭으로 넘어갔는지 확인

And Application tab 바에서 Basic Analyzer[T] Single Run 탭 이름의 하이라이트가 사라지고 Project Manager 탭 이름에 하이라이트가 생겼는지 확인