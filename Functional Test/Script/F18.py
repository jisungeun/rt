﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
  
@given("3개의 cube에 대해 2D Image Gallery 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(50, 14)
  dlgOpenProject = Aliases.TomoAnalysis.dlgOpenProject
  toolbar = dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar
  toolbar.ClickItem("‎Prefix_File", False)
  toolbar.ClickItemXY("‎Prefix_File", 33, 8, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\2D_Image_Gallery\\2D_Image_Gallery.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(136, 23)

@when("2D Image Gallery 탭 오른쪽의 스크롤 바를 마우스로 클릭한 상태로 내린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.panel_base.scrollarea_bottom_round.qt_scrollarea_vcontainer.ScrollBar.wPosition = 434

@then("2D Image Gallery 탭 화면이 내려갔는지 확인")
def step_impl():
  Regions.h7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.CubeTCFWidget.panel_contents.h7)

@when("2D Image Gallery 탭 오른쪽의 스크롤 바를 마우스로 클릭한 상태로 올린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.panel_base.scrollarea_bottom_round.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0

@then("2D Image Gallery 탭 화면이 올라갔는지 확인")
def step_impl():
  Regions.h71.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.CubeTCFWidget2.panel_contents.h7)

@when("2D Image Gallery 탭 화면에서 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.MouseWheel(-1)

@when("2D Image Gallery 탭 화면에서 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents
  widget.MouseWheel(1)

@given("2D Image Gallery 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(50, 14)
  dlgOpenProject = Aliases.TomoAnalysis.dlgOpenProject
  toolbar = dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar
  toolbar.ClickItem("‎Prefix_File", False)
  toolbar.ClickItemXY("‎Prefix_File", 33, 8, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\2D_Image_Gallery\\2D_Image_Gallery.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(136, 23)

@when("2D Image Gallery 탭의 Rep 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.panel_base.toolWidget.bt_round_tool.ClickButton()

@then("2D Image Gallery 탭의 오른쪽 하단에 ‘There is no selected item.’ 이라는 안내문이 나타났는지 확인")
def step_impl():
  Regions.Label1.Check(Aliases.TomoAnalysis.TC_ToastMessageBox.Label)

@then("TA종료")
def step_impl():
  aqUtils.Delay(3000)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
