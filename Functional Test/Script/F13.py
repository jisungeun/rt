﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob

@given("Playground canvas에서 Basic Analyzer[T] 아이콘이 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(190, 22)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")

@given("BAT Application Parameter 패널에 AI Segmentation이 선택되어 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("AI Segmentation")

@when("BAT Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.DropDown()

@when("BAT Application Parameter 패널의 RI Thresholding을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

@then("BAT Application Parameter의 UI가 RI Thresholding UI로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo, "wText", cmpEqual, "RI Thresholding")

@given("BAT Application Parameter 패널에 RI Thresholding이 선택되어 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

@when("BAT Application Parameter 패널의 AI Segmentation을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("AI Segmentation")

@then("BAT Application Parameter의 UI가 AI Segmentation UI로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo, "wText", cmpEqual, "AI Segmentation")

@when("BAT Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼에 마우스 포인터를 대고 마우스 휠을 아래로 내린다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame
  comboBox = frame.procCombo
  vlabel = frame.h8
  comboBox.MouseWheel(-1)
  comboBox.MouseWheel(-1)

@then("BAT Application Parameter 패널 SELECT PROCESSOR에서 RI Thresholding이 선택되는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo, "wText", cmpEqual, "RI Thresholding")

@then("Application Parameter의 UI가 RI Thresholding UI로 바뀌는지 확인")
def step_impl():
  Regions.processorParamFrame5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

  
@when("BAT Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame
  comboBox = frame.procCombo
  vlabel = frame.h8
  comboBox.MouseWheel(1)
  comboBox.MouseWheel(1)

@then("BAT Application Parameter 패널 SELECT PROCESSOR에서 AI Segmentation이 선택되는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo, "wText", cmpEqual, "AI Segmentation")

@given("BAT Application Parameter에서 AI Segmentation이 프로세서로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(190, 22)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")


@given("BAT Application Parameter 패널의 Basic Measurement버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  toolButton = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse
  toolButton.CheckButton(cbUnchecked)
  toolButton.CheckButton(cbChecked)

@when("RI Thresholding 상황에서는 BAT Application Parameter 패널 Basic Measurement 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 206
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse.ClickButton()

@then("BAT Application Parameter 패널의 Basic Measurement 밑의 UI가 접히는지 확인")
def step_impl():
  Regions.Widget3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget)

@then("BAT Application Parameter 패널의 Basic Measurement 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse45.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("BAT Application Parameter 패널의 Basic Measurement 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 203
  buttonBasicMeasurement = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse
  buttonBasicMeasurement.ClickButton()


@then("BAT Application Parameter 패널의 Basic Measurement 밑의 UI가 펼쳐지는지 확인")
def step_impl():
  Regions.CollapseWidget32.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)

@then("BAT Application Parameter 패널의 Basic Measurement 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse47.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("BAT Application Parameter 패널의 맨 위 드롭다운 버튼에서 AI Segmentation을 프로세서로 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("AI Segmentation")

@then("BAT Application Parameter 패널 Basic Measurement의 table의 Preset row가 왼쪽부터 순서대로 Protein, Protein, Protein, Lipid로 선택되어 있는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 21
  Tables.TableWidget77.Check()


@when("BAT Application Parameter 패널 Whole cell column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Protein")

@when("BAT Application Parameter 패널 Nucleus column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_1.ClickItem("Protein")

@when("BAT Application Parameter 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_2.ClickItem("Protein")

@when("BAT Application Parameter 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 21
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParamTable.TableWidget.qt_scrollarea_viewport.Preset_3.ClickItem("Protein")

@then("BAT Application Parameter 패널 Basic Measurement의 table의 네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인")
def step_impl():
  Tables.TableWidget16.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("BAT Application Parameter 패널 Whole cell column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 21
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParamTable.TableWidget.qt_scrollarea_viewport.Preset_0.ClickItem("Hemoglobin")

@when("BAT Application Parameter 패널 Nucleus column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 32
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParamTable.TableWidget.qt_scrollarea_viewport.Preset_1.ClickItem("Hemoglobin")

@when("BAT Application Parameter 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 32
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParamTable.TableWidget.qt_scrollarea_viewport.Preset_2.ClickItem("Hemoglobin")

@when("BAT Application Parameter 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 32
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParamTable.TableWidget.qt_scrollarea_viewport.Preset_3.ClickItem("Hemoglobin")

@then("BAT Application Parameter 패널 Basic Measurement의 table의 네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.15로 입력되는지 확인")
def step_impl():
  Tables.TableWidget17.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("BAT Application Parameter 패널 Whole cell column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Lipid")

@when("BAT Application Parameter 패널 Nucleus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 21
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParamTable.TableWidget.qt_scrollarea_viewport.Preset_1.ClickItem("Lipid")

@when("BAT Application Parameter 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_2.ClickItem("Lipid")

@when("BAT Application Parameter 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_3.ClickItem("Lipid")

@then("BAT Application Parameter 패널 Basic Measurement의 table의 네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인")
def step_impl():
  Tables.TableWidget18.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("BAT Application Parameter 패널 Basic Measurement의 table에서 모든 column의 Preset 드롭다운 버튼을 Customize로 선택한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport
  widget.Preset_Setter_0.ClickItem("Customize")
  widget.Preset_Setter_1.ClickItem("Customize")
  widget.Preset_Setter_2.ClickItem("Customize")
  widget.Preset_Setter_3.ClickItem("Customize")

@when("BAT Application Parameter 패널 Basic Measurement의 table의 Baseline RI row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("Baseline RI", "Nucleolus")
  tableWidget.Keys("1")
  tableWidget.Keys("12345")

@when("BAT Application Parameter 패널 Basic Measurement의 table의 RII row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("RII", "Nucleolus")
  tableWidget.Keys("1")
  tableWidget.Keys("12345")

  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  lineEdit.Keys("[Enter]")

@then("BAT Application Parameter 패널 Basic Measurement의 table의 네 column에서 모두 Baseline RI 값과 RII값이 ‘{arg}’로 변경되는지 확인")
def step_impl(param1):
  Tables.TableWidget21.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(190, 22)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

@given("BAT Application Parameter 패널의 UL Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  Regions.bt_collapse23.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse2)

@when("BAT Application Parameter 패널의 UL Threshold 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse.ClickButton()

@then("BAT Application Parameter 패널의 UL Threshold 밑의 UI가 접히는지 확인")
def step_impl():
  Regions.processorParamFrame6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@then("BAT Application Parameter 패널의 UL Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse55.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("BAT Application Parameter 패널의 UL Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse.ClickButton()

@then("BAT Application Parameter 패널의 UL Threshold 밑의 UI가 펼쳐지는지 확인")
def step_impl():
  Regions.F13_1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl)

@then("BAT Application Parameter 패널의 UL Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse24.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("BAT Application Parameter 패널의 ‘Assign lower threshold’ 의 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 1
  scrollBar.wPosition = 0

@then("BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바 옆의 수치가 1.0010으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.0010")

@when("BAT Application Parameter 패널의 ‘Assign lower threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다2.")
def step_impl():

  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 438.4
  scrollBar.wPosition = 0
  
@when("BAT Application Parameter 패널의 ‘Assign lower threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():

  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 438.4
  scrollBar.wPosition = 0
  
@then("BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바 옆의 수치가 2.0000으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "2.0000")

@when("BAT Application Parameter 패널의 ‘Assign upper threshold’ 의 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  slider = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSlider
  slider.wPosition = 358.4


@then("BAT Application Parameter 패널의 ‘Assign upper threshold’ 사이즈 바 옆의 수치가 1.0010으로 바뀌었는지 확인")
def step_impl():
  raise NotImplementedError

@when("BAT Application Parameter 패널의 ‘Assign upper threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSlider.wPosition = 1000

@then("BAT Application Parameter 패널의 ‘Assign upper threshold’ 사이즈 바 옆의 수치가 2.0000으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "2.0000")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바 옆의 칸에 {arg}을 입력한다.")
def step_impl(param1):
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  doubleSpinBox = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Drag(49, 9, -61, 1)
  doubleSpinBox.Keys("1")
  doubleSpinBox.wValue = 1

@when("BAT Application Parameter 패널의 ‘Assign upper threshold’ 사이즈 바 옆의 칸에 {arg}를 입력한다.")
def step_impl(param1):
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  doubleSpinBox = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin
  lineEdit = doubleSpinBox.qt_spinbox_lineedit
  lineEdit.Drag(41, 9, -52, 13)
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  lineEdit.Drag(51, 11, -53, 10)
  doubleSpinBox.Keys("2")
  doubleSpinBox.wValue = 2

@then("BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바와 ‘Assign upper threshold’ 사이즈 바가 각각 왼쪽 끝과 오른쪽 끝으로 이동했는지 확인")
def step_impl():
  Regions.BAT1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("BAT Application Parameter 패널의 ‘Assign lower threshold’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 1.3501000000000001

@then("BAT Application Parameter 패널의 ‘Assign lower threshold’의 수치가 1.3501로 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3501")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("BAT Application Parameter 패널의 ‘Assign upper threshold’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.wValue = 1.4400999999999999

@then("BAT Application Parameter 패널의 ‘Assign upper threshold’ 의 수치가 1.4401로 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4401")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("BAT Application Parameter 패널의 ‘Assign lower threshold’ 수치 칸 옆의 아래 방향 화살표를 한 번 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 1.3499000000000001

@then("BAT Application Parameter 패널의 ‘Assign lower threshold’ 의 수치가 1.3499로 내려가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3499")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("BAT Application Parameter 패널의 ‘Assign upper threshold’ 수치 칸 옆의 아래 방향 화살표를 한 번 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.wValue = 1.4399

@then("BAT Application Parameter 패널의 ‘Assign upper threshold’ 의 수치가 1.4399로 내려가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4399")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("BAT Application Parameter 패널의 ‘Assign lower threshold’의 UI에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl
  doubleSpinBox = TC_ParameterControl.ScalarUi.valueSpin
  lineEdit = doubleSpinBox.qt_spinbox_lineedit
  lineEdit.Click(52, 18)
  scrollBar.wPosition = 0
  lineEdit.Click(46, 11)
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3501000000000001

@when("BAT Application Parameter 패널의 ‘Assign upper threshold’ 의 UI에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl
  doubleSpinBox = TC_ParameterControl.ScalarUi2.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Click(50, 13)
  doubleSpinBox.qt_spinbox_lineedit.MouseWheel(1)
  doubleSpinBox.wValue = 1.4400999999999999

@then("BAT Application Parameter 패널의 ‘Assign lower threshold’가 1.3501로, ‘Assign upper threshold’가 1.4401로 각각 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3501")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4401")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("BAT Application Parameter 패널의 ‘Assign lower threshold’의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl
  doubleSpinBox = TC_ParameterControl.ScalarUi.valueSpin
  lineEdit = doubleSpinBox.qt_spinbox_lineedit
  lineEdit.Click(52, 18)
  scrollBar.wPosition = 0
  lineEdit.Click(46, 11)
  lineEdit.MouseWheel(-1)

@when("BAT Application Parameter 패널의 ‘Assign upper threshold’ 의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl
  doubleSpinBox = TC_ParameterControl.ScalarUi2.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Click(50, 13)
  doubleSpinBox.qt_spinbox_lineedit.MouseWheel(-1)


@then("BAT Application Parameter 패널의 ‘Assign lower threshold’가 1.3499로, ‘Assign upper threshold’가 1.4399로 각각 내려가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3499")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4399")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("BAT Application Parameter 패널의 Auto Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  Regions.bt_collapse25.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)

@when("BAT Application Parameter 패널의 Auto Threshold 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse2.ClickButton()

@then("BAT Application Parameter 패널의 Auto Threshold 밑의 체크박스가 접히는지 확인")
def step_impl():
  Regions.processorParamFrame7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@then("BAT Application Parameter 패널의 Auto Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse26.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("BAT Application Parameter 패널의 Auto Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse2.ClickButton()

@then("BAT Application Parameter 패널의 Auto Threshold 밑의 체크박스가 펼쳐지는지 확인")
def step_impl():
  Regions.processorParamFrame8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@then("BAT Application Parameter 패널의 Auto Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse27.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("BAT Application Parameter 패널의 Auto lower threshold 체크박스가 비어있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  TC_StrCheckBox = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox
  TC_StrCheckBox.Click(15, 14)
  scrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  TC_StrCheckBox.Click(14, 14)

@when("BAT Application Parameter 패널의 Auto lower Threshold 체크박스를 클릭한다.")
def step_impl():
  tomoAnalysis_ProjectManager_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  scrollBar = tomoAnalysis_ProjectManager_AppUI_MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 25
  scrollBar.wPosition = 23
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParamControl.TC_StrCheckBox.Click(-1)
  scrollBar.wPosition = 119

@then("BAT Application Parameter 패널의 Auto Threshold 탭에 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼이 나타나는지 확인")
def step_impl():
  TC_CollapseWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4
  TC_CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  TC_ParameterControl = TC_CollapseWidget.contentsFrame.TC_ParameterControl
  TC_ParameterControl.ScalarUi.valueSlider.MouseWheel(-1)
  TC_ParameterControl.Label2.MouseWheel(-1)
  Regions.CollapseWidget19.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)

@then("BAT Application Parameter 패널의 Auto lower Threshold 체크박스가 하이라이트 되며 체크 되는지 확인")
def step_impl():
  Regions.BAT2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("BAT Application Parameter에서 Auto lower Threshold 체크박스가 체크되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(190, 22)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(8, 12)

@then("AT Application Parameter 패널 Auto Threshold 탭의 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼이 사라지는지 확인")
def step_impl():
  Regions.CollapseWidget20.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)

@then("BAT Application Parameter 패널의 Auto lower Threshold 체크박스가  체크 해제 되는지 확인")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse.CheckButton(cbChecked)
  Regions.CollapseWidget21.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("BAT Application Parameter 패널의 Select Algorithm 드롭다운 버튼에서 Entropy를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 120
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 78
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Entropy")


@then("BAT Application Parameter 패널의 Select Algorithm 버튼에 Entropy가 선택되었는지 확인")
def step_impl():
  Regions.strCombo3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo)

@when("BAT Application Parameter 패널의 Select Algorithm 드롭다운 버튼에서 Moments Preserving을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Moment Preserving")

@then("BAT Application Parameter 패널의 Select Algorithm 버튼에 Moments Preserving이 선택되었는지 확인")
def step_impl():
  Regions.CollapseWidget22.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)

@given("BAT Application Parameter 패널의 Select Algorithm 버튼에 Moments Preserving이 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Moment Preserving")

@when("BAT Application Parameter 패널의 Select Algorithm 드롭다운 버튼에서 Otsu를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Otsu")

@then("BAT Application Parameter 패널의 Select Algorithm 버튼에 Otsu가 선택되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo, "wText", cmpEqual, "Otsu")

@given("BAT Application Parameter 패널의 Select Algorithm 버튼에 Otsu가 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Otsu")

@when("BAT Application Parameter 패널의 Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 60
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.MouseWheel(-1)

@given("BAT Application Parameter 패널의 Select Algorithm 버튼에 Entropy가 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 155
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Entropy")

@when("BAT Application Parameter 패널의 Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 155
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 18
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.HoverMouse()
  comboBox.MouseWheel(-1)


@when("BAT Application Parameter 패널의 Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 두 칸 이상 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 60
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  vlabel = TC_ParameterControl.Label
  comboBox.MouseWheel(1)
  comboBox.MouseWheel(1)

@given("BAT Application Parameter 패널의 Labeling 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse2.CheckButton(cbUnchecked)

@when("BAT Application Parameter 패널의 Labeling 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse.ClickButton()

@then("BAT Application Parameter 패널 Labeling 밑의 체크박스가 펼쳐지는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 146
  Regions.processorParamFrame10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@then("BAT Application Parameter 패널의 Labeling 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse29.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse2)

@given("BAT Application Parameter 패널의 Labeling 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  buttonLabeling = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse
  buttonLabeling.ClickButton()
  scrollBar.wPosition = 0
  buttonLabeling.ClickButton()

@then("BAT Application Parameter 패널 Labeling 밑의 체크박스가 접히는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 103
  Regions.processorParamFrame9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@then("BAT Application Parameter 패널의 Labeling 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse28.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse2)

@given("BAT Application Parameter 패널의 Use Labeling 체크박스가 비어있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  TC_ProcessorControl = scrollArea.qt_scrollarea_viewport.panel_contents
  widget = TC_ProcessorControl.ScrollArea.qt_scrollarea_viewport.Widget
  widget.CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  Regions.CollapseWidget23.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3)


@when("BAT Application Parameter 패널의 Use Labeling 체크박스를 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(9, 10)

@then("BAT Application Parameter 패널 Labeling 탭에 ‘Select Option’ 문구와 옵션 드롭다운 버튼, ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 나타나는지 확인")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  widget = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget
  widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox.MouseWheel(-1)
  widget.CollapseWidget3.MouseWheel(-1)
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 125
  Regions.BAT3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl)

@then("BAT Application Parameter 패널의 Use Labeling 체크박스가 하이라이트 되며 체크 되는지 확인")
def step_impl():
  Regions.BAT4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox)


@given("BAT Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(190, 22)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.DropDown()
  tomoAnalysis.ComboBoxPrivateContainer.ListView.qt_scrollarea_viewport.Drag(102, 67, 0, 5)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 170
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(6, 9)



@then("BAT Application Parameter 패널 Labeling 탭에 ‘Select Option’ 문구와 옵션 드롭다운 버튼, ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 사라지는지 확인")
def step_impl():
  Regions.CollapseWidget24.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3)

@then("BAT Application Parameter 패널의 Use Labeling 체크박스가  체크 해제 되는지 확인")
def step_impl():
  Regions.BAT5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox)

@when("BAT Application Parameter 패널의 Select Option 드롭다운 버튼에서 Largest Label 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Largest Label")

@then("BAT Application Parameter 패널의 Select Option 버튼이 Largest Label 옵션으로 변경되었는지 확인")
def step_impl():
  Regions.CollapseWidget25.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3)

@when("BAT Application Parameter 패널의 Select Option 드롭다운 버튼에서 Multi Labels 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Multi Labels")

@then("BAT Application Parameter 패널의 Select Option 버튼이 Multi Labels 옵션으로 변경되었는지 확인")
def step_impl():
  Regions.CollapseWidget26.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3)

@given("BAT Application Parameter 패널의 Select Option 버튼에 Multi Labels가 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Multi Labels")

@when("BAT Application Parameter 패널의 Select Option 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 0
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  comboBox.CloseUp()
  TC_ParameterControl.MouseWheel(-1)
  TC_ParameterControl.MouseWheel(-1)

@given("BAT Application Parameter 패널의 Select Option 버튼에 Largest Label이 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Largest Label")

@when("BAT Application Parameter 패널의 Select Option 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 0
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  comboBox.CloseUp()
  comboBox.MouseWheel(1)
  comboBox.MouseWheel(1)
  
@when("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 밑의 사이즈 바를 마우스로 왼쪽 끝으로 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 1
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0

@then("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 사이즈 바 옆의 수치가 0.1001로 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "0.1001")

@when("BAT Application Parameter 패널 Labeling ‘Assign Size of Neglectable Particle’ 밑의 사이즈 바를 마우스로 오른쪽 끝으로 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 0
  slider = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider
  slider.wPosition = 11
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  slider.wPosition = 1000
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0

@then("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 사이즈 바 옆의 수치가 100.0000으로 변경되었는지 확인")
def step_impl():
  Regions.qt_spinbox_lineedit.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit)

@when("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 사이즈 바 옆의 칸에 50.0000을 입력한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 0
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  doubleSpinBox = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin
  lineEdit = doubleSpinBox.qt_spinbox_lineedit
  lineEdit.DblClick(28, 9)
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  lineEdit.DblClick(28, 9)
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  lineEdit.DblClick(15, 13)
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  lineEdit.Click(9, 13)
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  lineEdit.Click(11, 13)
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  scrollBar.wPosition = 180
  scrollBar2.wPosition = 0
  lineEdit.Drag(5, 14, 38, 2)
  doubleSpinBox.Keys("850")
  doubleSpinBox.wValue = 85
  doubleSpinBox.Keys("[BS]")
  doubleSpinBox.wValue = 8
  doubleSpinBox.Keys("[BS]5")
  doubleSpinBox.wValue = 5
  doubleSpinBox.Keys("0")
  doubleSpinBox.wValue = 50

@then("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 사이즈 바가 값에 따라 정 중앙에 위치하는지 확인")
def step_impl():
  Regions.BAT6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl)

@when("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 25.0001

@then("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 값이 25.0001로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "25.0001")

@when("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 수치 칸 옆의 아래 방향 화살표를 한 번 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 180
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 24.9999

@then("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 값이 24.9999로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "24.9999")

@when("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 의 UI에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 120
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 135
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  doubleSpinBox = TC_ParameterControl.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Click(41, 10)
  doubleSpinBox.MouseWheel(1)



@when("BAT Application Parameter 패널 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 120
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 135
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  doubleSpinBox = TC_ParameterControl.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Click(41, 10)
  doubleSpinBox.MouseWheel(-1)


@given("BAT Application Parameter 패널의 Basic Measurement 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  Regions.bt_collapse40.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)


@when("BAT Application Parameter 패널에서 RI Thresholding을 프로세서로 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

@then("BAT Application Parameter 패널 Basic Measurement의 table의 Preset row가 Protein으로 선택되어 있는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0, "wText", cmpEqual, "Protein")

@when("BAT Application Parameter 패널 Basic Measurement 탭 table의 default column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Hemoglobin")

@then("BAT Application Parameter 패널 Basic Measurement 탭 table에서 Baseline RI 값이 1.3337로, RII값이 0.15로 입력되는지 확인")
def step_impl():
  Tables.TableWidget34.Check()

@when("BAT Application Parameter 패널 Basic Measurement 탭 table의 default column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Lipid")

@then("BAT Application Parameter 패널 Basic Measurement 탭 table에서 Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인")
def step_impl():
  Tables.TableWidget35.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("BAT Application Parameter 패널 Basic Measurement의 table에서 default column의 Preset 드롭다운 버튼을 Customize로 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Customize")

@when("BAT Application Parameter 패널 Basic Measurement 탭 table에서 Baseline RI row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 203
  scrollBar.wPosition = 203
  scrollBar.wPosition = 203
  tableWidget = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.DblClickCell("Baseline RI", "default")
  scrollBar.wPosition = 203
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  lineEdit.Click(26, 15)
  lineEdit.SetText("12345")
  lineEdit.Keys("[Enter]")



@when("BAT Application Parameter 패널 Basic Measurement 탭 table에서 RII row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 203
  scrollBar.wPosition = 203
  scrollBar.wPosition = 203
  tableWidget = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.DblClickCell("RII", "default")
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  lineEdit.SetText("12345")
  lineEdit.Keys("[Enter]")

@then("BAT Application Parameter 패널 Basic Measurement 탭 table에서 Baseline RI 값과 RII값이 ‘{arg}’로 변경되는지 확인")
def step_impl(param1):
  Tables.TableWidget36.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@then("BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바가 ‘Assign upper threshold’ 값인 1.4400까지만 올라가는지 확인")
def step_impl():
  raise NotImplementedError

@then("BAT Application Parameter 패널의 ‘Assign upper threshold’ 사이즈 바가 ‘Assign lower threshold’ 값인 1.3500까지만 내려가는지 확인")
def step_impl():
  raise NotImplementedError

@then("BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바가 ‘Assign upper threshold’ 값을 넘지 않는 1.4384까지만 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4384")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@then("BAT Application Parameter 패널의 ‘Assign upper threshold’ 사이즈 바가 ‘Assign lower threshold’ 값을 넘지 않는 1.3584까지만 내려가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3584")

@when("BAT Application Parameter 패널 Basic Measurement 탭 table의 default column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Protein")

@then("BAT Application Parameter 패널 Basic Measurement 탭 table에서 Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("BAT Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼을 클릭하고 RI Thresholding을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

@then("BAT Application Parameter 패널 Basic Measurement의 table의 Whole cell column에서 Baseline RI 값이 1.3337로, RII값이 0.15로 입력되는지 확인")
def step_impl():
  Tables.TableWidget65.Check()

@then("BAT Application Parameter 패널 Basic Measurement의 table의 Nucleus column에서 Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인")
def step_impl():
  Tables.TableWidget66.Check()

@when("BAT Application Parameter 패널 Basic Measurement의 table에서 Nucleolus column의 Preset 드롭다운 버튼을 Customize로 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParamTable.TableWidget.qt_scrollarea_viewport.Preset_2.ClickItem("Customize")

@then("BAT Application Parameter 패널 Basic Measurement의 table의 Nucleolus column에서 Baseline RI 값과 RII값이 ‘{arg}’로 변경되는지 확인")
def step_impl(param1):
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 21
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterTable.TableWidget.ClickCell("Baseline RI", "Nucleolus")

@then("BAT Application Parameter 패널 Basic Measurement의 table의 Lipid droplet column에서 Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인")
def step_impl():
  Tables.TableWidget67.Check()

@then("BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바가 ‘Assign upper threshold’ 값을 넘지 않는 값 까지만 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4384")

@then("입력한 값으로 수치가 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "2.0000")

@then("BAT Application Parameter 패널 Auto Threshold 탭의 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼이 사라지는지 확인")
def step_impl():
  Regions.processorParamFrame12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@when("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points 칸에 time points를 {arg}, {arg}, 3을 입력한다.")
def step_impl(param1, param2):
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints
  lineEdit.Drag(136, 15, -174, 11)
  lineEdit.Drag(144, 18, -124, -1)
  lineEdit.SetText("1,2,3")

@when("Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스에서 Multi-layered mask 스위치를 키고 All 체크박스를 클릭한다.") 
def step_impl():
  tomoAnalysis_BasicAnalysisTime_Plugins_VizControlPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel
  tomoAnalysis_BasicAnalysisTime_Plugins_VizControlPanel.bt_switch.ClickButton()
  tomoAnalysis_BasicAnalysisTime_Plugins_VizControlPanel.controlSocket.CheckBox5.ClickButton(cbChecked)

@when("Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스에서 Multi-layered mask 스위치를 켠다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch.ClickButton()

@then("And Basic Analyzer[T] 탭 Mask Viewing panel에서 multi layer로 마스크가 표현됐는지 확인")
def step_impl():
  Regions.TomoAnalysis206.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 202)
  
@when("Basic Analyzer[T] 탭의 Tool box에서 Cell Instance, Whole Cell, Nucleus, Nucleolus 체크박스를 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭에 Lipid droplet 마스크만 남아있는지 확인")
def step_impl():
  Regions.TomoAnalysis205.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 202)

@when("Basic Analyzer[T] 탭의 Tool box에서 Nucleolus, Lipid droplet 체크박스를 클릭한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket
  widget.CheckBox3.Click(-1)
  widget.CheckBox4.Click(-1)

@then("Basic Analyzer[T] 탭에 Nucleolus 마스크만 남아있는지 확인")
def step_impl():
  aqUtils.Delay(5000)
  Regions.TomoAnalysis207.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 202)

@then("BAT Application Parameter 패널 Basic Measurement의 table의 Whole cell column에서 RII값이 0.15로 입력되는지 확인")
def step_impl():
  Tables.TableWidget78.Check()

@then("BAT Application Parameter 패널 Basic Measurement의 table의 Nucleus column에서 RII값이 0.135로 입력되는지 확인")
def step_impl():
  Tables.TableWidget79.Check()

@then("BAT Application Parameter 패널 Basic Measurement의 table의 Lipid droplet column에서 RII값이 0.19로 입력되는지 확인")
def step_impl():
  Tables.TableWidget81.Check()

@then("BAT Application Parameter 패널 Basic Measurement의 table의 Nucleolus column에서 RII값이 ‘{arg}’로 변경되는지 확인")
def step_impl(param1):
  Tables.TableWidget82.Check()
