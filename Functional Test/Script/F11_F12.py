﻿import shutil
import os
from distutils.dir_util import copy_tree
import time
import glob


@given("Basic Analyzer Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_Batch_RUN\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Batch_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Batch_RUN\\BA_Batch_RUN.tcpro")


@when("Batch Run 버튼을 누른다.")
def step_impl():

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(81, 15)

@then("Basic Analyzer 탭이 생기는지 확인")
def step_impl():
  Regions.BR1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@then("프로세싱 바 팝업이 생기는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.ProgressDialog, "QtText", cmpEqual, "")

@then("Application tab 바에서 Project Manager 탭 이름의 하이라이트가 사라지고 Basic Analyzer 이름에 하이라이트가 생겼는지 확인")
def step_impl():
  Regions.BR2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@then("Batch Run 탭 이름에 하이라이트가 생겼는지 확인")
def step_impl():
  Regions.qt_tabwidget_tabbar1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_tabbar)


@when("프로세싱 바 팝업의 진행이 완료된다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");
  Regions.BR3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)

@then("프로세싱 바 팝업과 Basic Analyzer 탭이 사라지는 지 확인")
def step_impl():
  Regions.BR4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@then("table과 graph가 출력된 Report 탭이 화면에 나타나는지 확인")
def step_impl():
  Regions.BR5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@then("Report 탭 Select Parameter 패널이 Mean RI에 체크됐는지 확인")
def step_impl():
  Regions.parametersGroupBox.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox)

@then("Report 탭의 Mean RI 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.RadioButton.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton)
  
@given("BR Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo.ClickItem("AI Segmentation")

@then("Inter-Cube mean comparison panel의 그래프 y축이 Mean RI로 되어 있는지 확인")
def step_impl():
  Regions.Widget5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget)

@then("Graph of individual cell 패널의 y축이 Mean RI로 되어 있는지 확인")
def step_impl():
  Regions.Widget6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)
  
@given("Report 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Report\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Reportplayground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Report\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Report'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Batch_RUN - Report'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(65, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Batch_RUN - Report\\BA_Batch_RUN - Report.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("AI Segmentation")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(90, 9)

@when("Report 탭 Select Prameter 패널에서 Volume 체크박스에 체크한다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox
  groupBox.RadioButton7.ClickButton()
  groupBox.RadioButton.ClickButton()

@then("Report 탭 Select Parameter 패널이 Volume에 체크됐는지 확인")
def step_impl():
  Regions.parametersGroupBox1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox)

@then("Report 탭의 Volume 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.RadioButton1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton2)

@then("Inter-Cube mean comparison panel의 그래프 y축이 Volume으로 되어 있는지 확인")
def step_impl():
  Regions.Widget56.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget, False, False, 1000)

@then("Graph of individual cell 패널의 y축이 Volume으로 되어 있는지 확인")
def step_impl():
  Regions.Widget57.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget, False, False, 1000)
  
@when("Report 탭 Select Prameter 패널에서 Surface area 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton2.ClickButton()

@then("Report 탭 Select Parameter 패널이 Surface area에 체크됐는지 확인")
def step_impl():
  Regions.parametersGroupBox2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox, False, False, 1000)

@then("Report 탭의 Surface area 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.RadioButton2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton3, False, False, 1000)

@then("Inter-Cube mean comparison panel의 그래프 y축이 Surface area로 되어 있는지 확인")
def step_impl():
  Regions.Widget9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget, False, False, 1000)

@then("Graph of individual cell 패널의 y축이 Surface area로 되어 있는지 확인")
def step_impl():
  Regions.Widget10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget, False, False, 1000)

@when("Report 탭 Select Prameter 패널에서 Projected area 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton3.ClickButton()

@then("Report 탭 Select Parameter 패널이 Projected area에 체크됐는지 확인")
def step_impl():
  Regions.parametersGroupBox3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox)
  Regions.parametersGroupBox4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox)

@then("Report 탭의 Projected area 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.RadioButton3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton4)

@then("Inter-Cube mean comparison panel의 그래프 y축이 Projected area로 되어 있는지 확인")
def step_impl():
  Regions.Widget11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget)

@then("Graph of individual cell 패널의 y축이 Projected area로 되어 있는지 확인")
def step_impl():
  Regions.Widget12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)

@when("Report 탭 Select Prameter 패널에서 Dry mass 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton4.ClickButton()

@then("Report 탭 Select Parameter 패널이 Dry mass에 체크됐는지 확인")
def step_impl():
  Regions.parametersGroupBox5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox)

@then("Report 탭의 Dry mass 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.RadioButton4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton5)
  Regions.RadioButton5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton5)

@then("Inter-Cube mean comparison panel의 그래프 y축이 Dry mass로 되어 있는지 확인")
def step_impl():
  Regions.Widget13.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget, False, False, 1000)

@then("Graph of individual cell 패널의 y축이 Dry mass로 되어 있는지 확인")
def step_impl():
  Regions.Widget15.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)

@when("Report 탭 Select Prameter 패널에서 Concentration 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton5.ClickButton()

@then("Report 탭 Select Parameter 패널이 Concentration에 체크됐는지 확인")
def step_impl():
  Regions.parametersGroupBox6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox)

@then("Report 탭의 Concentration 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.RadioButton6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton6)

@then("Inter-Cube mean comparison panel의 그래프 y축이 Concentration으로 되어 있는지 확인")
def step_impl():
  Regions.Widget16.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget)

@then("Graph of individual cell 패널의 y축이 Concentration으로 되어 있는지 확인")
def step_impl():
  Regions.Widget17.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)

  
@when("Report 탭 Select Prameter 패널에서 Sphericity 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton6.ClickButton()

@then("Report 탭 Select Parameter 패널이 Sphericity에 체크됐는지 확인")
def step_impl():
  Regions.parametersGroupBox7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox)

@then("Report 탭의 Sphericity 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.RadioButton8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton7)

@then("Inter-Cube mean comparison panel의 그래프 y축이 Sphericity로 되어 있는지 확인")
def step_impl():
  Regions.Widget18.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget)

@then("Graph of individual cell 패널의 y축이 Sphericity로 되어 있는지 확인")
def step_impl():
  Regions.Widget19.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)

@then("Report 탭 Select organelle 패널의 기본 값이 Whole cell에 체크됐는지 확인")
def step_impl():
  Regions.RadioButton9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox.RadioButton)

@then("Report 탭의 Whole cell 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.organellesGroupBox1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox)

@then("Inter-Cube mean comparison panel의 그래프 타이틀이 Whole cell으로 되어 있는지 확인")
def step_impl():
  Regions.Widget22.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget)

@then("Graph of individual cell 패널의 타이틀이 Whole cell으로 되어 있는지 확인")
def step_impl():
  Regions.Widget23.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)

@when("Report 탭 Select organelle 패널에서 Nucleus 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox.RadioButton.ClickButton()

@then("Report 탭 Select Parameter 패널이 Nucleus에 체크됐는지 확인")
def step_impl():
  Regions.organellesGroupBox.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox)

@then("Report 탭의 Nucleus 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.RadioButton10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox.RadioButton2)

@then("Inter-Cube mean comparison panel의 그래프 타이틀이 Nucleus로 되어 있는지 확인")
def step_impl():
  Regions.Widget20.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget)

@then("Graph of individual cell 패널의 타이틀이 Nucleus로 되어 있는지 확인")
def step_impl():
  Regions.Widget21.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)

@when("Report 탭 Select organelle 패널에서 Nucleolus 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox.RadioButton2.ClickButton()

@then("Report 탭 Select Parameter 패널이 Nucleolus에 체크됐는지 확인")
def step_impl():
  Regions.organellesGroupBox2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox)

@then("Report 탭의 Nucleolus 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.RadioButton11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox.RadioButton3)

@then("Inter-Cube mean comparison panel의 그래프 타이틀이 Nucleolus로 되어 있는지 확인")
def step_impl():
  Regions.Widget24.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget)

@then("Graph of individual cell 패널의 타이틀이 Nucleolus로 되어 있는지 확인")
def step_impl():
  Regions.Widget25.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)

@when("Report 탭 Select organelle 패널에서 Lipid droplet 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox.RadioButton3.ClickButton()

@then("Report 탭 Select Parameter 패널이 Lipid droplet에 체크됐는지 확인")
def step_impl():
  Regions.organellesGroupBox3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox)

@then("Report 탭의 Lipid droplet 글자가 하이라이트 됐는지 확인")
def step_impl():
  Regions.RadioButton12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox.RadioButton4)

@then("Inter-Cube mean comparison panel의 그래프 타이틀이 Lipid droplet으로 되어 있는지 확인")
def step_impl():
  Regions.Widget26.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget)

@then("Graph of individual cell 패널의 타이틀이 Lipid droplet으로 되어 있는지 확인")
def step_impl():
  Regions.Widget27.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)

  
@given("Report 탭의 Inter-Cube mean comparison panel에 graph가 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Report\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Reportplayground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Report\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Report'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Batch_RUN - Report'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(65, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(62, 10)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Batch_RUN - Report\\BA_Batch_RUN - Report.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(90, 9)
  dat = r'E:\Regression_Test\*.csv'
  for f in glob.glob(dat):
    os.remove(f)
  
@when("Inter-Cube mean comparison panel에서 ‘Table' 텍스트를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.ClickTab("Table")

@then("Inter-Cube mean comparison panel에 graph 대신 table이 나타나는지 확인")
def step_impl():
  Tables.tableWidget84.Check()

@then("Inter-Cube mean comparison panel의 Graph 글자의 하이라이트가 꺼지고 Table 글자가 하이라이트 됨을 확인")
def step_impl():
  Regions.qt_tabwidget_tabbar2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_tabbar)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Report 탭의 Inter-Cube mean comparison panel에 table이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(65, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(62, 10)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Batch_RUN - Report\\BA_Batch_RUN - Report.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(90, 9)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.ClickTab("Table")
@when("Inter-Cube mean comparison panel에서 Graph 글자를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.ClickTab("Graph")

@then("Inter-Cube mean comparison panel에 table 대신 graph가 나타나는지 확인")
def step_impl():
  Regions.Widget28.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget)

@then("Inter-Cube mean comparison panel의 Table 글자의 하이라이트가 꺼지고 Graph 글자가 하이라이트 됨을 확인")
def step_impl():
  Regions.qt_tabwidget_tabbar3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_tabbar)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Report 탭의 Graph theme 스위치 버튼이 꺼져 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(65, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(62, 10)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Batch_RUN - Report\\BA_Batch_RUN - Report.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(90, 9)

@when("Report 탭의 Graph theme 스위치 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.bt_switch.ClickButton()

@then("Report 탭의 Graph theme 스위치 버튼이 하이라이트 됐는지 확인")
def step_impl():
  Regions.bt_switch3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.bt_switch)

@then("Report 탭의 Graph theme 스위치 버튼이 오른쪽으로 움직였는지 확인")
def step_impl():
  Regions.bt_switch4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.bt_switch)

@then("Inter-Cube mean comparison panel과 Graph of individual cell 패널의 그래프 배경 색이 검은 색으로 바뀌었는지 확인")
def step_impl():
  Regions1.Check(Regions.CreateRegionInfo(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow, 353, 108, 1545, 371, False))
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Report 탭의 Graph theme 스위치 버튼이 켜져 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(65, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(62, 10)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Batch_RUN - Report\\BA_Batch_RUN - Report.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(90, 9)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.bt_switch.ClickButton()

@then("Report 탭의 Graph theme 스위치 버튼이 하이라이트 해제됐는지 확인")
def step_impl():
  Regions.bt_switch5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.bt_switch)

@then("Report 탭의 Graph theme 스위치 버튼이 왼쪽으로 움직였는지 확인")
def step_impl():
  Regions.bt_switch6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.bt_switch)

@then("Inter-Cube mean comparison panel과 Graph of individual cell 패널의 그래프 배경 색이 흰 색으로 바뀌었는지 확인")
def step_impl():
  Regions2.Check(Regions.CreateRegionInfo(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow, 339, 61, 1563, 435, False))
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Graph of individual cell 패널의 Min 값을 입력하는 창에 원하는 수치를 넣고 키보드의 엔터 키를 누른다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.yMinInputBox
  lineEdit = doubleSpinBox.qt_spinbox_lineedit
  lineEdit.Drag(44, 12, -38, 0)
  lineEdit.Drag(44, 11, -45, 0)
  doubleSpinBox.Keys("1")
  doubleSpinBox.wValue = 1
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 1

@then("Graph of individual cell 패널 그래프의 y축 최솟값이 설정된 값으로 바뀌었는지 확인")
def step_impl():
  Regions.Widget30.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Graph of individual cell 패널의 Max 값을 입력하는 창에 원하는 수치를 넣고 키보드의 엔터 키를 누른다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.yMaxInputBox
  lineEdit = doubleSpinBox.qt_spinbox_lineedit
  lineEdit.DblClick(10, 19)
  lineEdit.Drag(50, 11, -46, 0)
  doubleSpinBox.Keys("2")
  doubleSpinBox.wValue = 2
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 2

@then("Graph of individual cell 패널 그래프의 y축 최댓값이 설정된 값으로 바뀌었는지 확인")
def step_impl():
  Regions.Widget31.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Inter-Cube mean comparison panel의 Capture 버튼을 누른다.")
def step_impl():
  img = r'E:\Regression_Test\*.png'
  for f in glob.glob(img):
    os.remove(f)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.captureOverviewGraphButton.ClickButton()

@when("경로와 파일 이름을 지정하고 저장 버튼을 누른다.")
def step_impl():
  dlgExportHypercubeGraph = Aliases.TomoAnalysis.dlgExportHypercubeGraph
  HWNDView = dlgExportHypercubeGraph.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(50, 11)
  HWNDView.FloatNotifySink.ComboBox.Edit.Click(43, 4)
  dlgExportHypercubeGraph.btn_S.ClickButton()

@then("해당 저장 경로에 지정한 이름의 그래프 png 파일이 생성되었는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(25, 22)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test.Item.DblClick(64, 13)
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.untitled_png.Item, "Value", cmpEqual, "untitled.png")

@then("캡쳐 된 그래프가 Inter-Cube mean comparison panel의 그래프와 같은 내용인지 확인")
def step_impl():
  UIItem = Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.untitled_png
  UIProperty = UIItem.Item
  UIProperty.Click(35, 14)
  UIProperty.Click(35, 14)
  UIItem.Click(120, 13)
  UIProperty.Click(98, 13)
  UIItem.Click(132, 14)
  UIProperty.DblClick(110, 14)
  Regions.D2DImageControl3.Check(Aliases.App.Item.MainPage.D2DImageControl)
  Aliases.ApplicationFrameHost.wndApplicationFrameWindow.Close()
  Aliases.explorer.wndGenerated_File.Close()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
  
@given("Report 탭의 Inter-Cube mean comparison panel에 table이 출력되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(65, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(62, 10)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Batch_RUN - Report\\BA_Batch_RUN - Report.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(90, 9)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.ClickTab("Table")
  dat = r'E:\Regression_Test\*.csv'
  for f in glob.glob(dat):
    os.remove(f)
    
@when("Inter-Cube mean comparison panel의 Export to Excel 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.dataTable.tabWidget.qt_tabwidget_stackedwidget.DataTab.TableItemWidget.exportButton.ClickButton()

@when("Export 할 경로와 파일 이름을 지정하고 저장 버튼을 누른다.")
def step_impl():
  dlgExportHypercubeReport = Aliases.TomoAnalysis.dlgExportHypercubeReport
  HWNDView = dlgExportHypercubeReport.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(87, 18)
  dlgExportHypercubeReport.btn_S.ClickButton()
  
@then("해당 저장 경로에 지정한 이름의 table csv 파일이 생성되었는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(40, 9)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(49, 7)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.CubeName_csv.Item, "Value", cmpEqual, "CubeName.csv")
  Aliases.explorer.wndGenerated_File.Close()

  
@then("저장한 table이 Inter-Cube mean comparison panel의 table과 같은 내용인지 확인")
def step_impl():
  TestedApps.EXCEL1.Run(1, True)
  Regions.EXCEL71.Check(Regions.CreateRegionInfo(Aliases.EXCEL.wndXLMAIN3.XLDESK.EXCEL7, 21, 18, 324, 174, False))
  Aliases.EXCEL.wndXLMAIN3.Close()
  Aliases.explorer.wndGenerated_File.Close()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Graph of individual cell 패널의 Capture 버튼을 누른다.")
def step_impl():
  img = r'E:\Regression_Test\*.png'
  for f in glob.glob(img):
    os.remove(f)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.detailsGraphCaptureButton.ClickButton()

@when("파일 탐색기에서 경로와 파일 이름을 지정하고 저장 버튼을 누른다.")
def step_impl():
  dlgExportCubeGraph = Aliases.TomoAnalysis.dlgExportCubeGraph
  HWNDView = dlgExportCubeGraph.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(34, 17)
  comboBox = HWNDView.FloatNotifySink.ComboBox
  comboBox.Edit.Click(38, 7)
  comboBox.SetText("untitled1.png")
  dlgExportCubeGraph.btn_S.ClickButton()

@then("해당 저장 경로에 지정한 이름을 가진 Inter-Cube mean comparison panel의 그래프의 png 파일이 생성되었는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(29, 16)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test.Item.DblClick(48, 14)
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.untitled1_png.Item, "Value", cmpEqual, "untitled1.png")
  Aliases.explorer.wndGenerated_File.Close()

@then("캡쳐된 그래프가 Graph of individual cell 패널의 그래프와 같은 내용인지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(27, 22)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink.ShellView.Item
  UIItemsView.Regression_Test.Item.DblClick(34, 12)
  UIItemsView.untitled1_png.Item.DblClick(61, 6)
  Regions.D2DImageControl2.Check(Aliases.App.Item.MainPage.D2DImageControl)
  Aliases.ApplicationFrameHost.wndApplicationFrameWindow2.Close()
  Aliases.explorer.wndGenerated_File.Close()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("2개 이상의 cube를 batch run 한 결과의 Report 탭이 열려 있다.")  
def step_impl():
  raise NotImplementedError

@given("Report 탭 Data Table 패널에 출력된 개별 data table 하단부에 스크롤 바가 생성되어 있다.")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.dataTable.tabWidget.qt_tabwidget_stackedwidget.DataTab.TableItemWidget.tableView.qt_scrollarea_hcontainer.ScrollBar, "Exists", cmpEqual, True)

@when("스크롤 바를 마우스로 클릭한 상태로 움직인다.")
def step_impl():

  raise NotImplementedError

@then("각 데이터 table이 화면에 보이는 영역이 달라지는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.Click(923, 21)
  Regions.DataTab.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.dataTable.tabWidget.qt_tabwidget_stackedwidget.DataTab, False, False, 1000)

@when("Data Table 패널 오른쪽의 화살표 버튼\\(>)을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.dataTable.tabWidget.qt_tabwidget_stackedwidget.DataTab.bt_arrow_right.ClickButton()

@then("스크롤 바를 민 것처럼 화면에 표시되는 Data Table 패널 전체가 오른쪽으로 이동함을 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.Click(923, 21)
  Regions.DataTab1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.dataTable.tabWidget.qt_tabwidget_stackedwidget.DataTab, False, False, 1000)

@when("Data Table 패널 왼쪽의 화살표 버튼\\(<)을 누른다.")
def step_impl():
  raise NotImplementedError

@then("스크롤 바를 민 것처럼 화면에 표시되는 Data table이 왼쪽으로 이동함을 확인")
def step_impl():
  raise NotImplementedError

@when("Data Table 패널 테이블의 셀 하나에 마우스 포인터를 가져가서 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Data Table 패널의 헤더가 모두 파란색으로 하이라이트 됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Data Table 패널의 저장할 table 위의 Export 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("파일 탐색기에 파일 저장 경로와 파일 이름을 지정하고 저장 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("저장한 table이 Data Table 패널의 table과 같은 내용인지 확인")
def step_impl():
  raise NotImplementedError

@when("Application tab 바의 Report 탭 이름 옆의 x 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Report 탭이 종료되는지 확인")
def step_impl():
  raise NotImplementedError

@given("BA Application Parameter에서 프로세서로 RI Thresholding이 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_RI_Thresholding'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(38, 36)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(72, 12)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.Prefix_File
  UIItem.Item.Click(78, 14)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.BA_RI_Thresholding
  UIItem.Item.Click(111, 12)
  UIItem.Keys("[Enter]")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_RI_Thresholding\\BA_RI_Thresholding.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

@when("BA Application Parameter 패널의 Single RUN 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(82, 17)

@when("Single RUN 팝업에서 App이 연결된 hypercube 안의 TCF 중 Single RUN 하고 싶은 TCF를 더블 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.DblClick(154, 98)

@when("Single RUN 팝업에서 App이 연결된 hypercube 안의 TCF 중 Single RUN 하고 싶은 TCF를 선택하고 팝업 안의 Single RUN 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = Aliases.TomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(55, 66)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(55, 18)

@then("BA 탭에서 Processing mode 패널의 맨 위 드롭다운 버튼이 RI Thresholding으로 설정됐는지 확인")
def step_impl():
  Regions.procCombo1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo)

@then("Processing mode 패널의 UL Threshold 탭에서 Execute 버튼이 하이라이트 되었는지 확인")
def step_impl():
  Regions.groupBox3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@then("Project Manager 탭의 BA Application Parameter 패널의 parameter 설정과 Basic Analyzer 탭의 Processing mode 패널의 Parameter 설정이 서로 일치하는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3500")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4400")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer Single Run 탭의 UL Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  Regions.bt_collapse13.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)

@when("Basic Analyzer Single Run 탭의 UL Threshold 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse3.ClickButton()

@then("Basic Analyzer Single Run 탭의 UL Threshold 버튼 밑의 UI가 접히는지 확인")
def step_impl():
  Regions.groupBox4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox)

@then("Basic Analyzer Single Run 탭의 UL Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse14.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer Single Run 탭의 UL Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse3.ClickButton()

@then("Basic Analyzer Single Run 탭의 UL Threshold 버튼 밑의 UI가 펼쳐지는지 확인")
def step_impl():
  Regions.CollapseWidget11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)

@then("Basic Analyzer Single Run 탭의 UL Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse15.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’ 의 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 1

@then("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’ 사이즈 바 옆의 수치가 1.0010으로 바뀌었는지 확인")
def step_impl():
  Regions.valueSpin.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin)

@when("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 438.4

@then("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’ 사이즈 바 옆의 수치가 2.0000으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "2.0000")

@when("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 의 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSlider.wPosition = 358.4

@then("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 사이즈 바 옆의 수치가 1.0010으로 바뀌었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab
  TC_ParameterControl = widget.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  TC_ParameterControl.Drag(64, 110, 66, -1)
  widget.Click(393, 52)
  TC_ParameterControl.ScalarUi2.valueSlider.wPosition = 1000

@then("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 사이즈 바 옆의 수치가 2.0000으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "2.0000")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’ 사이즈 바 옆의 칸에 {arg}을 입력한다.")
def step_impl(param1):
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Drag(46, 18, -55, -4)
  doubleSpinBox.Keys("1")
  doubleSpinBox.wValue = 1

@when("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 사이즈 바 옆의 칸에 {arg}를 입력한다.")
def step_impl(param1):
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin
  lineEdit = doubleSpinBox.qt_spinbox_lineedit
  doubleSpinBox.Keys("2")
  doubleSpinBox.wValue = 2

@then("‘Assign lower threshold’ 사이즈 바와 ‘Assign upper threshold’ 사이즈 바가 각각 왼쪽 끝과 오른쪽 끝으로 이동했는지 확인")
def step_impl():
  Regions.CollapseWidget12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 1.3501000000000001

@then("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’의 수치가 1.3501로 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3501")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.wValue = 1.4400999999999999
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4401")

@then("Basic Analyzer Single Run 탭의 ‘Assign upper threshold'의 수치가 1.4401로 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4401")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’ 수치 칸 옆의 아래 방향 화살표를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 1.3499000000000001

@then("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’의 수치가 1.3499로 내려가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3499")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 수치 칸 옆의 아래 방향 화살표를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.wValue = 1.4399
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4399")

@then("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’의 수치가 1.4399로 내려가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4399")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’의 UI에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  TC_ParameterControl = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  TC_ParameterControl.Label
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit
  lineEdit.Click(37, 15)
  lineEdit.MouseWheel(1)

@when("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 의 UI에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  TC_ParameterControl = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  TC_ParameterControl = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  TC_ParameterControl.Click(196, 110)
  doubleSpinBox = TC_ParameterControl.ScalarUi2.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Click(43, 16)
  doubleSpinBox.MouseWheel(1)

@then("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’가 1.3501로, ‘Assign upper threshold’ 가 1.4401로 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3501")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4401")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  TC_ParameterControl = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  TC_ParameterControl.Label
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit
  lineEdit.Click(37, 15)
  lineEdit.MouseWheel(-1)

@when("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  TC_ParameterControl = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  TC_ParameterControl = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  TC_ParameterControl.Click(196, 110)
  doubleSpinBox = TC_ParameterControl.ScalarUi2.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Click(43, 16)
  doubleSpinBox.MouseWheel(-1)

@then("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’가 1.3499로, ‘Assign upper threshold’ 가 1.4399로 내려가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3499")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4399")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Basic Analyzer Single Run 탭 Processing mode 패널에서 UL Threshold 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer Single Run 탭의 Mask Viewing panel에 3D RI 마스크가 생성되었는지 확인")
def step_impl():
  Regions.TomoAnalysis27.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis3)

@then("Basic Analyzer Single Run 탭의 Mask Viewing panel에 HT 이미지에 마스크가 생성되었는지 확인")
def step_impl():
  Regions.TomoAnalysis28.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis4)

@then("Basic Analyzer Single Run 탭 Mask Viewing panel 상단에 Cell Instance, Whole Cell 체크박스와 Cell-wise filter 스위치, Opacity 조절 버튼이 있는 툴박스가 생성되었는지 확인")
def step_impl():
  Regions.VizControlPanel1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Basic Analyzer 탭의 Mask Viewing panel에 3D RI 마스크가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_RI_Thresholding'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(38, 36)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(72, 12)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.Prefix_File
  UIItem.Item.Click(78, 14)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.BA_RI_Thresholding
  UIItem.Item.Click(111, 12)
  UIItem.Keys("[Enter]")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_RI_Thresholding\\BA_RI_Thresholding.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(115, 10)
  tomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(120, 54)
  widget.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@when("Basic Analyzer Single Run 탭 Mask Viewing panel 상단 Mask 툴박스의 Whole Cell 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton.ClickButton()

@then("Mask Viewing panel의 3D 마스크 캔버스에서 Cell 전체에 RI 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis29.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis3)

@then("Mask Viewing panel의 HT 사진의 Cell 영역 전체에 RI 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis30.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis4)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Basic Analyzer 탭에서 RI Thresholding이 프로세서로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_RI_Thresholding'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(38, 36)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(72, 12)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.Prefix_File
  UIItem.Item.Click(78, 14)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.BA_RI_Thresholding
  UIItem.Item.Click(111, 12)
  UIItem.Keys("[Enter]")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_RI_Thresholding\\BA_RI_Thresholding.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(115, 10)
  tomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(120, 54)

@given("Basic Analyzer 탭의 Auto Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  toolButton = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse
  toolButton.CheckButton(cbUnchecked)
  toolButton.CheckButton(cbChecked)

@when("Basic Analyzer 탭의 Auto Threshold 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(7, 8)

@then("Basic Analyzer 탭의 Auto Threshold 밑의 체크박스가 접히는지 확인")
def step_impl():
  Regions.groupBox7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox)

@then("Basic Analyzer 탭의 Auto Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse16.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer 탭의 Auto Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  buttonAutoThreshold = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse2
  buttonAutoThreshold.ClickButton()
  buttonAutoThreshold.ClickButton()

@then("Basic Analyzer 탭의 Auto Threshold 밑의 체크박스가 펼쳐지는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse2.ClickButton()
  Regions.CollapseWidget13.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)

@then("Basic Analyzer 탭의 Auto Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse17.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Basic Analyzer 탭 Auto threshold 탭의 Auto lower threshold 체크박스가 체크되어 있지 않다.")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox, "checked", cmpEqual, False)

@when("Basic Analyzer 탭의 Auto lower Threshold 체크박스를 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(10, 9)

@then("Basic Analyzer 탭의 Auto lower Threshold 체크박스 밑에 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼, 하이라이트 된 Execute 버튼이 나타나는지 확인")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  scrollBar.wPosition = 0
  Regions.groupBox11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox)

@then("Basic Analyzer 탭의 Auto lower Threshold 체크박스가 하이라이트 되며 체크 되는지 확인")
def step_impl():
  Regions.RI2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox)

@then("Basic Analyzer 탭의 UL Threshold 탭의 Execute 버튼이 하이라이트 해제 되었는지 확인")
def step_impl():
  Regions.TR3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer 탭에서 Auto lower Threshold 체크박스가 체크되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_RI_Thresholding'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(38, 36)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(72, 12)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.Prefix_File
  UIItem.Item.Click(78, 14)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.BA_RI_Thresholding
  UIItem.Item.Click(111, 12)
  UIItem.Keys("[Enter]")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_RI_Thresholding\\BA_RI_Thresholding.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(115, 10)
  tomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(120, 54)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(7, 8)

@then("Basic Analyzer 탭의 Auto lower Threshold 체크박스 밑의 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼, Execute 버튼이 사라지는지 확인")
def step_impl():
  Regions.CollapseWidget14.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)

@then("Basic Analyzer 탭의 Auto lower Threshold 체크박스가 체크 해제 되는지 확인")
def step_impl():
  Regions.RI4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox)

@then("Basic Analyzer 탭의 UL Threshold 탭의 Execute 버튼이 하이라이트 되었는지 확인")
def step_impl():
  Regions.RI5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에서 Entropy를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Entropy")

@then("Basic Analyzer 탭 Processing mode 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Entropy가 선택되었는지 확인")
def step_impl():
  Regions.RI6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에서 Moments Preserving을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Moment Preserving")

@then("Basic Analyzer 탭 Processing mode 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Moments Preserving이 선택되었는지 확인")
def step_impl():
  Regions.RI7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에서 Otsu를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Otsu")

@then("Basic Analyzer 탭 Processing mode 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Otsu가 선택되었는지 확인")
def step_impl():
  Regions.RI8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer 탭의 Processing mode 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Otsu가 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Otsu")

@when("Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 0
  comboBox.CloseUp()
  comboBox.MouseWheel(-1)

@given("Basic Analyzer 탭의 Processing mode 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Entropy가 선택되어 있다.")
def step_impl():
  Regions.strCombo1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo)

@when("Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 0
  comboBox.CloseUp()
  comboBox.HoverMouse()
  comboBox.MouseWheel(-1)
  comboBox.MouseWheel(-1)
  
@given("Basic Analyzer 탭의 Processing mode 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Moments Preserving이 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Moment Preserving")

@when("Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 두 칸 이상 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 0
  comboBox.CloseUp()
  comboBox.MouseWheel(1)
  comboBox.MouseWheel(1)
  
@when("Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에서 Otsu 알고리즘을 선택하고 Auto Threshold 탭의 Execute 버튼을 누른다.")
def step_impl():
  Regions.RI9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer 탭의 UL Threshold 탭에서 값과 사이즈 바가 Otsu 알고리즘에 의해 자동으로 바뀌었는지 확인")
def step_impl():
  Regions.RI10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에서 Entropy 알고리즘을 선택하고 Auto Threshold 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  TC_ParameterControl.StrComboBox.strCombo.ClickItem("Entropy")
  scrollBar.wPosition = 0
  TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer 탭의 UL Threshold 탭에서 값과 사이즈 바가 Entropy 알고리즘에 의해 자동으로 바뀌었는지 확인")
def step_impl():
  Regions.CollapseWidget39.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)

  
@when("Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에서 Moment Preserving 알고리즘을 선택하고 Auto Threshold 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  TC_ParameterControl.StrComboBox.strCombo.ClickItem("Moment Preserving")
  scrollBar.wPosition = 0
  TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer 탭의 UL Threshold 탭에서 값과 사이즈 바가 Moment Preserving 방식에 의해 자동으로 바뀌었는지 확인")
def step_impl():
  Regions.CollapseWidget15.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)

@given("Basic Analyzer 탭의 Labeling 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  buttonLabeling = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse2
  buttonLabeling.ClickButton()
  buttonLabeling.ClickButton()

@when("Basic Analyzer 탭의 Labeling 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse2.ClickButton()

@then("Basic Analyzer 탭의 Labeling 버튼 밑의 체크박스가 접히는지 확인")
def step_impl():
  Regions.groupBox8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox)

@then("Basic Analyzer 탭의 Labeling 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse19.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer 탭의 Labeling 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  buttonLabeling = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse2
  buttonLabeling.ClickButton()
  buttonLabeling.ClickButton()

@then("Basic Analyzer 탭 Labeling 버튼 밑의 체크박스가 펼쳐지는지 확인")
def step_impl():
  Regions.CollapseWidget16.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3)

@then("Basic Analyzer 탭의 Labeling 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse20.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Basic Analyzer 탭에서 Use Labeling 체크박스가 체크되어 있다.")
def step_impl():

  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_RI_Thresholding'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(38, 36)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(72, 12)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.Prefix_File
  UIItem.Item.Click(78, 14)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.BA_RI_Thresholding
  UIItem.Item.Click(111, 12)
  UIItem.Keys("[Enter]")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_RI_Thresholding\\BA_RI_Thresholding.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(115, 10)
  tomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(120, 54)

  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TC_StrCheckBox.Click(10, 9)

  
@when("Basic Analyzer 탭 Processing mode 패널의 Select Option 드롭다운 버튼을 누르고 Largest Label 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Largest Label")

@then("Basic Analyzer 탭 Processing mode 패널의 Select Option 버튼이 Largest Label 옵션으로 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo, "wText", cmpEqual, "Largest Label")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer 탭 Labeling 탭의 Use Labeling 체크박스에 체크 되어있지 않다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  TC_StrCheckBox = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox
  TC_StrCheckBox.Click(8, 16)
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  TC_StrCheckBox.Click(8, 12)

@when("Basic Analyzer 탭의 Use Labeling 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(25, 12)

@then("Basic Analyzer 탭 Processing mode 패널에 ‘Select Option’ 문구와 옵션 드롭다운 버튼, ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 나타나는지 확인")
def step_impl():
  TC_ParameterControl = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  TC_ParameterControl.TC_StrCheckBox.MouseWheel(-1)
  TC_ParameterControl.StrComboBox.strCombo.MouseWheel(-1)
  TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit.MouseWheel(-1)
  TC_ParameterControl.buttonExecute.MouseWheel(-1)
  Regions.RI12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl)
  Regions.RI13.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4)

@then("Basic Analyzer 탭의 Use Labeling 체크박스가 하이라이트 되며 체크 되는지 확인")
def step_impl():
  Regions.RI14.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox)

@then("Basic Analyzer 탭의 Labeling 탭의 Execute 버튼이 하이라이트 되는지 확인")
def step_impl():
  Regions.RI15.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@then("Basic Analyzer 탭 Processing mode 패널에서 ‘Select Option’ 문구와 옵션 드롭다운 버튼, ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 사라지는지 확인")
def step_impl():
  Regions.panel_contents5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents)

@then("Basic Analyzer 탭의 Use Labeling 체크박스가 체크 해제 되는지 확인")
def step_impl():
  Regions.RI16.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox)

@then("Basic Analyzer 탭의 Labeling 탭의 Execute 버튼이 하이라이트 해제되는지 확인")
def step_impl():
  Regions.RI17.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer 탭 Processing mode 패널의 Select Option 드롭다운 버튼을 누르고 Multi Labels 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Multi Labels")

@then("Basic Analyzer 탭 Processing mode 패널의 Select Option 버튼이 Multi Labels 옵션으로 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo, "wText", cmpEqual, "Multi Labels")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer 탭 Processing mode 패널의 Select Option 버튼에 Multi Labels가 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  comboBox = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo
  comboBox.Click(105, 4)
  scrollBar.wPosition = 0
  comboBox.ClickItem("Multi Labels")

@when("Basic Analyzer 탭 Select Option 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 0
  comboBox.CloseUp()
  vlabel = TC_ParameterControl.Label
  comboBox.MouseWheel(-1)
  comboBox.MouseWheel(-1)


@given("Basic Analyzer 탭 Processing mode 패널의 Select Option 버튼에 Largest Label가 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Largest Label")

@when("Basic Analyzer 탭 Select Option 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 0
  comboBox.CloseUp()
  vlabel = TC_ParameterControl.Label
  comboBox.MouseWheel(1)
  comboBox.MouseWheel(1)

@when("Basic Analyzer 탭 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 밑의 사이즈 바를 마우스로 왼쪽 끝으로 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 1
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0

@then("Basic Analyzer 탭 ‘Assign Size of Nehlectable Particle’ 사이즈 바 옆의 수치가 0.1001로 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "0.1001")

@when("Basic Analyzer 탭 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 밑의 사이즈 바를 마우스로 오른쪽 끝으로 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 1000
  scrollBar.wPosition = 0

@then("Basic Analyzer 탭 Labeling 탭의 ‘Assign Size of Neglectable Particle’ 사이즈 바 옆의 수치가 100.0000으로 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "100.0000")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer 탭 ‘Assign Size of Neglectable Particle’ 사이즈 바 옆의 칸에 50.0000을 입력한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  doubleSpinBox = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.DblClick(42, 17)
  doubleSpinBox.Keys("[BS][BS][BS][BS]")
  doubleSpinBox.wValue = 1
  doubleSpinBox.Keys("[BS]5")
  doubleSpinBox.wValue = 5
  doubleSpinBox.Keys("0")
  doubleSpinBox.wValue = 50

@then("Basic Analyzer 탭 ‘Assign Size of Neglectable Particle’ 사이즈 바가 값에 따라 정 중앙에 위치하는지 확인")
def step_impl():
  Regions.valueSlider.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer 탭의 ‘Assign Size of Neglectable Particle’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 25.0001

@then("Basic Analyzer 탭의 ‘Assign Size of Neglectable Particle’ 값이 25.0001로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin, "wValue", cmpEqual, 25.0001)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer 탭의 ‘Assign Size of Neglectable Particle’ 수치 칸 옆의 아래 방향 화살표를 한 번 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 24.9999

@then("Basic Analyzer 탭의 ‘Assign Size of Neglectable Particle’ 값이 24.9999로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "24.9999")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer 탭의 ‘Assign Size of Neglectable Particle’ 의 UI에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  doubleSpinBox = TC_ParameterControl.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Click(42, 12)
  doubleSpinBox.MouseWheel(1)

@when("Basic Analyzer 탭의 ‘Assign Size of Neglectable Particle’ 의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  doubleSpinBox = TC_ParameterControl.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Click(50, 10)
  doubleSpinBox.MouseWheel(-1)

@when("Basic Analyzer 탭 Processing mode 패널의 Option에서 Multi Label을 선택하고 Labeling 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  TC_ParameterControl.StrComboBox.strCombo.ClickItem("Multi Labels")
  scrollBar.wPosition = 0
  TC_ParameterControl.buttonExecute.ClickButton()




@then("Basic Analyzer 탭 Mask Viewing panel에서 작은 크기의 알갱이 같은 mask가 없어지는 방식으로 mask가 수정되었는지 확인")
def step_impl():
  Regions.TomoAnalysis33.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis3)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Basic Analyzer 탭 Processing mode 패널의 Option에서 Largest Label을 선택하고 Labeling 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 0
  comboBox.ClickItem("Largest Label")
  scrollBar.wPosition = 0
  TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer 탭 Mask Viewing panel에서 가장 큰 크기의 mask만 남는 방식으로 mask가 수정되었는지 확인")
def step_impl():
  Regions.TomoAnalysis34.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)
  
@given("Basic Analyzer 탭의 Basic Measurement 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  Regions.bt_collapse35.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse)

@when("Basic Analyzer 탭의 Basic Measurement 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse2.ClickButton()

@then("Basic Analyzer 탭의 Basic Measurement 밑의 UI가 접히는지 확인")
def step_impl():
  Regions.Widget32.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget)

@then("Basic Analyzer 탭의 Basic Measurement 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse36.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer 탭의 Basic Measurement 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse.CheckButton(cbUnchecked)

@then("Basic Analyzer 탭의 Basic Measurement 버튼 밑의 UI가 펼쳐지는지 확인")
def step_impl():
  Regions.panel_contents7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents)

@when("Basic Analyzer 탭 Basic Measurement 탭의 table에서 default column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Protein")

@then("Basic Analyzer 탭 Basic Measurement 탭의 table에서 Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(-2)
  Tables.TableWidget39.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer 탭 Basic Measurement 탭의 table에서 default column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Hemoglobin")

@then("Basic Analyzer 탭 Basic Measurement 탭의 table에서 Baseline RI 값이 1.3337로, RII값이 0.15로 입력되는지 확인")
def step_impl():
  Tables.TableWidget26.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Analyzer 탭 Basic Measurement 탭의 table에서 default column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Lipid")

@then("Basic Analyzer 탭 Basic Measurement 탭의 table에서 Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인")
def step_impl():
  Tables.TableWidget40.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Basic Analyzer 탭 Basic Measurement 탭의 table에서 default column의 Preset 드롭다운 버튼을 Customize로 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Customize")

@when("Basic Analyzer 탭 Basic Measurement 탭의 table에서 Baseline RI row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("Baseline RI", "default")
  tableWidget.Keys("1.3337")
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  lineEdit.SetText("123456")
  lineEdit.Keys("[Enter]")

@when ("Basic Analyzer 탭 Basic Measurement 탭의 table에서 RII row의 cell을 클릭하고 키보드로 ‘12345’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab
  tableWidget = widget.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("RII", "default")
  tableWidget.Keys("0.19")
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  lineEdit.SetText("123456")
  lineEdit.Keys("[Enter]")

@then ("Basic Analyzer 탭 Basic Measurement 탭의 table에서 Baseline RI 값과 RII값이 모두 ‘{arg}’로 변경되는지 확인")
def step_impl(param1):
  Tables.TableWidget28.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Basic Analyzer 탭의 Mask Viewing panel에 RI 마스크가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_RI_Thresholding'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_RI_Thresholding'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  splitter = widget.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(103, 38)
  tomoAnalysis.dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_RI_Thresholding\\BA_RI_Thresholding.tcpro")
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(12, 7)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(96, 66)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(64, 20)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo.ClickItem("RI Thresholding")
  widget.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()


@when("Basic Analyzer 탭 Basic Measurement의 table의 default column의 Preset 드롭다운 버튼을 ‘Protein’으로 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.")
def step_impl():
  TC_ParameterTable = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable
  TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Protein")
  TC_ParameterTable.buttonExecute.ClickButton()

@then("Basic Analyzer 탭에 Measurement 팝업이 생기는지 확인한다.")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.BasicAnalysisResultPanel, "QtText", cmpEqual, "Measurement")
  Aliases.TomoAnalysis.BasicAnalysisResultPanel.Close()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@when("Basic Analyzer 탭의 Measurement 팝업에서 Export 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  buttonHideResult = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.resultBtn
  buttonHideResult.ClickButton()
  buttonHideResult.ClickButton()
  widget = Aliases.TomoAnalysis.BasicAnalysisResultPanel.tableWidget.qt_scrollarea_viewport

  Aliases.TomoAnalysis.BasicAnalysisResultPanel.Click(413, 653)


@then("저장 경로에 지정한 이름의 .csv 파일이 생성되었는지 확인한다.")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(35, 15)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test.Item.DblClick(64, 17)
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.test_csv.Item, "Value", cmpEqual, "test.csv")

@then("Basic Analyzer에서 Measurement 팝업이 열리는지 확인")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer Single Run 탭의 ‘Assign lower threshold’ 사이즈 바가 ‘Assign upper threshold’ 값을 넘지 않는 1.4384까지만 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4384")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@then("Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 사이즈 바가 ‘Assign lower threshold’ 값을 넘지 않는 1.3584까지만 내려가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3584")


@then("리포트와 TA종료")
def step_impl():
  aqUtils.Delay(3000)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("2개 이상의 cube를 RI Thresholding으로 batch run 한 결과의 Report 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Report\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Report\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Report\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Batch_RUN - Report'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Batch_RUN - Report'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(65, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Batch_RUN - Report\\BA_Batch_RUN - Report.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.frame_tab.bt_round_tab3.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_CubePanel = frame.widget_panel.CubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_CubePanel.input_high
  lineEdit.Click(90, 27)
  lineEdit.SetText("C2")
  tomoAnalysis_ProjectManager_Plugins_CubePanel.bt_round_operation.ClickButton()
  widget = tomoAnalysis_ProjectManager_AppUI_MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  widget.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(91, 45)
  widget.bt_round_tool2.Click(86, 12)
  tomoAnalysis_ProjectManager_Plugins_LinkDialog = tomoAnalysis.LinkDialog
  tomoAnalysis_ProjectManager_Plugins_LinkDialog.dropdown_high.ClickItem("C2")
  tomoAnalysis_ProjectManager_Plugins_LinkDialog.bt_square_primary.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(111, 22)



@when("data table의 스크롤 바를 마우스로 클릭한 상태로 오른쪽으로 움직인다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.dataTable.tabWidget.qt_tabwidget_stackedwidget.DataTab
  widget.TableItemWidget.tableView.qt_scrollarea_hcontainer.ScrollBar.wPosition = 3
  widget.TableItemWidget2.tableView.qt_scrollarea_hcontainer.ScrollBar.wPosition = 3


@then("스크롤 바를 민 것처럼 화면에 표시되는 Data Table 패널 전체가 오")
def step_impl():
  raise NotImplementedError

@given("Basic Analyzer 탭 Mask Viewing panel에 마스크가 생성되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@given("Basic Analyzer 탭 Mask Viewing panel에 마스크가 생성되어 있지 않다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar.Click(249, 29)
  Regions.TomoAnalysis142.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)


@then("Basic Analyzer 탭에 경고창이 떴는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox6, "focus", cmpEqual, False)

@then("Processing mode 패널 UL Threshold 탭의 Assign lower threshold 값이 1.0010으로 설정되어 있는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin, "wValue", cmpEqual, 1.0009999999999999)

@then("Basic Analyzer Single Run 탭의 Mask Viewing panel 3D 화면과 2D 화면에 RI 마스크가 생성되었는지 확인")
def step_impl():
  Regions.viewerPanel.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@when("3D 마스크 캔버스 좌하단 정육면체의 위쪽 변을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(75, 674)

@then("해당 변이 정면으로 오도록 마스크와 정육면체의 방향이 움직였는지 확인")
def step_impl():
  Regions.TomoAnalysis136.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 19060)

@when("3D 마스크 캔버스 좌하단 정육면체의 Left면을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(65, 698)
  
@then("Left 면이 정면으로 오도록 마스크와 정육면체의 방향이 움직였는지 확인")
def step_impl():
  Regions.TomoAnalysis137.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 19060)

@when("3D 마스크 캔버스 좌하단 정육면체의 Left 면의 오른쪽 위 점을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(117, 676)

@then("해당 점이 정면으로 오도록 마스크와 정육면체의 방향이 움직였는지 확인")
def step_impl():
  Regions.TomoAnalysis138.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 19060)

@when("마우스를 커서 모양이 바뀌는 3D 마스크 캔버스 위로 가져가 마우스 휠을 위로 두 칸 올린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.MouseWheel(1)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.MouseWheel(1)
  
@then("3D 마스크가 두 단위 확대됨을 확인")
def step_impl():
  Regions.TomoAnalysis139.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 19060)


@when("마우스를 커서 모양이 바뀌는 3D 마스크 캔버스 위로 가져가 마우스 휠을 아래로 두 칸 내린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.MouseWheel(-1)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.MouseWheel(-1)

@then("3D 마스크가 두 단위 축소됨을 확인")
def step_impl():
  Regions.TomoAnalysis140.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 19060)

@then("error TA종료")
def step_impl():
  Aliases.TomoAnalysis.MessageBox6.qt_msgbox_buttonbox.buttonOk.ClickButton()
  aqUtils.Delay(3000)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Basic Analyzer Single Run 탭에 Hep G2{arg} 데이터의 RI 마스크가 열려 있다.")
def step_impl(param1):
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_borderkill\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_borderkill\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_borderkill\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_borderkill'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_borderkill'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(61, 18)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_borderkill\\BA_borderkill.tcpro")
  tomoAnalysis_ProjectManager_AppUI_MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(66, 22)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(112, 105)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(104, 8)
  groupBox = tomoAnalysis_ProjectManager_AppUI_MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox
  groupBox.procCombo.ClickItem("RI Thresholding")
  groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()


@when("Basic Analyzer 탭 Labeling 탭에서 Use Labeling을 체크하고 Option을 Multi Labels로 선택한 뒤 Labeling 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  TC_ParameterControl.TC_StrCheckBox2.Click(11, 15)
  scrollBar.wPosition = 100
  TC_ParameterControl.buttonExecute.ClickButton()

@when("Basic Analyzer 탭 Border Kill 탭에서 Use Border Kill을 체크하고 Border Kill 탭의 Execute를 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 318
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl
  TC_ParameterControl.TC_StrCheckBox.Click(12, 13)
  scrollBar.wPosition = 318
  TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer 탭 Mask Viewing panel에서 가장자리 세포의 마스크가 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis143.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@when("File Change 팝업에서 Accept 버튼을 누른다.")
def step_impl():
  if Aliases.TomoAnalysis.fileChangeForm.Exists == True :
    Aliases.TomoAnalysis.fileChangeForm.bt_square_primary.ClickButton()
